package main.groovy.GUI

import javafx.application.Application
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.geometry.HPos
import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.CheckBox
import javafx.scene.control.Label
import javafx.scene.control.ScrollPane
import javafx.scene.layout.BorderPane
import javafx.scene.layout.GridPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.stage.Stage
import main.groovy.ALU
import main.groovy.Component.ComponentType
import main.groovy.GUIWrapper.ChipState
import main.groovy.GUIWrapper.WrapperInterface.WrappedBoard
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent
import main.groovy.InstructionDecoderUnit
import main.groovy.MiniCPU
import main.groovy.Wire.Wire

class MainWindow extends Application {
    
    static final String TITLE                   = "MiniCpu-853 Simulator"
    static final String BUTTON_TICK_TEXT        = "Tick >>"
    static final String BUTTON_CYCLE_TEXT       = "Cycle >>"
    static final String BUTTON_INSTRUCTION_TEXT = "Instr. >>"
    
    static final int WIDTH = 500
    static final int HEIGHT = 500
    
    private static final Insets INSETS_SMALL = new Insets(3.0, 3.0, 3.0, 3.0)
    private static final Insets INSET_MEDIUM = new Insets(7.0, 7.0, 7.0, 7.0)
    private static final Insets INSETS_LARGE = new Insets(15.0, 15.0, 15.0, 15.0)
    
    private Stage _primaryStage
    private Scene _scene
    
    private HBox _controlPanel
    private VBox _stateInfoPanel
    private ScrollPane _inputPanel
    private ScrollPane _scrollingComponentList
    private VBox _inputPanelContent
    private GridPane _componentList
    
    private WrappedBoard _board
    private HashMap<String, Label> _taggedLabels
    private HashMap<CheckBox, Wire> _checkboxesToWires
    String borderStyle =
        ''' -fx-border-color: grey;
            -fx-border-width: 1;'''
    
    
    
    MainWindow() {
    
    }
    
    void runApplication(String[] args) {
        launch(MainWindow.class, args)
    }
    
    @Override
    void start(Stage primaryStage) throws Exception {
        
        _primaryStage = primaryStage
        _primaryStage.setTitle(TITLE)

        BorderPane _root = new BorderPane()
        
        _controlPanel = new HBox()
        _controlPanel.setPadding(INSETS_SMALL)
        _controlPanel.setStyle(borderStyle)
        Button tickButton = new Button(BUTTON_TICK_TEXT)
        tickButton.setOnAction(new EventHandler<ActionEvent>() {
            void handle(ActionEvent actionEvent) {
                _board.tick()
                updateInputs()
                updateOutputs()
            }
        })

        Button cycleButton = new Button(BUTTON_CYCLE_TEXT)
        cycleButton.setOnAction(new EventHandler<ActionEvent>() {
            void handle(ActionEvent actionEvent) {
                _board.clockCycle()
                updateInputs()
                updateOutputs()
            }
        })
        
        Button instructionButton = new Button(BUTTON_INSTRUCTION_TEXT)

        _controlPanel.getChildren().addAll(tickButton, cycleButton, instructionButton)
        
        _stateInfoPanel = new VBox()
        _stateInfoPanel.setPadding(INSETS_SMALL)
        _stateInfoPanel.setStyle(borderStyle)
    
        _inputPanel = new ScrollPane()
        _inputPanel.setPadding(INSETS_SMALL)
        _inputPanel.setStyle(borderStyle)
        _inputPanel.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER)
        _inputPanel.setMinWidth(250.0)
        
        _inputPanelContent = new VBox()
    
        _inputPanel.setContent(_inputPanelContent)
        
        _componentList = new GridPane()
        _componentList.setPadding(INSETS_LARGE)
        _componentList.setHgap(10.0)
        _componentList.setVgap(10.0)
    
        _scrollingComponentList = new ScrollPane()
        _scrollingComponentList.setPadding(INSETS_SMALL)
        _scrollingComponentList.setStyle(borderStyle)
        _scrollingComponentList.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER)
        _scrollingComponentList.setContent(_componentList)
        
        _board = loadCPU()
    
        updateOutputs()
    
        _root.setRight(_stateInfoPanel)
        _root.setLeft(_inputPanel)
        _root.setBottom(_controlPanel)
        _root.setCenter(_scrollingComponentList)
    
        _scene = new Scene(_root, WIDTH, HEIGHT)
        _primaryStage.setScene(_scene)
        _primaryStage.show()
        
    }
    
    WrappedBoard loadCPU() {
        MiniCPU cpu = new MiniCPU()
        WrappedBoard board = cpu.Board()
        loadBoard(board)
        
        return board
    }
    
    WrappedBoard loadInstructionDecoder() {
        InstructionDecoderUnit instructionDecoderUnit = new InstructionDecoderUnit()
        WrappedBoard board = instructionDecoderUnit.Board()
        loadBoard(board)
    
        _checkboxesToWires = new HashMap<>()
        _inputPanelContent.getChildren().with {
            add(createInput("DATA", instructionDecoderUnit.DATA))
            add(createInput("SP", instructionDecoderUnit.SP))
            add(createInput("MEM_ADDR 1", instructionDecoderUnit.MEM_1))
            add(createInput("MEM_ADDR 2", instructionDecoderUnit.MEM_2))
            add(createInput("MEM_ADDR 3", instructionDecoderUnit.MEM_3))
            add(createInput("MEM_ADDR 4", instructionDecoderUnit.MEM_4))
            add(createInput("U115 Select", instructionDecoderUnit.U115_SEL))
            add(createInput("U116 Select", instructionDecoderUnit.U116_SEL))
            add(createInput("U15 Chip Sel.", instructionDecoderUnit.U15_CS))
            add(createInput("U15 Clock.", instructionDecoderUnit.U15_CLOCK))
        }
        
        return board
    }
    
    WrappedBoard loadALU() {
        ALU alu = new ALU()
        WrappedBoard board = alu.Board()
        loadBoard(board)
    
        _checkboxesToWires = new HashMap<>()
        _inputPanelContent.getChildren().with{
            add(createInput("Register Chip Sel.", alu.REGISTER_CHIP_SELECT))
            add(createInput("U14 Chip Sel.", alu.U14_CHIP_SELECT))
            add(createInput("U100 C_in", alu.U100_CIN))
            add(createInput("U107 +/-", alu.U107_PLUS_MINUS))
            add(createInput("U110 chip Sel.", alu.U110_CHIP_SELECT))
    
            add(createInput("U111 Sel.", alu.U111_SEL))
    
            add(createInput("U112 MEM_ADDR", alu.U112_MEM))
            add(createInput("U112 FLAGS", alu.U112_FLAGS))
            add(createInput("U112 INST", alu.U112_INST))
            add(createInput("U112 SEL", alu.U112_SEL))
    
            add(createInput("U113 MEM_ADDR", alu.U113_MEM))
            add(createInput("U113 FLAGS", alu.U113_FLAGS))
            add(createInput("U113 INST", alu.U113_INST))
            add(createInput("U113 SEL", alu.U113_SEL))
    
            add(createInput("U114 OEA", alu.U114_OEA))
            add(createInput("U114 SELA", alu.U114_SELA))
            add(createInput("U114 OEB", alu.U114_OEB))
            add(createInput("U114 SELB", alu.U114_SELB))
    
            add(createInput("U117 SEL", alu.U117_SEL))
    
            add(createInput("U118A DATA", alu.U118A_DATA))
            add(createInput("U118A SP", alu.U118A_SP))
            add(createInput("U118A INS.", alu.U118A_INST))
            add(createInput("U118A ALU", alu.U118A_ALU))
            add(createInput("U118A MEM_ADDR", alu.U118A_MEM))
            add(createInput("U118A SEL", alu.U118A_SEL))
    
            add(createInput("U118B DATA", alu.U118B_DATA))
            add(createInput("U118B SP", alu.U118B_SP))
            add(createInput("U118B INST", alu.U118B_INST))
            add(createInput("U118B ALU", alu.U118B_ALU))
            add(createInput("U118B MEM_ADDR", alu.U118B_MEM))
            add(createInput("U118B SEL", alu.U118B_SEL))
    
            add(createInput("U120 SEL", alu.U120_SEL))
        }
        
        return board
    }
    
    VBox createInput(String name, Wire wire) {
        VBox inputRoot = new VBox()
        Label label = new Label(name)
        CheckBox control = new CheckBox()
        inputRoot.getChildren().addAll(label, control)
        _checkboxesToWires.put(control, wire)
        return inputRoot
    }
    
    VBox createInput(String name, Wire[] wires) {
        VBox inputRoot = new VBox()
        Label label = new Label(name)
        HBox controlHolder = new HBox()
        controlHolder.setSpacing(-1.0)
        wires.toList().reverse().each {
            CheckBox control = new CheckBox()
            controlHolder.getChildren().add(control)
            _checkboxesToWires.put(control, it)
        }
        inputRoot.getChildren().addAll(label, controlHolder)
        return inputRoot
    }
    
    void loadBoard(WrappedBoard board) {
        _taggedLabels = new HashMap<>()

        int gridx = 0
        int gridy = 0
        final int maxGridWidth = 4
    
        board.getComponents().each{
            WrappedComponent component ->
                // Add Component to List
            
                VBox componentInfo = new VBox()
                Label idLabel = new Label("${component.getId()}")
                Label nameLabel = new Label("${component.getName()}")
                Label typeLabel = new Label("${component.getType()}")
            
                componentInfo.getChildren().addAll(idLabel, nameLabel, typeLabel)
            
                GridPane table = new GridPane()
                table.setHgap(5.0)
            
                ChipState chipState = component.getChipState()
                chipState.inputs().eachWithIndex{
                    String input, int index ->
                        
                        Label inputName = new Label("${input}")
                        GridPane.setHalignment(inputName, HPos.LEFT)
                        Label inputValue = new Label("${chipState.state(input)}")
                        GridPane.setHalignment(inputValue, HPos.RIGHT)
    
                        _taggedLabels.put("${component.getId()}_${input}".toString(), inputValue)
    
                        GridPane.setConstraints(inputName, 0, index)
                        GridPane.setConstraints(inputValue, 1, index)
                        table.getChildren().addAll(inputName, inputValue)
                }
            
                componentInfo.getChildren().add(table)
                componentInfo.setStyle(borderStyle)
                componentInfo.setPadding(INSET_MEDIUM)
            
                GridPane.setConstraints(componentInfo, gridx, gridy)
                _componentList.getChildren().add(componentInfo)
                gridx++
                if(gridx >= maxGridWidth) {
                    gridx = 0
                    gridy++
                }
            
                // Add Registers to state info panel
                if(component.getType() == ComponentType.REGISTER.toString()) {
                    Label label = new Label()
                    label.setPadding(INSETS_LARGE)
                    _taggedLabels.put("${component.getId()}_INTERNAL_VALUE".toString(), label)
                    _stateInfoPanel.getChildren().add(label)
                }
                
                if(component.getType() == ComponentType.INSTRUCTION_DECODER.toString()) {
                    Label label = new Label()
                    label.setPadding(INSETS_LARGE)
                    _taggedLabels.put("${component.getId()}_DATA".toString(), label)
                    _stateInfoPanel.getChildren().add(label)
                }
        }
    }
    
    void updateInputs() {
        _checkboxesToWires.each {
                it.value.setEnd(Wire.IN, it.key.isSelected())
        }
        
        _checkboxesToWires.each{
            it.value.process()
        }
        
        _board.getComponents().each{
            WrappedComponent component ->
                ChipState chipState = component.getChipState()
                chipState.inputs().eachWithIndex{
                    String input, int index ->
                        Label inputValue = _taggedLabels.get("${component.getId()}_${input}".toString())
                        inputValue.setText("${chipState.state(input)}")
                }
        }
    }
    
    void updateOutputs() {
        _board.getComponents().each{
            WrappedComponent component ->
                if(component.getType() == ComponentType.REGISTER.toString()) {
                    Label label = _taggedLabels.get("${component.getId()}_INTERNAL_VALUE".toString())
                    label.setText("${component.getId()}\n\t${component.getName()}\n\tValue: ${component.getValue("internal_value")}")
                }
                if(component.getType() == ComponentType.INSTRUCTION_DECODER.toString()) {
                    Label label = _taggedLabels.get("${component.getId()}_DATA".toString())
                    label.setText(
                        "${component.getId()}\n" +
                        "\t${component.getName()}\n" +
                        "\tMemory: ${component.getValue("memory_buffer")}\n" +
                        "\tInstruction: ${component.getValue("instruction")}\n" +
                        "\tState: ${component.getValue("state")} (${component.getValue("cycles")})\n" +
                        "\tSrc: ${component.getValue("operand2")}\n" +
                        "\tDst: ${component.getValue("operand1")}"
                    )
                }
        }
    }
}
