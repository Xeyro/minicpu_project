package main.groovy

import main.groovy.Board.Board
import main.groovy.Board.CircuitBoard
import main.groovy.Component.Adder
import main.groovy.Component.ComponentType
import main.groovy.Component.DataSelector
import main.groovy.Component.FlagsChip
import main.groovy.Component.LogicChip
import main.groovy.Component.Register
import main.groovy.Component.U114Demulitplexer
import main.groovy.GUIWrapper.WrappedBoards.WrappedBoardFactory
import main.groovy.GUIWrapper.WrapperInterface.WrappedBoard
import main.groovy.Wire.UniDirectionalWire
import main.groovy.Wire.Wire

class ALU {
    
    private final Board _board
    
    private final Register          U10
    private final Register          U11
    private final Register          U12
    private final Register          U13
    private final Register          U14
    private final Adder             U100
    private final LogicChip         U101
    private final LogicChip         U102
    private final LogicChip         U103
    private final LogicChip         U104
    private final Adder             U107
    private final Register          U110
    private final DataSelector      U111
    private final DataSelector      U112
    private final DataSelector      U113
    private final U114Demulitplexer U114
    private final DataSelector      U117
    private final DataSelector      U118A
    private final DataSelector      U118B
    private final DataSelector      U120
    private final FlagsChip         E1
    private final FlagsChip         E2
    
    private final Wire ALWAYS_HIGH  = [getEnd : { end -> true }, setEnd : { end, state -> }] as Wire
    private final Wire ALWAYS_LOW   = [getEnd : { end -> false }, setEnd : { end, state -> }] as Wire

    /** Inputs */
    public final Wire   CLOCK                   = new UniDirectionalWire()
    
    public final Wire[] REGISTER_CHIP_SELECT    = new Wire[4]
    public final Wire   U14_CHIP_SELECT         = new UniDirectionalWire()
    public final Wire   U100_CIN                = new UniDirectionalWire()
    public final Wire   U107_PLUS_MINUS         = new UniDirectionalWire()
    public final Wire   U110_CHIP_SELECT        = new UniDirectionalWire()

    public final Wire[] U111_SEL                = new Wire[3]
    
    public final Wire[] U112_MEM                = new Wire[8]
    public final Wire[] U112_FLAGS              = new Wire[8]
    public final Wire[] U112_INST               = new Wire[8]
    public final Wire[] U112_SEL                = new Wire[3]
    
    public final Wire[] U113_MEM                = new Wire[8]
    public final Wire[] U113_FLAGS              = new Wire[8]
    public final Wire[] U113_INST               = new Wire[8]
    public final Wire[] U113_SEL                = new Wire[3]
    
    public final Wire   U114_OEA                = new UniDirectionalWire()
    public final Wire[] U114_SELA               = new Wire[2]
    public final Wire   U114_OEB                = new UniDirectionalWire()
    public final Wire[] U114_SELB               = new Wire[2]
    
    public final Wire   U117_SEL                = new UniDirectionalWire()

    public final Wire[] U118A_DATA              = new Wire[8]
    public final Wire[] U118A_SP                = new Wire[8]
    public final Wire[] U118A_INST              = new Wire[8]
    public final Wire[] U118A_ALU               = new Wire[8]
    public final Wire[] U118A_MEM               = new Wire[8]
    public final Wire[] U118A_SEL               = new Wire[3]
    
    public final Wire[] U118B_DATA              = new Wire[8]
    public final Wire[] U118B_SP                = new Wire[8]
    public final Wire[] U118B_INST              = new Wire[8]
    public final Wire[] U118B_ALU               = new Wire[8]
    public final Wire[] U118B_MEM               = new Wire[8]
    public final Wire[] U118B_SEL               = new Wire[3]
    
    public final Wire   U120_SEL                = new UniDirectionalWire()
    
    /** Outputs */
    public final Wire[] DATA                    = new Wire[16]
    public final Wire[] FLAGS                   = new Wire[4]
    public final Wire[] ALU                     = new Wire[8]
    public final Wire[] SP                      = new Wire[16]
    
    /** Other */
    public final Wire NOTHING                   = new UniDirectionalWire()
    
    WrappedBoard Board() { WrappedBoardFactory.wrap(_board) }
    
    ALU() {
        _board = new CircuitBoard()
        
        /** Create Components ======== */
        /** ========================== */
        U10     = new Register(8)
        U10.setId("U10")
        U10.setName("Register (R1)")
        _board.addComponent(U10)
        
        U11     = new Register(8)
        U11.setId("U11")
        U11.setName("Register (R2)")
        _board.addComponent(U11)

        U12     = new Register(8)
        U12.setId("U12")
        U12.setName("Register (R3)")
        _board.addComponent(U12)

        U13     = new Register(8)
        U13.setId("U13")
        U13.setName("Register (R4)")
        _board.addComponent(U13)

        U14     = new Register(16)
        U14.setId("U14")
        U14.setName("Stack Pointer")
        _board.addComponent(U14)
    
        U100    = new Adder(8)
        U100.setId("U100")
        U100.setName("ALU Adder")
        _board.addComponent(U100)
    
        U101    = new LogicChip.Builder()
            .operandSize(8)
            .functionAnd()
            .build()
        U101.setId("U101")
        U101.setName("ALU AND Gate")
        _board.addComponent(U101)
    
        U102    = new LogicChip.Builder()
            .operandSize(8)
            .functionOr()
            .build()
        U102.setId("U102")
        U102.setName("ALU OR Gate")
        _board.addComponent(U102)

        U103    = new LogicChip.Builder()
            .operandSize(8)
            .functionXor()
            .build()
        U103.setId("U103")
        U103.setName("ALU XOR Gate")
        _board.addComponent(U103)
    
        U104    = new LogicChip.Builder()
            .operandSize(8)
            .functionNot()
            .build()
        U104.setId("U104")
        U104.setName("ALU NOT Gate")
        _board.addComponent(U104)
    
        U107    = new Adder(16)
        U107.setId("U107")
        U107.setName("Stack Pointer Adder")
        _board.addComponent(U107)

        U110    = new Register(4)
        U110.setId("U110")
        U110.setName("FLAGS Register")
        _board.addComponent(U110)

        U111    = new DataSelector.Builder()
            .inputSize(8)
            .ioCount(8)
            .selects(3)
            .buildMultiplexer()
        U111.setId("U111")
        U111.setName("ALU Output Selector")
        _board.addComponent(U111)

        U112    = new DataSelector.Builder()
            .inputSize(8)
            .ioCount(8)
            .selects(3)
            .buildMultiplexer()
        U112.setId("U112")
        U112.setName("ALU Reg. A Selector")
        _board.addComponent(U112)
    
        U113    = new DataSelector.Builder()
            .inputSize(8)
            .ioCount(8)
            .selects(3)
            .buildMultiplexer()
        U113.setId("U113")
        U113.setName("ALU Reg. B Selector")
        _board.addComponent(U113)
    
        U114    = new U114Demulitplexer()
        U114.setId("U114")
        U114.setName("2-4 Demultiplexer")
        _board.addComponent(U114)

        U117    = new DataSelector.Builder()
            .inputSize(16)
            .ioCount(2)
            .buildMultiplexer()
        U117.setId("U117")
        U117.setName("2-1 Multiplexer")
        _board.addComponent(U117)

        U118A   = new DataSelector.Builder()
            .inputSize(8)
            .ioCount(8)
            .selects(3)
            .buildMultiplexer()
        U118A.setId("U118A")
        U118A.setName("ALU Input A Selector")
        _board.addComponent(U118A)
        
        U118B   = new DataSelector.Builder()
            .inputSize(8)
            .ioCount(8)
            .selects(3)
            .buildMultiplexer()
        U118B.setId("U118B")
        U118B.setName("ALU Input B Selector")
        _board.addComponent(U118B)
        
        U120    = new DataSelector.Builder()
            .inputSize(4)
            .ioCount(2)
            .buildMultiplexer()
        U120.setId("U120")
        U120.setName("2-1 Multiplexer")
        _board.addComponent(U120)
    
    
        E1      = new FlagsChip(8)
        E1.setId("E1")
        E1.setName("Flags")
        _board.addComponent(E1)
    
        E2      = new FlagsChip(8)
        E2.setId("E2")
        E2.setName("Flags")
        _board.addComponent(E2)
    
        /** Connect Components ======= */
        /** ========================== */
    
        int offset = 0

// ====== U10 (Register 1)
        REGISTER_CHIP_SELECT[0] = new UniDirectionalWire()
        U10.connect(REGISTER_CHIP_SELECT[0], U10.CHIP_SELECT, Wire.OUT)
        U10.connect(CLOCK.split(), U10.CLOCK, Wire.OUT)
    
// ====== U11 (Register 2)
        REGISTER_CHIP_SELECT[1] = new UniDirectionalWire()
        U11.connect(REGISTER_CHIP_SELECT[1], U11.CHIP_SELECT, Wire.OUT)
        U11.connect(CLOCK.split(), U11.CLOCK, Wire.OUT)

// ====== U12 (Register 3)
        REGISTER_CHIP_SELECT[2] = new UniDirectionalWire()
        U12.connect(REGISTER_CHIP_SELECT[2], U12.CHIP_SELECT, Wire.OUT)
        U12.connect(CLOCK.split(), U12.CLOCK, Wire.OUT)

// ====== U13 (Register 4)
        REGISTER_CHIP_SELECT[3] = new UniDirectionalWire()
        U13.connect(REGISTER_CHIP_SELECT[3], U13.CHIP_SELECT, Wire.OUT)
        U13.connect(CLOCK.split(), U13.CLOCK, Wire.OUT)
        
    // Connect Registers in bulk
        (0..7).each{
            int index ->
                _board.connect(U114, U114.OUTPUT_R1 + index, U10, U10.INPUT + index)
                _board.connect(U114, U114.OUTPUT_R2 + index, U11, U11.INPUT + index)
                _board.connect(U114, U114.OUTPUT_R3 + index, U12, U12.INPUT + index)
                _board.connect(U114, U114.OUTPUT_R4 + index, U13, U13.INPUT + index)
        }

// ====== U14 Stack Pointer
        offset = 0
        (0..(U14.inputSize()-1)).each{
            int index ->
                _board.connect(U117, U117.OUTPUT + index, U14, U14.INPUT + index)
        }

        U14.connect(U14_CHIP_SELECT, U14.CHIP_SELECT, Wire.OUT)
        U14.connect(CLOCK, U14.CLOCK, Wire.OUT)
        


// ====== U100 - U104
        // U100
        U100.connect(ALWAYS_HIGH, U100.CHIP_SELECT, Wire.OUT)
        U100.connect(ALWAYS_LOW, U100.SUBTRACT, Wire.OUT)
        U100.connect(U100_CIN, U100.CARRY_IN, Wire.OUT)
        
        U101.connect(ALWAYS_HIGH, U101.CHIP_SELECT, Wire.OUT)
        U102.connect(ALWAYS_HIGH, U102.CHIP_SELECT, Wire.OUT)
        U103.connect(ALWAYS_HIGH, U103.CHIP_SELECT, Wire.OUT)
        U104.connect(ALWAYS_HIGH, U104.CHIP_SELECT, Wire.OUT)
        
        Wire[] U112ToLogicChips = new Wire[U112.inputSize()]
        Wire[] U113ToLogicChips = new Wire[U113.inputSize()]
        
        (0..(U112.inputSize()-1)).each{
            int index ->
                Wire u112wire = _board.connect(U112, U112.OUTPUT + index, U100, U100.INPUT_A + index)
                Wire u113wire = _board.connect(U113, U113.OUTPUT + index, U100, U100.INPUT_B + index)
            
                U112ToLogicChips[index] = u112wire
                U113ToLogicChips[index] = u113wire
            
                U101.connect(u112wire.split(), U101.INPUT_A + index, Wire.OUT)
                U101.connect(u113wire.split(), U101.INPUT_B + index, Wire.OUT)
            
                U102.connect(u112wire.split(), U102.INPUT_A + index, Wire.OUT)
                U102.connect(u113wire.split(), U102.INPUT_B + index, Wire.OUT)
            
                U103.connect(u112wire.split(), U103.INPUT_A + index, Wire.OUT)
                U103.connect(u113wire.split(), U103.INPUT_B + index, Wire.OUT)
            
                // 104 is a NOT gate, only takes one input; other is always low
                U104.connect(u112wire.split(), U104.INPUT_A + index, Wire.OUT)
                U104.connect(ALWAYS_LOW, U104.INPUT_B + index, Wire.OUT)
// DATA
                DATA[index] = u112wire.split()
                DATA[U112.inputSize() + index] = u113wire.split()
        }
    
// ====== U107
        offset = 0
        (0..(U14.inputSize()-1)).each{
            int index ->
                Wire stackPointerWire = _board.connect(U14, U14.OUTPUT + index, U107, U107.INPUT_A + index)
                SP[index] = stackPointerWire.split()
        }

        U107.connect(ALWAYS_HIGH, U107.CHIP_SELECT, Wire.OUT)
        U107.connect(ALWAYS_LOW, U107.CARRY_IN, Wire.OUT)
        
        // U107, input_b = 2
        (0..(U107.operandSize()-1)).each{
            int index ->
                if(index == 1)
                    U107.connect(ALWAYS_HIGH, U107.INPUT_B + index , Wire.OUT)
                else
                    U107.connect(ALWAYS_LOW, U107.INPUT_B + index, Wire.OUT)
        }

        U107.connect(U107_PLUS_MINUS, U107.SUBTRACT, Wire.OUT)
        U107.connect(NOTHING, U107.CARRY_OUT, Wire.IN)
        
// ====== U110
        offset = 0
        (0..(U110.inputSize()-1)).each{
            int index ->
                _board.connect(U120, U120.OUTPUT + index, U110, U110.INPUT + index)
        }

        U110.connect(CLOCK, U110.CLOCK, Wire.OUT)
        U110.connect(U110_CHIP_SELECT, U110.CHIP_SELECT, Wire.OUT)
        
        (0..(FLAGS.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                FLAGS[index] = wire
                U110.connect(FLAGS[index], U110.OUTPUT + index, Wire.IN)
        }
        
// ====== U111
        offset = 0
        (0..(U111.inputSize()-1)).each{
            int index ->
                _board.connect(U100, U100.OUTPUT + index, U111, U111.INPUT + offset + index)
        }
        
        offset += U111.inputSize()
        (0..(U111.inputSize()-1)).each{
            int index ->
                _board.connect(U101, U101.OUTPUT + index, U111, U111.INPUT + offset + index)
        }
    
        offset += U111.inputSize()
        (0..(U111.inputSize()-1)).each{
            int index ->
                _board.connect(U102, U102.OUTPUT + index, U111, U111.INPUT + offset + index)
        }
    
        offset += U111.inputSize()
        (0..(U111.inputSize()-1)).each{
            int index ->
                _board.connect(U103, U103.OUTPUT + index, U111, U111.INPUT + offset + index)
        }
    
        offset += U111.inputSize()
        (0..(U111.inputSize()-1)).each{
            int index ->
                _board.connect(U104, U104.OUTPUT + index, U111, U111.INPUT + offset + index)
        }
    
        offset += U111.inputSize()
        (0..23).each{
            int index ->
                U111.connect(ALWAYS_LOW, U111.INPUT + offset + index, Wire.OUT)
        }
        
        U111.connect(ALWAYS_HIGH, U111.CHIP_SELECT, Wire.OUT)
    
        (0..2).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U111_SEL[index] = wire
                U111.connect(wire, U111.SELECTOR + index, Wire.OUT)
        }
        
// ALU
        (0..(U111.inputSize()-1)).each{
            int index ->
                Wire aluWire = new UniDirectionalWire()
                ALU[index] = aluWire
                U111.connect(aluWire, U111.OUTPUT + index, Wire.IN)
        }
        
        

// ====== U112 (Register A Selector)
    
        
        // R1
        offset = 0
        (0..(U112.inputSize()-1)).each {
            int index ->
                Wire wire = _board.connect(U10, U10.OUTPUT + index, U112, U112.INPUT + offset + index)
                U113.connect(wire.split(), U113.INPUT + offset + index, Wire.OUT)
        }
        
        // R2
        offset += U112.inputSize()
        (0..(U112.inputSize()-1)).each{
            int index ->
                Wire wire = _board.connect(U11, U11.OUTPUT + index, U112, U112.INPUT + offset + index)
                U113.connect(wire.split(), U113.INPUT + offset + index, Wire.OUT)
        }
    
        // R3
        offset += U112.inputSize()
        (0..(U112.inputSize()-1)).each{
            int index ->
                Wire wire = _board.connect(U12, U12.OUTPUT + index, U112, U112.INPUT + offset + index)
                U113.connect(wire.split(), U113.INPUT + offset + index, Wire.OUT)
        }
    
        // R4
        offset += U112.inputSize()
        (0..(U112.inputSize()-1)).each{
            int index ->
                Wire wire = _board.connect(U13, U13.OUTPUT + index, U112, U112.INPUT + offset + index)
                U113.connect(wire.split(), U113.INPUT + offset + index, Wire.OUT)
        }
    
        // MEM_ADDR
        offset += U112.inputSize()
        (0..(U112_MEM.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U112.connect(wire, U112.INPUT + offset + index, Wire.OUT)
                U112_MEM[index] = wire
        }
    
        // FLAGS
        offset += U112.inputSize()
        (0..(U112_FLAGS.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U112.connect(wire, U112.INPUT + offset + index, Wire.OUT)
                U112_FLAGS[index] = wire
        }
    
        // INST
        offset += U112.inputSize()
        (0..(U112_INST.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U112.connect(wire, U112.INPUT + offset + index, Wire.OUT)
                U112_INST[index] = wire
        }
    
        offset += U112.inputSize()
        (0..7).each{
            int index ->
                U112.connect(ALWAYS_LOW, U112.INPUT + index + offset, Wire.OUT)
        }
        
        U112.connect(ALWAYS_HIGH, U112.CHIP_SELECT, Wire.OUT)
        
        (0..2).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U112_SEL[index] = wire
                U112.connect(wire, U112.SELECTOR + index, Wire.OUT)
        }
    
// ====== U113 (Register B Selector)


        offset = U113.inputSize() * 3
        // MEM_ADDR
        offset += U113.inputSize()
        (0..(U113_MEM.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U113.connect(wire, U113.INPUT + offset + index, Wire.OUT)
                U113_MEM[index] = wire
        }
    
        // FLAGS
        offset += U113.inputSize()
        (0..(U113_FLAGS.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U113.connect(wire, U113.INPUT + offset + index, Wire.OUT)
                U113_FLAGS[index] = wire
        }
    
        // INST
        offset += U113.inputSize()
        (0..(U113_INST.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U113.connect(wire, U113.INPUT + offset + index, Wire.OUT)
                U113_INST[index] = wire
        }
    
        offset += U113.inputSize()
        (0..7).each{
            int index ->
                U113.connect(ALWAYS_LOW, U113.INPUT + index + offset, Wire.OUT)
        }
    
        U113.connect(ALWAYS_HIGH, U113.CHIP_SELECT, Wire.OUT)
    
        (0..2).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U113_SEL[index] = wire
                U113.connect(wire, U113.SELECTOR + index, Wire.OUT)
        }
        
// ====== U114
        // U118A
        (0..7).each{
            int index ->
                _board.connect(U118A, U118A.OUTPUT + index, U114, U114.INPUT_A + index)
        }
        // U118B
        (0..7).each{
            int index ->
                _board.connect(U118B, U118B.OUTPUT + index, U114, U114.INPUT_B + index)
        }

        offset = 0
        (0..(U114_SELA.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U114.connect(wire, U114.SELECT_A + index, Wire.OUT)
                U114_SELA[index] = wire
        }
    
        offset = 0
        (0..(U114_SELB.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U114.connect(wire, U114.SELECT_B + index, Wire.OUT)
                U114_SELB[index] = wire
        }

        U114.connect(U114_OEA, U114.OUTPUT_ENABLE_A, Wire.OUT)
        
        U114.connect(U114_OEB, U114.OUTPUT_ENABLE_B, Wire.OUT)

// ====== U117
        // DATA must be instantiated and populated first
        offset = 0
        (0..(U117.inputSize()-1)).each{
            int index ->
                U117.connect(DATA[index].split(), U117.INPUT + offset + index, Wire.OUT)
        }
    
        offset += U117.inputSize()
        (0..(U117.inputSize()-1)).each{
            int index ->
                _board.connect(U107, U107.OUTPUT + index, U117, U117.INPUT + offset + index)
        }
        
        U117.connect(ALWAYS_HIGH, U117.CHIP_SELECT, Wire.OUT)
        U117.connect(U117_SEL, U117.SELECTOR, Wire.OUT)

// ====== U118A
        // DATA
        offset = 0
        (0..(U118A_DATA.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U118A.connect(wire, U118A.INPUT + offset + index, Wire.OUT)
                U118A_DATA[index] = wire
        }
    
        // SP
        offset += U118A.inputSize()
        (0..(U118A_SP.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U118A.connect(wire, U118A.INPUT + offset + index, Wire.OUT)
                U118A_SP[index] = wire
        }
    
        // INST
        offset += U118A.inputSize()
        (0..(U118A_INST.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U118A.connect(wire, U118A.INPUT + offset + index, Wire.OUT)
                U118A_INST[index] = wire
        }
    
        // ALU
        offset += U118A.inputSize()
        (0..(U118A_ALU.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U118A.connect(wire, U118A.INPUT + offset + index, Wire.OUT)
                U118A_ALU[index] = wire
        }
    
        // RES
        offset += U118A.inputSize()
        (0..7).each{
            int index ->
                U118A.connect(ALWAYS_LOW, U118A.INPUT + offset + index, Wire.OUT)
        }
    
        // MEM_ADDR
        offset += U118A.inputSize()
        (0..(U118A_MEM.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U118A.connect(wire, U118A.INPUT + offset + index, Wire.OUT)
                U118A_MEM[index] = wire
        }
    
        // RES x 2
        offset += U118A.inputSize()
        (0..15).each{
            int index ->
                U118A.connect(ALWAYS_LOW, U118A.INPUT + offset + index, Wire.OUT)
        }
        
        // SEL
        offset = 0
        (0..(U118A_SEL.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U118A.connect(wire, U118A.SELECTOR + index, Wire.OUT)
                U118A_SEL[index] = wire
        }
        
        U118A.connect(ALWAYS_HIGH, U118A.CHIP_SELECT, Wire.OUT)
        
// ====== U118B
        offset = 0
        // DATA
        (0..(U118B_DATA.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U118B.connect(wire, U118B.INPUT + offset + index, Wire.OUT)
                U118B_DATA[index] = wire
        }
    
        // SP
        offset += U118B.inputSize()
        (0..(U118B_SP.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U118B.connect(wire, U118B.INPUT + offset + index, Wire.OUT)
                U118B_SP[index] = wire
        }
    
        // INST
        offset += U118B.inputSize()
        (0..(U118B_INST.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U118B.connect(wire, U118B.INPUT + offset + index, Wire.OUT)
                U118B_INST[index] = wire
        }
    
        // ALU
        offset += U118B.inputSize()
        (0..(U118B_ALU.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U118B.connect(wire, U118B.INPUT + offset + index, Wire.OUT)
                U118B_ALU[index] = wire
        }
    
        // RES
        offset += U118B.inputSize()
        (0..7).each{
            int index ->
                U118B.connect(ALWAYS_LOW, U118B.INPUT + offset + index, Wire.OUT)
        }
    
        // MEM_ADDR
        offset += U118B.inputSize()
        (0..(U118B_MEM.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U118B.connect(wire, U118B.INPUT + offset + index, Wire.OUT)
                U118B_MEM[index] = wire
        }
    
        // RES x 2
        offset += U118B.inputSize()
        (0..15).each{
            int index ->
                U118B.connect(ALWAYS_LOW, U118B.INPUT + offset + index, Wire.OUT)
        }
    
        // SEL
        offset = 0
        (0..(U118B_SEL.size()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U118B.connect(wire, U118B.SELECTOR + index, Wire.OUT)
                U118B_SEL[index] = wire
        }
    
        U118B.connect(ALWAYS_HIGH, U118B.CHIP_SELECT, Wire.OUT)

// ====== U120

        offset = 0
        (0..(U120.inputSize()-1)).each{
            int index ->
                _board.connect(E1, E1.OUTPUT + index, U120, U120.INPUT + index)
        }

        offset += U120.inputSize()
        (0..(U120.inputSize()-1)).each{
            int index ->
                _board.connect(E2, E2.OUTPUT + index, U120, U120.INPUT + index + offset)
        }
        
        U120.connect(ALWAYS_HIGH, U120.CHIP_SELECT, Wire.OUT)
        U120.connect(U120_SEL, U120.SELECTOR, Wire.OUT)
        
// ====== E1

        (0..(E1.inputSize()-1)).each{
            int index ->
                _board.connect(U100, U100.OUTPUT + index, E1, E1.INPUT + index)
        }
        
        _board.connect(U100, U100.CARRY_OUT, E1, E1.CARRY_IN)
        
// ====== E2
    
        (0..(E2.inputSize()-1)).each{
            int index ->
                Wire wire = ALU[index].split()
                E2.connect(wire, E2.INPUT + index, Wire.OUT)
        }
        
        E2.connect(ALWAYS_LOW, E2.CARRY_IN, Wire.OUT)
        
    }
}
