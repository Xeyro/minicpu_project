package main.groovy

import main.groovy.GUI.MainWindow

class Program {
    
    
    static void main(String[] args) {
        MainWindow window = new MainWindow()
        window.runApplication(args)
    }
    
}
