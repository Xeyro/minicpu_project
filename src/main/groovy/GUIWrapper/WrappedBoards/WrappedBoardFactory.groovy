package main.groovy.GUIWrapper.WrappedBoards

import main.groovy.Board.Board
import main.groovy.Board.CircuitBoard
import main.groovy.GUIWrapper.WrapperInterface.WrappedBoard

class WrappedBoardFactory {

    static WrappedBoard wrap(Board board) {
        if(board instanceof CircuitBoard)
            return new WrappedCircuitBoard((CircuitBoard)board)
        
        return null
    }
}
