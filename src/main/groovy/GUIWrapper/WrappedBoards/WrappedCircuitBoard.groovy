package main.groovy.GUIWrapper.WrappedBoards

import main.groovy.Board.CircuitBoard
import main.groovy.Component.Component
import main.groovy.GUIWrapper.WrappedComponents.WrappedComponentFactory
import main.groovy.GUIWrapper.WrappedWires.WrappedWireImpl
import main.groovy.GUIWrapper.WrapperInterface.WrappedBoard
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent
import main.groovy.GUIWrapper.WrapperInterface.WrappedWire
import main.groovy.Wire.Wire

class WrappedCircuitBoard implements WrappedBoard {
    
    private CircuitBoard _circuitBoard

    private List<WrappedComponent> _components
    private List<WrappedWire> _wires
    
    WrappedCircuitBoard(CircuitBoard board) {
        _circuitBoard = board
        
        _components = _circuitBoard.getComponents().collect{
            WrappedComponentFactory.wrap(it)
        }
    
        _wires = _circuitBoard.getWires().collect{
            Wire wire ->
                
                Component input = null
                List<Component> outputs = new ArrayList<>()
                
                // Find the input
                _circuitBoard.getComponents().each{
                    
                    final Wire[] wires = it.getWires()
                    
                    it.getEnds().eachWithIndex{
                        int end, int index ->
                            if(end == Wire.IN && wires[index] == wire) {
                                input = it
                            }
                    }
                }
                
                // Find outputs
                _circuitBoard.getComponents().each{
        
                    final Wire[] wires = it.getWires()
        
                    it.getEnds().eachWithIndex{
                        int end, int index ->
                            if(end == Wire.OUT && wires[index] == wire)
                                outputs.add(it)
                    }
                }
                
                List<String> outputIds = outputs.collect{ it.getId() }
                
                
                if(input == null)
                    System.err.println("Warning: Wire with null input detected: ${wire.toString()}")
                    
                new WrappedWireImpl(
                    wire,
                    input != null ? input.getId() : "",
                    outputIds
                )
        }
    }
    
    List<WrappedComponent> getComponents() {
        return _components
    }
    
    List<WrappedWire> getWires() {
        return _wires
    }
    
    void tick() {
        _circuitBoard.clockTick()
    }
    
    void clockCycle() {
        _circuitBoard.clockCycle()
    }
}
