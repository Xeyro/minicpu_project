package main.groovy.GUIWrapper

import main.groovy.MetaData.MetaData

class ChipState {
    
    static final char UNKNOWN           = '?'
    static final char HIGH_IMPEDANCE    = '-'
    static final char HIGH              = '1'
    static final char LOW               = '0'
    
    private String _componentId
    private Map<String, String> _inputsToStates
    
    ChipState(String componentId) {
        _componentId = componentId
        _inputsToStates = new TreeMap<>()
    }
    
    void addInput(String input, int inputSize) {
    
        if(hideInput(input))
            return
        
        input = renameInputIfNeeded(input)
        
        char[] stateArray = new char[inputSize]
        (0..(inputSize-1)).each{ int index -> stateArray[index] = UNKNOWN }
        _inputsToStates.put(input, new String(stateArray))
    }
    
    List<String> inputs() {
        _inputsToStates.keySet().toList()
    }
    
    String state(String input) {
        _inputsToStates.get(input)
    }

    String renameInputIfNeeded(String input) {
        if(MetaData.chipMetaData.containsKey(_componentId)) {
            MetaData.ChipMetaData metaData = MetaData.chipMetaData.get(_componentId)
            if(metaData.renameInput.containsKey(input))
                input = metaData.renameInput.get(input)
        }
        return input
    }
    
    boolean hideInput(String input) {
        if(MetaData.chipMetaData.containsKey(_componentId)) {
            MetaData.ChipMetaData metaData = MetaData.chipMetaData.get(_componentId)
            if(metaData.hideInput.contains(input))
                return  true
        }
        return false
    }
    
    void setState(String input, boolean[] state) {
        
        if(hideInput(input))
            return
        
        input = renameInputIfNeeded(input)
        
        final int pinCount = _inputsToStates.get(input).length()
        char[] stateArray = new char[pinCount]
        
        if (state == null) {
            (0..(pinCount - 1)).each { int index -> stateArray[index] = HIGH_IMPEDANCE }
            _inputsToStates.put(input, new String(stateArray))
            return
        } else {
            (0..(pinCount - 1)).each { int index -> stateArray[index] = UNKNOWN }
        }
        
        (0..(state.length-1)).each {
            int index ->
                stateArray[index] = state[index] ? HIGH : LOW
        }
        
        _inputsToStates.put(input, new String(stateArray).reverse())
    }
}
