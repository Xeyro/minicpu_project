package main.groovy.GUIWrapper.WrapperInterface

/**
 * Wrapped Board
 *
 * Interface that allows the View part of the application to talk to the model without
 * being aware of the underlying implementation. Functions as a wrapper class around
 * the Board interface in the model, which represents a collection of Components and Wires.
 */
public interface WrappedBoard {
    
    /** Get Components:
     * @return The list of Components on this board */
    public List<WrappedComponent> getComponents();
    
    /** Get Wires:
     * @return  The list of Wires on this board */
    public List<WrappedWire> getWires();
    
    public void tick()
    
    public void clockCycle()
}
