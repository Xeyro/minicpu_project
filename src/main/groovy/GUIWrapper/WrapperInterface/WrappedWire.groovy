package main.groovy.GUIWrapper.WrapperInterface

/**
 * Wrapped Wire
 *
 * Interface that allows the View part of the application to talk to the model without
 * being aware of the underlying implementation. Functions as a wrapper class around
 * the Wire class in the model, which represents a physical wire.
 */
public interface WrappedWire {
    
    /** Get Label:
     * @return ??? */
    public String getLabel();
    
    /** Get From Id
     * @return The Component that this wire originates from */
    public String getFromId();
    
    /** Get To Ids:
     * @return The list of Components that this wire goes to */
    public List<String> getToIds();
    
    /** Is High:
     * @return True if the wire is carrying a High signal, false otherwise */
    public boolean isHigh();
    
    
    public void SetEnd(int end, boolean value);
}
