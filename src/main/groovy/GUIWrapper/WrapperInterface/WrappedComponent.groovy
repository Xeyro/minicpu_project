package main.groovy.GUIWrapper.WrapperInterface

import main.groovy.GUIWrapper.ChipState

/**
 * Wrapped Component
 *
 * Interface that allows the View part of the application to talk to the model without
 * being aware of the underlying implementation. Functions as a wrapper class around
 * the Component interface in the model, which represents a physical hardware component.
 */
public interface WrappedComponent {
    
    /** Pin Count:
     * @return Number of pins on the Component */
    public int pinCount();

    /** Max Control Pin :
     * @return Maximum Control pin number or -1 if the Component has no Control pins */
    public int maxControlPin();
    
    /** Max Input Pin:
     * @return Maximum Input pin number or -1 if the Component has no Input pins */
    public int maxInputPin();
    
    /** Max Output Pin:
     * @return Maximum Output pin number or -1 if the Component has no Output pins */
    public int maxOutputPin();
    
    /** Get Pin Label:
     * @return ??? */
    public String getPinLabel(int pinId);
    
    /** Get Id:
     * @return Id of the Component, such as 'U110' */
    public String getId();
    
    /** Get Value:
     * @return Special value for this Component, such as 'HexValue' for a REGISTER Component */
    public String getValue(String key);
    
    /** Get Type:
     * @return Type of the Component, such as 'ADDER' or 'REGISTER' */
    public String getType();
    
    public String getName();

    ChipState getChipState();
}