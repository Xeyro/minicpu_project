package main.groovy.GUIWrapper.WrappedWires

import main.groovy.GUIWrapper.WrapperInterface.WrappedWire
import main.groovy.Wire.Wire

class WrappedWireImpl implements WrappedWire {
    
    private Wire _wire
    private String _inputId
    private List<String> _outputIds
    
    WrappedWireImpl(Wire wire, String inputId, List<String> outputIds) {
        _wire = wire
        _inputId = inputId
        _outputIds = outputIds
    }
    
    String getLabel() { return null }
    
    String getFromId() { _inputId }
    
    List<String> getToIds() { _outputIds }
    
    boolean isHigh() { _wire.getEnd(Wire.OUT) }
    
    void SetEnd(int end, boolean value) {
        _wire.setEnd(end, value)
    }
}
