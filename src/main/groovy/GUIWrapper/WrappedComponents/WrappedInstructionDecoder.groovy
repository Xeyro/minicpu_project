package main.groovy.GUIWrapper.WrappedComponents

import main.groovy.Component.InstructionDecoder
import main.groovy.GUIWrapper.ChipState
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent
import main.groovy.MetaData.MetaData

class WrappedInstructionDecoder extends BaseWrappedComponent implements WrappedComponent {
    
    private InstructionDecoder _instructionDecoder
    
    WrappedInstructionDecoder(InstructionDecoder instructionDecoder) {
        _instructionDecoder = instructionDecoder
        _wrapped = _instructionDecoder
    }
    
    int pinCount() { _instructionDecoder.PIN_COUNT }
    
    int maxControlPin() { _instructionDecoder.CHIP_SELECT }
    
    int maxInputPin() { _instructionDecoder.INSTRUCTION - 1}
    
    int maxOutputPin() { _instructionDecoder.PIN_COUNT - 1 }
    
    String getValue(String key) {
        if(key == "instruction")
            return "${_instructionDecoder.opcode()} [${MetaData.InstructionInfo[_instructionDecoder.opcode()].name}]"
        else if(key == "memory_buffer")
            return "${_instructionDecoder.memoryBuffer()}"
        else if(key == "operand1")
            return "${_instructionDecoder.operand1()}"
        else if(key == "operand2")
            return "${_instructionDecoder.operand2()}"
        else if(key == "state")
            return "${_instructionDecoder.state()}"
        else if(key == "cycles")
            return "${_instructionDecoder.cyclesLeftInState()}"
        else
            super.getValue(key)
    }
    
    @Override
    ChipState getChipState() {
        _chipState = new ChipState(_wrapped.getId())
        _chipState.with {
            addInput("0_MEM_1", 8)
            setState("0_MEM_1", _instructionDecoder.read(_instructionDecoder.MEM_1,8))
    
            addInput("1_MEM_2", 8)
            setState("1_MEM_2", _instructionDecoder.read(_instructionDecoder.MEM_2,8))
    
            addInput("2_MEM_3", 8)
            setState("2_MEM_3", _instructionDecoder.read(_instructionDecoder.MEM_3,8))
    
            addInput("3_MEM_4", 8)
            setState("3_MEM_4", _instructionDecoder.read(_instructionDecoder.MEM_4,8))
    
            addInput("Inst. Length", 16)
            setState("Inst. Length", _instructionDecoder.read(_instructionDecoder.INSTRUCTION_LENGTH,16))
    
            addInput("Offset", 16)
            setState("Offset", _instructionDecoder.read(_instructionDecoder.OFFSET,16))
    
            addInput("Instruction", 16)
            setState("Instruction", _instructionDecoder.read(_instructionDecoder.INSTRUCTION,16))
        }
        return _chipState
    }
}
