package main.groovy.GUIWrapper.WrappedComponents

import main.groovy.Component.RAM
import main.groovy.GUIWrapper.ChipState
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent
import sun.reflect.generics.reflectiveObjects.NotImplementedException

class WrappedRAM extends BaseWrappedComponent implements WrappedComponent {
    
    private RAM _ram

    WrappedRAM(RAM ram) {
        _ram = ram
        _wrapped = _ram
    }
    
    int pinCount() {
        throw new NotImplementedException()
    }
    
    int maxControlPin() {
        throw new NotImplementedException()
    }
    
    int maxInputPin() {
        throw new NotImplementedException()
    }
    
    int maxOutputPin() {
        throw new NotImplementedException()
    }
    
    @Override
    ChipState getChipState() {
        _chipState = new ChipState(_wrapped.getId())
        _chipState.with {
            addInput("CHIP_SELECT", 1)
            setState("CHIP_SELECT", _ram.isHigh(_ram.CHIP_SELECT))
    
            addInput("ADDRESS", _ram.addressSize())
            setState("ADDRESS", _ram.read(_ram.ADDRESS, _ram.addressSize()))
    
            addInput("DATA_IN", 8)
            setState("DATA_IN", _ram.read(_ram.DATA_IN, 8))
    
            addInput("DATA_OUT", 8)
            setState("DATA_OUT", _ram.read(_ram.DATA_OUT, 8))
        }
        return _chipState
    }
}
