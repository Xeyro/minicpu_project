package main.groovy.GUIWrapper.WrappedComponents

import main.groovy.Component.FlagsChip
import main.groovy.GUIWrapper.ChipState
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent

class WrappedFlagsChip extends BaseWrappedComponent implements WrappedComponent {
    
    private FlagsChip _chip
    
    WrappedFlagsChip(FlagsChip flagsChip) {
        _chip = flagsChip
        _wrapped = flagsChip
    }
    
    int pinCount() { _chip.PIN_COUNT }
    
    int maxControlPin() { _chip.INPUT - 1 }
    
    int maxInputPin() { _chip.OUTPUT - 1 }
    
    int maxOutputPin() { _chip.PIN_COUNT - 1 }
    
    @Override
    ChipState getChipState() {
        _chipState = new ChipState(_wrapped.getId())
        _chipState.with {
            addInput("INPUT", _chip.inputSize())
            setState("INPUT", _chip.read(_chip.INPUT, _chip.inputSize()))
    
            addInput("FLAG_ZERO", 1)
            setState("FLAG_ZERO", _chip.read(_chip.FLAG_ZERO, 1))
    
            addInput("FLAG_OVERFLOW", 1)
            setState("FLAG_OVERFLOW", _chip.read(_chip.FLAG_OVERFLOW, 1))
    
            addInput("FLAG_NEGATIVE", 1)
            setState("FLAG_NEGATIVE", _chip.read(_chip.FLAG_NEGATIVE, 1))
    
            addInput("FLAG_CARRY", 1)
            setState("FLAG_CARRY", _chip.read(_chip.FLAG_CARRY, 1))
        }
        return _chipState
    }
}
