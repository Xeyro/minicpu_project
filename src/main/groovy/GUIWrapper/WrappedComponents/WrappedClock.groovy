package main.groovy.GUIWrapper.WrappedComponents

import main.groovy.Component.Clock
import main.groovy.GUIWrapper.ChipState
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent

class WrappedClock extends BaseWrappedComponent implements WrappedComponent {
    
    private Clock _clock
    
    WrappedClock(Clock clock) {
        _clock = clock
        _wrapped = clock
    }
    
    int pinCount() { return 1 }
    
    int maxControlPin() { return -1 }
    
    int maxInputPin() { return -1 }
    
    int maxOutputPin() { return 0 }
}
