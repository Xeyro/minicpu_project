package main.groovy.GUIWrapper.WrappedComponents

import main.groovy.Component.LogicChip
import main.groovy.GUIWrapper.ChipState
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent

class WrappedLogicChip extends BaseWrappedComponent implements WrappedComponent {
    
    private LogicChip _logicChip
    
    WrappedLogicChip(LogicChip logicChip) {
        _logicChip = logicChip
        _wrapped = _logicChip
    }
    
    int pinCount() { _logicChip.PIN_COUNT }
    
    int maxControlPin() { _logicChip.CHIP_SELECT }
    
    int maxInputPin() { _logicChip.OUTPUT - 1 }
    
    int maxOutputPin() { _logicChip.PIN_COUNT - 1 }
    
    @Override
    ChipState getChipState() {
        _chipState = new ChipState(_wrapped.getId())
        _chipState.with {
            addInput("CHIP_SELECT", 1)
            setState("CHIP_SELECT", _logicChip.isHigh(_logicChip.CHIP_SELECT))
    
            addInput("INPUT_A", _logicChip.operandSize())
            setState("INPUT_A", _logicChip.read(_logicChip.INPUT_A, _logicChip.operandSize()))
    
            addInput("INPUT_B", _logicChip.operandSize())
            setState("INPUT_B", _logicChip.read(_logicChip.INPUT_B, _logicChip.operandSize()))
    
            addInput("OUTPUT", _logicChip.operandSize())
            setState("OUTPUT", _logicChip.read(_logicChip.OUTPUT, _logicChip.operandSize()))
        }
        return _chipState
    }
}
