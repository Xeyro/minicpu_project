package main.groovy.GUIWrapper.WrappedComponents

import main.groovy.Component.Component
import main.groovy.GUIWrapper.ChipState
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent

class BaseWrappedComponent implements WrappedComponent {
    
    protected Component _wrapped
    protected ChipState _chipState
    
    int pinCount() { -1 }
    int maxControlPin() { -1 }
    int maxInputPin() { -1 }
    int maxOutputPin() { -1 }
    String getPinLabel(int pinId) { "---" }
    String getId() { _wrapped.getId() }
    String getValue(String key) { "---" }
    String getType() { _wrapped.getComponentType().toString() }
    String getName() { _wrapped.getName() }
    ChipState getChipState() { new ChipState(_wrapped.getId()) }
}
