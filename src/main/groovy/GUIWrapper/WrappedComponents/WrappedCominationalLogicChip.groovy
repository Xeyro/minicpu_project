package main.groovy.GUIWrapper.WrappedComponents

import main.groovy.Component.CombinationalLogicChip
import main.groovy.Component.ComponentType
import main.groovy.GUIWrapper.ChipState
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent

class WrappedCominationalLogicChip extends BaseWrappedComponent implements WrappedComponent {
    
    CombinationalLogicChip _chip
    
    WrappedCominationalLogicChip(CombinationalLogicChip chip) {
        _chip = chip
        _wrapped = chip
    }
    
    int pinCount() { _chip.PIN_COUNT }
    
    int maxControlPin() { _chip.CHIP_SELECT }
    
    int maxInputPin() { _chip.OUTPUT - 1 }
    
    int maxOutputPin() { _chip.PIN_COUNT - 1 }
    
    @Override
    ChipState getChipState() {
        _chipState = new ChipState(_wrapped.getId())
        _chipState.with {
            addInput("CHIP_SELECT", 1)
            setState("CHIP_SELECT", _chip.isHigh(_chip.CHIP_SELECT))
    
            int inputSize = _chip.getComponentType() == ComponentType.DECODER ? _chip.encodedDataSize() : _chip.decodedDataSize()
            int outputSize = _chip.getComponentType() == ComponentType.DECODER ? _chip.decodedDataSize() : _chip.encodedDataSize()
            addInput("INPUT", inputSize)
            setState("INPUT", _chip.read(_chip.INPUT, inputSize))
            
            addInput("OUTPUT", outputSize)
            setState("OUTPUT", _chip.read(_chip.OUTPUT, outputSize))
        }
        return _chipState
    }
}
