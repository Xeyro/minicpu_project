package main.groovy.GUIWrapper.WrappedComponents

import main.groovy.Component.Register
import main.groovy.GUIWrapper.ChipState
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent

class WrappedRegister extends BaseWrappedComponent implements WrappedComponent {
    
    private Register _register
    
    WrappedRegister(Register register) {
        _register = register
        _wrapped = _register
    }
    
    int pinCount() { _register.PIN_COUNT }
    
    int maxControlPin() { _register.CLOCK }
    
    int maxInputPin() { _register.OUTPUT - 1}
    
    int maxOutputPin() { _register.PIN_COUNT - 1 }
    
    String getValue(String key) {
        if(key == "internal_value")
            return _register.getInternalValue().toString()
        else
            super.getValue(key)
    }
    
    @Override
    ChipState getChipState() {
        _chipState = new ChipState(_wrapped.getId())
        _chipState.with {
            addInput("CHIP_SELECT", 1)
            setState("CHIP_SELECT", _register.isHigh(_register.CHIP_SELECT))
        
            addInput("CLOCK", 1)
            setState("CLOCK", _register.isHigh(_register.CLOCK))
        
            addInput("INPUT", _register.inputSize())
            setState("INPUT", _register.read(_register.INPUT, _register.inputSize()))
        
            addInput("OUTPUT", _register.inputSize())
            setState("OUTPUT", _register.read(_register.OUTPUT, _register.inputSize()))
        }
        return _chipState
    }
}
