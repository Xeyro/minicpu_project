package main.groovy.GUIWrapper.WrappedComponents

import main.groovy.Component.Adder
import main.groovy.Component.Clock
import main.groovy.Component.CombinationalLogicChip
import main.groovy.Component.Component
import main.groovy.Component.DataSelector
import main.groovy.Component.FlagsChip
import main.groovy.Component.InstructionDecoder
import main.groovy.Component.LogicChip
import main.groovy.Component.RAM
import main.groovy.Component.Register
import main.groovy.Component.U114Demulitplexer
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent

class WrappedComponentFactory {
    
    static WrappedComponent wrap(Component component) {
        if(component instanceof Adder)
            return new WrappedAdder((Adder)component)
        
        if(component instanceof Clock)
            return new WrappedClock((Clock)component)
        
        if(component instanceof DataSelector)
            return new WrappedDataSelector((DataSelector)component)
        
        if(component instanceof LogicChip)
            return new WrappedLogicChip((LogicChip)component)
        
        if(component instanceof Register)
            return new WrappedRegister((Register)component)
        
        if(component instanceof InstructionDecoder)
            return new WrappedInstructionDecoder((InstructionDecoder)component)
    
        if(component instanceof CombinationalLogicChip)
            return new WrappedCominationalLogicChip((CombinationalLogicChip)component)
    
        if(component instanceof RAM)
            return new WrappedRAM((RAM)component)
        
        if(component instanceof U114Demulitplexer)
            return new WrappedU114Demultiplexer((U114Demulitplexer)component)
    
        if(component instanceof FlagsChip)
            return new WrappedFlagsChip((FlagsChip)component)
        
        System.err.println("Warning, unknown Component type for: ${component.toString()}")
        
        return null
    }
    
}
