package main.groovy.GUIWrapper.WrappedComponents

import main.groovy.Component.Adder
import main.groovy.GUIWrapper.ChipState
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent

class WrappedAdder extends BaseWrappedComponent implements WrappedComponent {
    
    private Adder _adder
    
    WrappedAdder(Adder adder) {
        _adder = adder
        _wrapped = adder
        
    }
    
    int pinCount() { _adder.PIN_COUNT }
    
    int maxControlPin() { _adder.CHIP_SELECT }
    
    int maxInputPin() { _adder.OUTPUT - 1 }
    
    int maxOutputPin() { _adder.PIN_COUNT - 1 }
    
    @Override
    ChipState getChipState() {
        _chipState = new ChipState(_wrapped.getId())
        _chipState.with {
            addInput("CHIP_SELECT", 1)
            setState("CHIP_SELECT", _adder.isHigh(_adder.CHIP_SELECT))
            
            addInput("INPUT_A", _adder.operandSize())
            setState("INPUT_A", _adder.read(_adder.INPUT_A, _adder.operandSize()))
            
            addInput("INPUT_B", _adder.operandSize())
            setState("INPUT_B", _adder.read(_adder.INPUT_B, _adder.operandSize()))
    
            addInput("SUBTRACT", 1)
            setState("SUBTRACT", _adder.isHigh(_adder.SUBTRACT))
            
            addInput("CARRY_IN", 1)
            setState("CARRY_IN", _adder.isHigh(_adder.CARRY_IN))
            
            addInput("OUTPUT", _adder.operandSize())
            setState("OUTPUT", _adder.read(_adder.OUTPUT, _adder.operandSize()))
            
            addInput("CARRY_OUT", 1)
            setState("CARRY_OUT", _adder.isHigh(_adder.CARRY_OUT))
        }
        return _chipState
    }
}
