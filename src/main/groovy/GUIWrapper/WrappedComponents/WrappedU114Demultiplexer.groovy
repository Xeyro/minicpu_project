package main.groovy.GUIWrapper.WrappedComponents

import main.groovy.Component.DataSelector
import main.groovy.Component.U114Demulitplexer
import main.groovy.GUIWrapper.ChipState
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent

class WrappedU114Demultiplexer extends  BaseWrappedComponent implements WrappedComponent {
    
    private U114Demulitplexer _demultiplexer
    
    WrappedU114Demultiplexer(U114Demulitplexer demulitplexer) {
        _demultiplexer = demulitplexer
        _wrapped = _demultiplexer
    }
    
    int pinCount() { _demultiplexer.PIN_COUNT }
    
    int maxControlPin() { _demultiplexer.INPUT_A - 1 }
    
    int maxInputPin() { _demultiplexer.OUTPUT_R1 - 1 }
    
    int maxOutputPin() { _demultiplexer.PIN_COUNT - 1 }
    
    @Override
    ChipState getChipState() {
        _chipState = new ChipState(_wrapped.getId())
        _chipState.with {
            addInput("OE_A", 1)
            setState("OE_A", _demultiplexer.isHigh(_demultiplexer.OUTPUT_ENABLE_A))
    
            addInput("OE_B", 1)
            setState("OE_B", _demultiplexer.isHigh(_demultiplexer.OUTPUT_ENABLE_B))
    
            addInput("SELECT_A", U114Demulitplexer.SelectSize)
            setState("SELECT_A", _demultiplexer.read(_demultiplexer.SELECT_A, U114Demulitplexer.SelectSize))
    
            addInput("SELECT_B", U114Demulitplexer.SelectSize)
            setState("SELECT_B", _demultiplexer.read(_demultiplexer.SELECT_B, U114Demulitplexer.SelectSize))
    
            addInput("INPUT_A", U114Demulitplexer.InputSize)
            setState("INPUT_A", _demultiplexer.read(_demultiplexer.INPUT_A, U114Demulitplexer.InputSize))
    
            addInput("INPUT_B", U114Demulitplexer.InputSize)
            setState("INPUT_B", _demultiplexer.read(_demultiplexer.INPUT_B, U114Demulitplexer.InputSize))
    
            addInput("OUTPUT_R1", U114Demulitplexer.InputSize)
            setState("OUTPUT_R1", _demultiplexer.read(_demultiplexer.OUTPUT_R1, U114Demulitplexer.InputSize))
    
            addInput("OUTPUT_R2", U114Demulitplexer.InputSize)
            setState("OUTPUT_R2", _demultiplexer.read(_demultiplexer.OUTPUT_R2, U114Demulitplexer.InputSize))
    
            addInput("OUTPUT_R3", U114Demulitplexer.InputSize)
            setState("OUTPUT_R3", _demultiplexer.read(_demultiplexer.OUTPUT_R3, U114Demulitplexer.InputSize))
    
            addInput("OUTPUT_R4", U114Demulitplexer.InputSize)
            setState("OUTPUT_R4", _demultiplexer.read(_demultiplexer.OUTPUT_R4, U114Demulitplexer.InputSize))
            
        }
        return _chipState
    }
    
}
