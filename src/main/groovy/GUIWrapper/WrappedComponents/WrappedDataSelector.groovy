package main.groovy.GUIWrapper.WrappedComponents

import main.groovy.Component.ComponentType
import main.groovy.Component.DataSelector
import main.groovy.GUIWrapper.ChipState
import main.groovy.GUIWrapper.WrapperInterface.WrappedComponent

class WrappedDataSelector extends BaseWrappedComponent implements WrappedComponent {
    
    private DataSelector _dataSelector
    
    WrappedDataSelector(DataSelector dataSelector) {
        _dataSelector = dataSelector
        _wrapped = _dataSelector
    }
    
    int pinCount() { _dataSelector.PIN_COUNT }
    
    int maxControlPin() { _dataSelector.INPUT - 1 }
    
    int maxInputPin() { _dataSelector.OUTPUT - 1 }
    
    int maxOutputPin() { _dataSelector.PIN_COUNT - 1 }
    
    @Override
    ChipState getChipState() {
        _chipState = new ChipState(_wrapped.getId())
        _chipState.with {
            addInput("CHIP_SELECT", 1)
            setState("CHIP_SELECT", _dataSelector.isHigh(_dataSelector.CHIP_SELECT))
    
            addInput("SELECTOR", _dataSelector.selects())
            setState("SELECTOR", _dataSelector.read(_dataSelector.SELECTOR, _dataSelector.selects()))
    
            if(_dataSelector.getComponentType() == ComponentType.MULTIPLEXER) {
                (0..(_dataSelector.ioCount()-1)).each {
                    int input ->
                        addInput("INPUT_${input}", _dataSelector.inputSize())
                        setState("INPUT_${input}", _dataSelector.read(_dataSelector.INPUT + (_dataSelector.inputSize() * input), _dataSelector.inputSize()))
                }
                addInput("OUTPUT", _dataSelector.inputSize())
                setState("OUTPUT", _dataSelector.read(_dataSelector.OUTPUT, _dataSelector.inputSize()))
            } else {
                (0..(_dataSelector.ioCount()-1)).each {
                    int output ->
                        addInput("OUTPUT_${output}", _dataSelector.inputSize())
                        setState("OUTPUT_${output}", _dataSelector.read(_dataSelector.OUTPUT + (_dataSelector.inputSize() * output), _dataSelector.inputSize()))
                }
                addInput("OUTPUT", _dataSelector.inputSize())
                setState("OUTPUT", _dataSelector.read(_dataSelector.OUTPUT, _dataSelector.inputSize()))
            }
        }
        return _chipState
    }
    
}
