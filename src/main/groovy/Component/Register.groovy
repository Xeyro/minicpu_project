package main.groovy.Component

import main.groovy.Wire.Wire

/**
 * Register:
 * Represents a hardware register. Can represent a general purpose register, flag register,
 * instruction pointer, etc.
 */
class Register extends BaseComponent implements Component {
    
    public final int CHIP_SELECT    = 0
    public final int CLOCK          = 1
    public final int INPUT
    public final int OUTPUT
    public final int PIN_COUNT

    private int _inputSize
    private int _internalValue

    int inputSize() { _inputSize }
    int internalValue() { _internalValue }
    
    Register(int inputSize) {
        _inputSize = inputSize
        
        INPUT = CLOCK + 1
        OUTPUT = INPUT + _inputSize
        PIN_COUNT = OUTPUT + _inputSize + 1

        _pins = new Wire[PIN_COUNT]
        _end = new int[PIN_COUNT]
        
        // Randomize initial value - for no particular reason
        Random random = new Random()
        _internalValue = random.nextInt(((int)Math.pow(2, _inputSize)) - 1)
        _componentType = ComponentType.REGISTER
        _name = "Register"
    }
    
    @Override
    void process() {
        if(isHigh(CHIP_SELECT)) {
            boolean[] input = new boolean[_inputSize]
            (0..(_inputSize - 1)).each {
                int offset ->
                    input[offset] = isHigh(INPUT + offset)
            }
            _internalValue = binaryToDecimal(input)
        }

        boolean[] output = decimalToBinary(_internalValue)
        
        (0..(_inputSize-1)).each{
            int offset ->
                setPin(OUTPUT + offset, output[offset])
        }
    }
    
    String getInternalValue() { _internalValue.toString() }
}