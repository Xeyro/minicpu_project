package main.groovy.Component

import main.groovy.Wire.Wire

/**
 * Component:
 * Interface for all electrical components
 */
interface Component {
    
    /**
     * Process:
     * Represents a single operation for this component that reads signals
     * from this components data and moves them its outputs
     */
    void process()
    
    /**
     * Connect:
     * Used to connect the given wire to this component
     * @param wire  - Wire to connect
     * @param pin   - which pin on the component to connect the wire to
     * @param end   - which end of the wire to connect
     */
    void connect(Wire wire, int pin, int end)
    
    /**
     * Get Ends:
     * @return      - array of integers representing which of the wire each pin is connected to
     */
    int[] getEnds()
    
    /**
     * Get Wires:
     * @return      - array of wires that each pin is connected to
     */
    Wire[] getWires()
    
    /**
     * Get Id:
     * @return      - Id of this Component
     */
    String getId()
    
    /**
     * Set Id:
     */
    void setId(String label)
    
    String getName()
    
    void setName(String name)
    
    ComponentType getComponentType()
    
    void setComponentType(ComponentType type)
}