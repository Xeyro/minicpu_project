package main.groovy.Component

import main.groovy.Wire.Wire

/**
 * Logic Chip
 * Simulates a hardware chip that applies a single logic function
 * such as AND or XOR to all of its data and gives an output
 */
class LogicChip extends BaseComponent implements Component {
    
    public final int CHIP_SELECT   = 0
    public final int INPUT_A
    public final int INPUT_B
    public final int OUTPUT
    public final int PIN_COUNT
    
    private int _operandSize
    private def _logicFunction

    int operandSize() { _operandSize }
    
    private LogicChip(int operandSize, Closure logicFunction) {
        _operandSize = operandSize
        _logicFunction = logicFunction

        INPUT_A = 1
        INPUT_B = INPUT_A + _operandSize
        OUTPUT = INPUT_B + _operandSize
        PIN_COUNT = OUTPUT + _operandSize + 1
        
        _pins = new Wire[PIN_COUNT]
        _end = new int[PIN_COUNT]
    }
    
    @Override
    void process() {
        if(CHIP_SELECT == false)
            return
        
        (0..(_operandSize - 1)).each {
            int offset ->
                //noinspection GroovyAssignabilityCheck
                setPin(
                    OUTPUT + offset,
                    _logicFunction(
                        isHigh(INPUT_A + offset),
                        isHigh(INPUT_B + offset)
                    )
                )
        }
    }

    static class Builder {
    
        private static def FUNCTION_NO_OP = {
            boolean a, boolean b ->
                println("NO OP")
                return false
        }
    
        private static def FUNCTION_AND = {
            boolean a, boolean b ->
                a & b
        }
    
        private static def FUNCTION_OR = {
            boolean a, boolean b ->
                a | b
        }
    
        private static def FUNCTION_XOR = {
            boolean a, boolean b ->
                a ^ b
        }
    
        private static def FUNCTION_NOT = {
            boolean a, boolean b ->
                !a
        }
        
        private int _operandSize
        private def _logicFunction

        
        Builder() {
            _operandSize = 1
            _logicFunction = FUNCTION_NO_OP
        }

        Builder operandSize(int size) { _operandSize = size; return this}
        
        Builder functionAnd() { _logicFunction = FUNCTION_AND; return this }
        Builder functionOr()  { _logicFunction = FUNCTION_OR; return this }
        Builder functionXor() { _logicFunction = FUNCTION_XOR; return this }
        Builder functionNot() { _logicFunction = FUNCTION_NOT; return this }
    
        @SuppressWarnings("GroovyAssignabilityCheck")
        LogicChip build() {
            LogicChip chip = new LogicChip(_operandSize, _logicFunction)
            if(_logicFunction == FUNCTION_AND) {
                chip.setComponentType(ComponentType.AND_GATE)
                chip.setName("AND Gate")
            } else if(_logicFunction == FUNCTION_OR) {
                chip.setComponentType(ComponentType.OR_GATE)
                chip.setName("OR Gate")
            } else if(_logicFunction == FUNCTION_XOR) {
                chip.setComponentType(ComponentType.XOR_GATE)
                chip.setName("XOR Gate")
            } else if(_logicFunction == FUNCTION_NOT) {
                chip.setComponentType(ComponentType.NOT_GATE)
                chip.setName("NOT Gate")
            }
            
            return chip
        }
    }
    

}
