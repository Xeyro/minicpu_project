package main.groovy.Component

import main.groovy.Wire.Wire

/**
 * Adder:
 * Simulates a hardware adder.
 */
class Adder extends BaseComponent implements Component {
    
    /** MetaData representing hardware pins */
    public final int CHIP_SELECT    = 0
    public final int CARRY_IN       = 1
    public final int SUBTRACT       = 2
    public final int INPUT_A
    public final int INPUT_B
    public final int OUTPUT
    public final int CARRY_OUT
    public final int PIN_COUNT

    /** Number of bits used for a single input. */
    private final int _operandSize
    
    int operandSize() { _operandSize }
    
    /**
     * Creates a new adder that expects data of size operandBits.
     * @param operandBits
     */
    Adder(int operandBits) {
        this._operandSize = operandBits
        
        INPUT_A     = SUBTRACT + 1
        INPUT_B     = INPUT_A + operandBits
        OUTPUT      = INPUT_B + operandBits
        CARRY_OUT   = OUTPUT + operandBits
        PIN_COUNT   = CARRY_OUT + 1
        
        _pins = new Wire[PIN_COUNT]
        _end = new int[PIN_COUNT]
        _componentType = ComponentType.ADDER_SUBTRACTOR
    }

    @Override
    /**
     * Process:
     * Reads from both data and the carry in, does binary addition
     * and then writes to the output and carry out
     */
    void process() {
        // If chip not selected, do nothing...
        if(isLow(CHIP_SELECT))
            return
        
        boolean[] operand2 = read(INPUT_B, _operandSize)
        boolean carry = isHigh(CARRY_IN)
        
        if(isHigh(SUBTRACT)){
            operand2 = operand2.collect{ !it }
            carry = true
        }
        
        // Read data and do addition
        boolean[] result = add(
            read(INPUT_A, _operandSize),
            operand2,
            carry
        )
        
        // Write output
        write(OUTPUT, _operandSize, result)
        
        def carryOut = result[result.size()-1]
        write(CARRY_OUT, 1, carryOut)
    }
    
    /**
     * Add:
     * Takes two operands a and b or size _operandSize and a carry in and
     * adds them producing and array of size _operandSize + 1
     * The last value in the array is the carry out
     * @param a         - first number to add
     * @param b         - second number to add
     * @param carryIn   - carry in
     * @return          - an array of size a + 1, the extra bit representing the carry out
     */
    static boolean[] add(boolean[] a, boolean[] b, boolean carryIn) {
        boolean[] result = new boolean[a.size() + 1]
        boolean carry = carryIn
        
        for(def i = 0; i < a.size(); i++) {
            def aXORb = a[i] ^ b[i]
            def aANDb = a[i] & b[i]
            
            result[i] = aXORb ^ carry
            carry = (aXORb & carry) | aANDb
        }
        
        result[result.size()-1] = carry
        return result
    }
}
