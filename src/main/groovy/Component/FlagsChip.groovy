package main.groovy.Component

import main.groovy.Wire.Wire

class FlagsChip extends BaseComponent implements Component {

    public final int CARRY_IN   = 0
    public final int INPUT      = 1
    public final int PIN_COUNT
    
    public final int OUTPUT
    public final int FLAG_ZERO
    public final int FLAG_OVERFLOW
    public final int FLAG_NEGATIVE
    public final int FLAG_CARRY
    
    private int _inputSize
    
    int inputSize() { _inputSize }
    
    FlagsChip(int inputSize) {
        _inputSize = inputSize
        
        FLAG_ZERO = INPUT + _inputSize
        OUTPUT = FLAG_ZERO
        FLAG_OVERFLOW = FLAG_ZERO + 1
        FLAG_NEGATIVE = FLAG_OVERFLOW + 1
        FLAG_CARRY = FLAG_NEGATIVE + 1
        PIN_COUNT = FLAG_CARRY + 1
        
        _pins = new Wire[PIN_COUNT]
        _end = new int[PIN_COUNT]
    }
    
    void process() {
        boolean isZero = true
        (0..(_inputSize-1)).each{
            int index ->
                if(isHigh(INPUT + index))
                    isZero = false
        }
        
        
        
        setPin(FLAG_ZERO, isZero)
        setPin(FLAG_OVERFLOW, isHigh(INPUT + _inputSize - 1) && isHigh(CARRY_IN))
        setPin(FLAG_NEGATIVE, isHigh(INPUT + _inputSize - 1))
        setPin(FLAG_CARRY, isHigh(CARRY_IN))
    }
}