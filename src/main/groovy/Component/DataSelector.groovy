package main.groovy.Component

import main.groovy.Wire.Wire

class DataSelector extends BaseComponent implements Component {
    
    public static final def FUNCTION_MULTIPLEX = {
        DataSelector chip ->
            final int selectedInput =
                chip.INPUT +
                ( chip._inputSize * chip.binaryToDecimal(chip.read(chip.SELECTOR, chip._selects) )
            )
            (0..(chip._inputSize-1)).each{
                int offset ->
                    chip.setPin(
                        chip.OUTPUT + offset ,
                        chip.isHigh(selectedInput + offset)
                    )
            }
    }
    
    public static final def FUNCTION_DEMULTIPLEX = {
        DataSelector chip ->
            final int selectedOutput = chip.binaryToDecimal(
                chip.read(chip.SELECTOR, chip._selects)
            )
            (chip.OUTPUT..(chip.PIN_COUNT - 1)).each {
                chip.setPin(it, false)
            }
            (0..(chip._inputSize-1)).each{
                int offset ->
                    chip.setPin(
                        chip.OUTPUT + (selectedOutput * chip._inputSize) + offset,
                        chip.isHigh(chip.INPUT + offset)
                    )
            }
    }
    
    public final int CHIP_SELECT   = 0
    public final int SELECTOR
    public final int INPUT
    public final int OUTPUT
    public final int PIN_COUNT
    
    /** Number of pins for each input/output */
    private final int _inputSize
    /** Number of IO pins. For a multiplexer, this is the input count, for a demultiplexer it is the output count */
    private final int _ioCount
    /** Number of select pins */
    private final int _selects
    /** Logic function used by the chip to map inputs to outputs (Multiplex or Demultiplex)*/
    private final def _logicFunction
    
    int inputSize() { _inputSize }
    int ioCount() { _ioCount }
    int selects() { _selects }
    
    
    private DataSelector(int selects, int ioCount, int inputSize, def logicFunction, boolean isMultiplexer) {
        _ioCount        = ioCount
        _selects        = selects
        _inputSize      = inputSize
        _logicFunction  = logicFunction
        
        SELECTOR        = CHIP_SELECT + 1
        INPUT           = SELECTOR + _selects
    
        if(isMultiplexer) {
            OUTPUT      = (INPUT + (_ioCount * _inputSize))
            PIN_COUNT   = (OUTPUT + _inputSize)
            _componentType = ComponentType.MULTIPLEXER
        } else {
            OUTPUT      = (INPUT + _inputSize)
            PIN_COUNT   = (OUTPUT + (_ioCount * _inputSize))
            _componentType = ComponentType.DEMULTIPLEXER
        }
        
        _pins = new Wire[PIN_COUNT]
        _end = new int[PIN_COUNT]
    }
    
    @Override
    void process() {
        if(isLow(CHIP_SELECT))
            return

        _logicFunction(this)
    }
    
    @Override
    String toString() {
        String str = "--- Chip ---\n"
        str += "Type: multiplexer\n"

        _pins.eachWithIndex {
            Wire entry, int i ->
                
                    String label = "CH_SEL"
                    if(i >= SELECTOR)
                        label = "SELECT"
                    if(i >= INPUT)
                        label = " INPUT"
                    if(i >= OUTPUT)
                        label = "OUTPUT"
                if(isConnected(i)) {
                    str += "${label} :\t ${isHigh(i)}\n"
                }  else {
                    str += "${label} :\t---\n"
                }
                
        }
        return str
    }
    
    static class Builder {
    
        private int _ioCount
        private int _selects
        private int _inputSize
        
        Builder() {
            _ioCount = 1
            _selects = 1
            _inputSize = 1
        }
    
        Builder selects(int selects) {
            if (selects < 1)
                return this
        
            _selects = selects
        
            final int maxData = (int) (Math.pow(selects, 2))
            if (_ioCount > maxData)
                _ioCount = maxData
            return this
        }
    
        Builder ioCount(int ioCount) {
            if (ioCount < 1)
                return this
        
            _ioCount = ioCount
        
            final int minSelects = (int) (Math.log(ioCount) / Math.log(2))
            if (_selects < minSelects)
                _selects = minSelects
            return this
        }
        
        Builder inputSize(int size) { _inputSize = size; return this }
    
        DataSelector buildMultiplexer() {
            return new DataSelector(_selects, _ioCount, _inputSize, FUNCTION_MULTIPLEX, true)
        }
    
        DataSelector buildDemultiplexer() {
            return new DataSelector(_selects, _ioCount, _inputSize, FUNCTION_DEMULTIPLEX, false)
        }
    }
}