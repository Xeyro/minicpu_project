package main.groovy.Component

enum ComponentType {
    UNKNOWN,
    REGISTER,
    DEMULTIPLEXER,
    MULTIPLEXER,
    ADDER_SUBTRACTOR,
    AND_GATE,
    OR_GATE,
    XOR_GATE,
    NOT_GATE,
    CLOCK,
    ENCODER,
    DECODER,
    INSTRUCTION_DECODER,
    RAM
}
