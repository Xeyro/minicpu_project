package main.groovy.Component

import main.groovy.Wire.Wire

class CombinationalLogicChip extends BaseComponent implements Component {

    public static final def FUNCTION_ENCODE = {
        CombinationalLogicChip chip ->
            final boolean[] inputs = chip.read(chip.INPUT, chip._decodedDataSize)
            final int highestInput = inputs.findLastIndexOf { it }
            final boolean[] encoded = chip.decimalToBinary(highestInput)
            chip.write(chip.OUTPUT, chip._encodedDataSize, encoded)
    }
    
    public static final def FUNCTION_DECODE = {
        CombinationalLogicChip chip ->
            final int outputPin = binaryToDecimal(
                chip.read(chip.INPUT, chip._encodedDataSize)
            )
            
            (0..(chip.decodedDataSize()-1)).each{
                int offset ->
                    if(offset == outputPin)
                        chip.setPin(chip.OUTPUT + offset, true)
                    else
                        chip.setPin(chip.OUTPUT + offset, false)
                    
            }
    }

    public final int CHIP_SELECT    = 0
    public final int INPUT
    public final int OUTPUT
    public final int PIN_COUNT
    
    private int _encodedDataSize
    private int _decodedDataSize
    private def _logicFunction
    
    int encodedDataSize() { _encodedDataSize }
    int decodedDataSize() { _decodedDataSize }
    
    private CombinationalLogicChip(int encodedDataSize, def logicFunction, boolean isDecoder) {
        _encodedDataSize = encodedDataSize
        _decodedDataSize = (int)Math.pow(2, _encodedDataSize)
        _logicFunction = logicFunction
        
        INPUT = CHIP_SELECT + 1
        
        if(isDecoder) {
            OUTPUT = INPUT + _encodedDataSize
            PIN_COUNT = OUTPUT + _decodedDataSize
            _componentType = ComponentType.DECODER
            _name = "Decoder"
        } else {
            OUTPUT = INPUT + _decodedDataSize
            PIN_COUNT = OUTPUT + _encodedDataSize
            _componentType = ComponentType.ENCODER
            _name = "Encoder"
        }

        _pins = new Wire[PIN_COUNT]
        _end = new int[PIN_COUNT]
    }
    
    @Override
    void process() {
        if(isLow(CHIP_SELECT))
            return
        
        _logicFunction(this)
    }
    
    static class Builder {
    
        private int _encodedDataSize
        
        Builder encodedDataSize(int size) { _encodedDataSize = size; return this }
        
        CombinationalLogicChip buildEncoder() {
            return new CombinationalLogicChip(_encodedDataSize, FUNCTION_ENCODE, false)
        }
    
        CombinationalLogicChip buildDecoder() {
            return new CombinationalLogicChip(_encodedDataSize, FUNCTION_DECODE, true)
        }
    }
}