package main.groovy.Component

import main.groovy.Wire.UniDirectionalWire
import main.groovy.Wire.Wire

class U114Demulitplexer extends BaseComponent implements Component {

    public final int OUTPUT_ENABLE_A = 0
    public final int OUTPUT_ENABLE_B = 1
    public final int SELECT_A
    public final int SELECT_B
    public final int INPUT_A
    public final int INPUT_B
    public final int OUTPUT_R1
    public final int OUTPUT_R2
    public final int OUTPUT_R3
    public final int OUTPUT_R4
    public final int PIN_COUNT

    static final int InputSize  = 8
    static final int SelectSize = 2
    
    private DataSelector _demuxA
    private DataSelector _demuxB
    private List<Wire> _internalWires
    
    private Wire[] _demuxAInputs
    private Wire[] _demuxASelects
    private Wire   _demuxAChipSelect
    private Wire[] _demuxAOutputs
    private Wire[] _demuxBInputs
    private Wire[] _demuxBSelects
    private Wire   _demuxBChipSelect
    private Wire[] _demuxBOutputs
    
    U114Demulitplexer() {
        
        SELECT_A    = OUTPUT_ENABLE_B + 1
        SELECT_B    = SELECT_A + SelectSize
        INPUT_A     = SELECT_B + SelectSize
        INPUT_B     = INPUT_A + InputSize
        OUTPUT_R1   = INPUT_B + InputSize
        OUTPUT_R2   = OUTPUT_R1 + InputSize
        OUTPUT_R3   = OUTPUT_R2 + InputSize
        OUTPUT_R4   = OUTPUT_R3 + InputSize
        PIN_COUNT   = OUTPUT_R4 + InputSize + 1
        
        _pins = new Wire[PIN_COUNT]
        _end = new int[PIN_COUNT]
        _internalWires = new ArrayList<>()
        
        _demuxA = new DataSelector.Builder()
            .ioCount(4)
            .inputSize(8)
            .buildDemultiplexer()
    
        _demuxAChipSelect = new UniDirectionalWire()
        _demuxA.connect(_demuxAChipSelect, _demuxA.CHIP_SELECT, Wire.OUT)
        _internalWires.add(_demuxAChipSelect)
        _demuxAInputs = new Wire[8]

        (0..7).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                _demuxA.connect(wire, _demuxA.INPUT + index, Wire.OUT)
                _demuxAInputs[index] = wire
                _internalWires.add(wire)
        }
        
        _demuxASelects = new Wire[2]
        (0..1).each {
            int index ->
                Wire wire = new UniDirectionalWire()
                _demuxA.connect(wire, _demuxA.SELECTOR + index, Wire.OUT)
                _demuxASelects[index] = wire
                _internalWires.add(wire)
        }
        
        _demuxB = new DataSelector.Builder()
            .ioCount(4)
            .inputSize(8)
            .buildDemultiplexer()
    
        _demuxBChipSelect = new UniDirectionalWire()
        _demuxB.connect(_demuxBChipSelect, _demuxB.CHIP_SELECT, Wire.OUT)
        _internalWires.add(_demuxBChipSelect)
        _demuxBInputs = new Wire[8]
    
        (0..7).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                _demuxB.connect(wire, _demuxB.INPUT + index, Wire.OUT)
                _demuxBInputs[index] = wire
                _internalWires.add(wire)
        }
    
        _demuxBSelects = new Wire[2]
        (0..1).each {
            int index ->
                Wire wire = new UniDirectionalWire()
                _demuxB.connect(wire, _demuxB.SELECTOR + index, Wire.OUT)
                _demuxBSelects[index] = wire
                _internalWires.add(wire)
        }

        _demuxAOutputs = new Wire[32]
        _demuxBOutputs = new Wire[32]
        
        (0..31).each{
            int index ->
                Wire demuxAOut = new UniDirectionalWire()
                Wire demuxBOut = new UniDirectionalWire()
                _demuxAOutputs[index] = demuxAOut
                _demuxBOutputs[index] = demuxBOut
                _demuxA.connect(demuxAOut, _demuxA.OUTPUT + index, Wire.IN)
                _demuxB.connect(demuxBOut, _demuxB.OUTPUT + index, Wire.IN)
                _internalWires.add(demuxAOut)
                _internalWires.add(demuxBOut)
        }

        _componentType = ComponentType.DEMULTIPLEXER
        _name = "Dual Demultiplexer"
    }
    
    @Override
    void process() {
        // Forward inputs from the chip to internal wires
        _demuxAChipSelect.setEnd(Wire.IN, isHigh(OUTPUT_ENABLE_A))
        _demuxAInputs.eachWithIndex{
            Wire wire, int offset ->
                wire.setEnd(Wire.IN, isHigh(INPUT_A + offset))
        }
        _demuxASelects.eachWithIndex{
            Wire wire, int offset ->
                wire.setEnd(Wire.IN, isHigh(SELECT_A + offset))
        }
        _demuxBChipSelect.setEnd(Wire.IN, isHigh(OUTPUT_ENABLE_B))
        _demuxBInputs.eachWithIndex{
            Wire wire, int offset ->
                wire.setEnd(Wire.IN, isHigh(INPUT_B + offset))
        }
        _demuxBSelects.eachWithIndex{
            Wire wire, int offset ->
                wire.setEnd(Wire.IN, isHigh(SELECT_B + offset))
        }

        // Update all wires to move their inputs to each chip
        _internalWires.each{ it.process() }
        
        // Update chips
        _demuxA.process()
        _demuxB.process()
        
        // Update internal wires to carry outputs from chip
        _internalWires.each{ it.process() }
    
        // Forward outputs of chip to external wires
        (0..31).each {
            int offset ->
                boolean fromA = _demuxAOutputs[offset].getEnd(Wire.OUT)
                boolean fromB = _demuxBOutputs[offset].getEnd(Wire.OUT)
                
                setPin(OUTPUT_R1 + offset, fromA || fromB)
        }
    }
}