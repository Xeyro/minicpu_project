package main.groovy.Component

import main.groovy.Wire.Wire

/**
 * Base Component:
 * Provides a base implementation that other Components may choose to
 * use, however it is not required. Base Component provides convenience
 * methods that can be used to make code more readable.
 */
class BaseComponent implements Component {
    
    protected String _id
    protected String _name
    protected ComponentType _componentType
    
    // Maps physical hardware pins to the wire they are connected to
    protected Wire[]    _pins
    // Maps the hardware pin to the end of the wire that it is connected to (each wire has 2 ends)
    protected int[]     _end
    
    /**
     * Process:
     * Should be overridden by subclass
     */
    void process() {
        println("Warning, doing nothing...")
    }
    
    /**
     * Connect:
     * Connects a wire to this Component
     */
    void connect(Wire wire, int pin, int end) {
        _pins[pin] = wire
        _end[pin] = end
    }
    
    int[] getEnds() { _end }
    
    Wire[] getWires() { _pins }
    
    String getId() {
        _id
    }
    
    void setId(String label) {
        _id = label
    }
    
    String getName() {
        _name
    }
    
    void setName(String name) {
        _name = name
    }
    
    ComponentType getComponentType() {
        _componentType
    }
    
    void setComponentType(ComponentType type) {
        _componentType = type
    }
    
    protected boolean isConnected(int pin) {
        return _pins[pin] != null
    }
    
    boolean isHigh(int pin) {
        if(isConnected(pin) == false) {
            System.err.println("Warning: Called isHigh() on null pin: ${getId()}@${pin}")
            return false
        }
        return _pins[pin].getEnd(_end[pin])
    }
    
    protected boolean isLow(int pin) {
        return !isHigh(pin)
    }
    
    protected void setPin(int pinId, boolean state) {
        _pins[pinId].setEnd(_end[pinId], state)
    }
    
    /**
     * Reads a binary number of [length] bits starting at the [start] pin
     * @param start     - pin to start reading
     * @param length    - length of the binary number
     * @return          - array representing the binary number
     */
    boolean[] read(int start, int length) {
        (start..(start+length-1)).collect{
            isHigh(it)
        }.toArray()
    }
    
    /**
     * Writes a binary number of [length] starting at the [start] pin
     * @param start     - pin to start writing at
     * @param length    - length of the binary number
     * @param states    - array representing the binary number to write
     */
    protected void write(int start, int length, boolean[] states) {
        (start..(start+length-1)).eachWithIndex{
            int pinId, int index ->
                setPin(pinId, states[index])
        }
    }
    
    static int binaryToDecimal(boolean[] binary) {
        int decimal = 0
        binary.eachWithIndex{
            boolean bit, int power ->
                if(bit)
                    decimal += (int)Math.pow(2, power)
        }
        return decimal
    }
    
    static int binaryToDecimal(boolean[] binary, int offset, int length) {
        int decimal = 0
        (offset..(offset+length-1)).eachWithIndex{
            int index, int power ->
                if(binary[index])
                    decimal += (int)Math.pow(2, power)
        }
        return decimal
    }
    
    static boolean[] decimalToBinary(int decimal) {
        int bits = 32
        boolean[] binaryValue = new boolean[bits]
        ((bits - 1)..0).each{
            int bitNumber ->
            final int bitValue = (int)Math.pow(2, bitNumber)
            if(decimal >= bitValue) {
                binaryValue[bitNumber] = true
                decimal -= bitValue
            }
        }
        return binaryValue
    }
}