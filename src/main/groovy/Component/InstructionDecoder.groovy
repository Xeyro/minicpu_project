package main.groovy.Component

import main.groovy.MetaData.MetaData
import main.groovy.Wire.Wire
import static main.groovy.MetaData.MetaData.*

/**
 * Instruction Decoder:
 * Represents the instruction decoder from the hardware.
 */
class InstructionDecoder extends BaseComponent implements Component {

    private static final int STATE_WAIT             = 0
    private static final int STATE_READ_INSTRUCTION = 1
    private static final int STATE_EXECUTE          = 2
    
    public final int CHIP_SELECT    = 0
    public final int MEM_1
    public final int MEM_2
    public final int MEM_3
    public final int MEM_4
    public final int INSTRUCTION_LENGTH
    public final int OFFSET
    public final int INSTRUCTION
    public final int CONTROL
    public final int PIN_COUNT

    
    private final int _instructionSize
    private int _opcode
    private int _operand1
    private int _operand2
    private boolean[] _memoryBuffer

    private int _state
    private int _cyclesLeftInState
    private InstructionMetaData _instruction
    
    // Don't ask why, I don't know
    int opCode() { _opcode }
    
    int operand1() { _operand1 }
    int operand2() { _operand2 }
    int opcode() { _opcode }
    int cyclesLeftInState() { _cyclesLeftInState }
    
    String state() {
        switch (_state) {
            case STATE_WAIT: return "WAIT"
            case STATE_READ_INSTRUCTION: return "READ"
            case STATE_EXECUTE: return "EXEC"
            default: return "ERROR"
        }
    }
    
    String memoryBuffer() {
        String value = "\n"
        (0..3).each{
            int byteNumber ->
                String byteString = "\n\t"
                (0..7).each{
                    int index ->
                        byteString += _memoryBuffer[index + (byteNumber * 8)] ? "1" : "0"
                }
                value += byteString.reverse()
        }
        return value
    }
    
    InstructionDecoder(int instructionSize) {
        _memoryBuffer = new boolean[instructionSize]
        _instructionSize = instructionSize
        
        MEM_1               = CHIP_SELECT + 1
        MEM_2               = MEM_1 + _instructionSize/4
        MEM_3               = MEM_2 + _instructionSize/4
        MEM_4               = MEM_3 + _instructionSize/4
        INSTRUCTION_LENGTH  = MEM_4 + _instructionSize/4
        OFFSET              = INSTRUCTION_LENGTH + _instructionSize/2
        INSTRUCTION         = OFFSET + _instructionSize/2
        CONTROL             = INSTRUCTION + _instructionSize/2
        PIN_COUNT           = CONTROL + CONTROL_PINS
        
        _pins = new Wire[PIN_COUNT]
        _end = new int[PIN_COUNT]
        _componentType = ComponentType.INSTRUCTION_DECODER
        _name = "Instruction Decoder"
        
        _state = STATE_WAIT
        _cyclesLeftInState = 4

    }
    
    @Override
    void process() {
        if(isLow(CHIP_SELECT))
            return
        
        switch (_state) {
            case STATE_EXECUTE:
                write(CONTROL, CONTROL_PINS, _instruction.controlAtCycle.reverse()[_cyclesLeftInState-1])
                break

            case STATE_READ_INSTRUCTION:
                int instructionByte = (4 - _cyclesLeftInState)
                read(
                    MEM_1 + (instructionByte * (int) (_instructionSize / 4)),
                    (int) (_instructionSize / 4)
                ).eachWithIndex {
                    boolean bit, int offset ->
                        _memoryBuffer[instructionByte * (int)(_instructionSize / 4) + offset] = bit
                }
                break
        }
        
        _cyclesLeftInState--

        if(_cyclesLeftInState <= 0) {
            switch (_state) {
                case STATE_WAIT:
                    _state = STATE_EXECUTE
                    _instruction = ClearStackPointer
                    _cyclesLeftInState = _instruction.controlAtCycle.size()
                    break
                case STATE_READ_INSTRUCTION:
                    _state = STATE_EXECUTE
    
                    _opcode = binaryToDecimal(_memoryBuffer, 0, 8)
                    _instruction = InstructionInfo[_opcode]
    
                    if(_instruction.operand1Size > 0) {
                        _operand1 = binaryToDecimal(_memoryBuffer, 8, 8 * _instruction.operand1Size)
                    }
                    if(_instruction.operand2Size > 0) {
                        final int offset = 8  + (8 * _instruction.operand1Size)
                        _operand2 = binaryToDecimal(_memoryBuffer, offset, 8 * _instruction.operand2Size)
                    }
                    
                    _cyclesLeftInState = _instruction.cycles
                    break
                default:
                    System.err.println("SYSTEM HALTED")
                    _state = STATE_WAIT
                    _cyclesLeftInState = 1
                    break
            }

        }
        
    }

}
