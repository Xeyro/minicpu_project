package main.groovy.Component

import main.groovy.Wire.Wire

/**
 * Clock
 * Simulates a hardware clock
 * Used to output a oscillating signal
 */
class Clock implements Component {
    
    private List<Wire> _outputs
    private boolean signal
    private String _id
    
    private ComponentType _componentType
    private String _name
    
    final int OUT = 0
    
    Clock() {
        _outputs = new ArrayList<>()
        _componentType = ComponentType.CLOCK
        _name = "System Clock"
    }
    
    /**
     * Process:
     * Causes the clock signal to switch from high to low if high, or low
     * to high if low
     */
    void process() {
        _outputs.each{
            it.setEnd(Wire.IN, signal)
        }
        signal = !signal
    }
    
    void connect(Wire wire, int pin, int end) {
        if(end == Wire.OUT)
            throw new Exception("Clocks cannot have ioCount")
        
        _outputs.add(wire)
    }
    
    int[] getEnds() {
        int[] ends = new int[_outputs.size()]
        _outputs.eachWithIndex {
            Wire wire, int index ->
                ends[index] = Wire.IN
        }
        return ends
    }
    
    Wire[] getWires() {
        Wire[] wires = new Wire[_outputs.size()]
        _outputs.eachWithIndex{
            Wire wire, int index ->
                wires[index] = wire
        }
        return wires
    }
    
    String getId() { _id }
    
    void setId(String label) { _id = label }
    
    String getName() { _name }
    
    void setName(String name) { _name = name }
    
    ComponentType getComponentType() { _componentType }
    
    void setComponentType(ComponentType type) { _componentType = type}
}
