package main.groovy.Component

import main.groovy.Wire.Wire

class RAM extends BaseComponent implements Component {

    public final int CHIP_SELECT    = 0
    public final int ADDRESS        = 1
    public final int DATA_IN
    public final int DATA_OUT
    public final int PIN_COUNT
    
    private boolean[] _memory
    private int _addressSize

    int addressSize() { return _addressSize }
    
    RAM(int addressSize) {
        
        DATA_IN     = ADDRESS + addressSize
        DATA_OUT    = DATA_IN + 8
        PIN_COUNT   = DATA_OUT + 8
        
        _end = new int[PIN_COUNT]
        _pins = new Wire[PIN_COUNT]
        
        _addressSize = addressSize
        _memory = new boolean[Math.pow(2, _addressSize) * 8]
        _componentType = ComponentType.RAM
        _name = "RAM"
        
        Random random = new Random()
        (0..(_memory.length-1)).each{
            int index ->
                _memory[index] = random.nextBoolean()
        }
    }
    
    @Override
    void process() {
        final int address = binaryToDecimal(read(ADDRESS, _addressSize))
     
        if(isHigh(CHIP_SELECT)) {
            writeMemory(
                address,
                read(DATA_IN, 8)
            )
        }
     
        write(
            DATA_OUT,
            8,
            readMemory(address, 8)
        )
    }

    boolean[] readMemory(int address, int length) {
        boolean[] value = new boolean[length]
        (0..(length-1)).each {
            int offset ->
                value[offset] = _memory[address + offset]
        }
        return value
    }
    
    void writeMemory(int address, boolean[] value) {
        value.eachWithIndex {
            boolean bit, int offset ->
                _memory[address + offset] = bit
        }
    }
}