package main.groovy.Wire

/**
 * Wire:
 * Represents a physical wire connecting two Components
 * Used to transmit electrical signals between them
 */
interface Wire {
    
    public static int IN = 0
    public static int OUT = 1
    
    /**
     * Process:
     * Moves electrical signals from the IN end of the wire to the OUT
     * end of the wire
     */
    void process()
    
    /**
     * Get End:
     * @param end - which end of the wire
     * @return - true if end is High, false if end is Low
     */
    boolean getEnd(int end)
    
    /**
     * Set End:
     * Sets the given end of the wire to state
     * @param end - which end of the wire
     * @param state - true for High, false for Low
     */
    void setEnd(int end, boolean state)
    
    /**
     * Split:
     * Adds a new output end to the wire.
     * @return - the wire representing the new output end of the previous wire
     */
    Wire split()
    
    
    Wire fray()
    
    boolean isHighImpedance()
}
