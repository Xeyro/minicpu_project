package main.groovy.Wire

import sun.reflect.generics.reflectiveObjects.NotImplementedException

class UniDirectionalWire implements Wire {
    
    /** Always of size 2, represents the signal on either end of the wire */
    private boolean[] _ends
    
    private List<Wire> _splitEnds
    private boolean _isHighImpedance
    
    UniDirectionalWire() {
        _ends = new boolean[2]
        _splitEnds = new ArrayList<>()
    }
    
    /**
     * Process:
     * Moves electrical signals from the IN end of the wire to the OUT
     * end of the wire
     */
    void process() {
        _splitEnds.each{
            it.setEnd(IN, _ends[IN])
            it.process()
        }
        _ends[OUT] = _ends[IN]
        _isHighImpedance = true
    }
    
    /**
     * Get End:
     * @param end   - which end of the wire
     * @return      - true if end is High, false if end is Low
     */
    boolean getEnd(int end) {
        return _ends[end]
    }
    
    /**
     * Set End:
     * Sets the given end of the wire to state
     * @param end   - which end of the wire
     * @param state - true for High, false for Low
     */
    void setEnd(int end, boolean state) {
        _ends[end] = state
        _isHighImpedance = false
    }
    
    /**
     * Split:
     * Adds a new output end to the wire.
     * @return      - the wire representing the new output end of the previous wire
     */
    Wire split() {
        Wire wire = new UniDirectionalWire()
        _splitEnds.add(wire)
        return wire
    }
    
    Wire fray() {
        throw new NotImplementedException()
    }
    
    boolean isHighImpedance() {
        return _isHighImpedance
    }
}
