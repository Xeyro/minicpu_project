package main.groovy.Wire

class BiDirectionalWire implements Wire {
    
    protected boolean _isHighImpedance
    
    protected List<Wire> _frayedEnds
    protected List<Wire> _splitEnds
    boolean[] _end
    int _lastSignalEnd
    
    BiDirectionalWire() {
        _end = new boolean[2]
        _frayedEnds = new ArrayList<>()
        _splitEnds = new ArrayList<>()
        _lastSignalEnd = -1
        _isHighImpedance = true
    }
    
    void process() {
        if(_lastSignalEnd == -1) {
            _isHighImpedance = true
            _frayedEnds.each { it.process() }
            _splitEnds.each { it.process() }
            return
        }
        
        if(_lastSignalEnd == OUT) {
            _frayedEnds.each { it.setEnd(_lastSignalEnd, _end[_lastSignalEnd]) }
            _frayedEnds.each { it.process() }
        } else {
            _splitEnds.each { it.setEnd(_lastSignalEnd, _end[_lastSignalEnd]) }
            _splitEnds.each { it.process() }
        }
        
        _lastSignalEnd = -1
    }
    
    boolean getEnd(int end) {
        return _end[end]
    }
    
    void setEnd(int end, boolean state) {
        _end[end] = state
        _isHighImpedance = false
        _lastSignalEnd = end
    }
    
    Wire split() {
        Wire wire = new BiDirectionalWire()
        _splitEnds.add(wire)
        return wire
    }
    
    Wire fray() {
        Wire wire = new BiDirectionalWire()
        _frayedEnds.add(wire)
        return wire
    }
    
    boolean isHighImpedance() {
        return _isHighImpedance
    }
}