package main.groovy

import main.groovy.Board.Board
import main.groovy.Board.CircuitBoard
import main.groovy.Component.Adder
import main.groovy.Component.DataSelector
import main.groovy.Component.InstructionDecoder
import main.groovy.Component.Register
import main.groovy.GUIWrapper.WrappedBoards.WrappedBoardFactory
import main.groovy.GUIWrapper.WrapperInterface.WrappedBoard
import main.groovy.Wire.UniDirectionalWire
import main.groovy.Wire.Wire

class InstructionDecoderUnit {
    
    private final Wire ALWAYS_HIGH  = [getEnd : { end -> true }, setEnd : { end, state -> }] as Wire
    private final Wire ALWAYS_LOW   = [getEnd : { end -> false }, setEnd : { end, state -> }] as Wire

    private final Wire NOTHING = new UniDirectionalWire()
    
    private final Register              U15
    private final Adder                 U105
    private final Adder                 U106
    private final DataSelector          U115
    private final DataSelector          U116
    private final InstructionDecoder    U500
    
    private final Board _board
    
    // Inputs
    public final Wire[] DATA    = new Wire[16]
    public final Wire[] SP      = new Wire[16]
    public final Wire[] MEM_1   = new Wire[8]
    public final Wire[] MEM_2   = new Wire[8]
    public final Wire[] MEM_3   = new Wire[8]
    public final Wire[] MEM_4   = new Wire[8]
    public final Wire[] U115_SEL= new Wire[2]
    public final Wire[] U116_SEL= new Wire[2]
    public final Wire U15_CS    = new UniDirectionalWire()
    public final Wire U15_CLOCK = new UniDirectionalWire()
    
    // Outputs
    public final Wire[] MEM     = new Wire[16]
    public final Wire[] INSTR   = new Wire[16]
    
    WrappedBoard Board() { WrappedBoardFactory.wrap(_board) }
    
    InstructionDecoderUnit() {
    
        _board = new CircuitBoard()
        
        /** Create Components ======== */
        /** ========================== */
        U15     = new Register(16)
        U15.setId("U15")
        U15.setName("Instruction Pointer")
        _board.addComponent(U15)
    
        U105     = new Adder(16)
        U105.setId("U105")
        U105.setName("Inst. Length Adder")
        _board.addComponent(U105)
    
        U106     = new Adder(16)
        U106.setId("U106")
        U106.setName("Inst. Offset Adder")
        _board.addComponent(U106)
    
        U115    = new DataSelector.Builder()
            .inputSize(16)
            .ioCount(4)
            .buildMultiplexer()
        U115.setId("U115")
        U115.setName("Inst. Point Selector")
        _board.addComponent(U115)
    
        U116    = new DataSelector.Builder()
            .inputSize(16)
            .ioCount(4)
            .buildMultiplexer()
        U116.setId("U116")
        U116.setName("Memory Addr. Selector")
        _board.addComponent(U116)
        
        U500    = new InstructionDecoder(32)
        U500.setId("U500")
        _board.addComponent(U500)
        
        /** Connect Components ======= */
        /** ========================== */
    
        int offset = 0
        
// ====== U15
        (0..(U15.inputSize()-1)).each{
            int index ->
                _board.connect(U115, U115.OUTPUT + index, U15, U15.INPUT + index)
        }
        U15.connect(U15_CS, U15.CHIP_SELECT, Wire.OUT)
        U15.connect(U15_CLOCK, U15.CLOCK, Wire.OUT)

// ====== U105
        (0..(U105.operandSize()-1)).each{
            int index ->
                _board.connect(U500, U500.INSTRUCTION_LENGTH + index, U105, U105.INPUT_B + index)
                Wire wire = _board.connect(U15, U15.OUTPUT + index, U105, U105.INPUT_A + index)
                U116.connect(wire.split(), U116.INPUT + index, Wire.OUT)
        }
        
        U105.connect(ALWAYS_HIGH, U105.CHIP_SELECT, Wire.OUT)
        U105.connect(ALWAYS_LOW, U105.SUBTRACT, Wire.OUT)
        U105.connect(ALWAYS_LOW, U105.CARRY_IN, Wire.OUT)
    
// ====== U106
        offset = U115.inputSize() * 2
        (0..(U106.operandSize()-1)).each{
            int index ->
                _board.connect(U500, U500.OFFSET + index, U106, U106.INPUT_B + index)
                Wire wire = _board.connect(U105, U105.OUTPUT + index, U106, U106.INPUT_A + index)
                U115.connect(wire.split(), U115.INPUT + offset + index, Wire.OUT)
        }
    
        U106.connect(NOTHING, U106.CARRY_OUT, Wire.IN)
        U106.connect(ALWAYS_HIGH, U106.CHIP_SELECT, Wire.OUT)
        U106.connect(ALWAYS_LOW, U106.SUBTRACT, Wire.OUT)
        _board.connect(U105, U105.CARRY_OUT, U106, U106.CARRY_IN)
        
// ====== U115
        offset = U115.inputSize() * 3
        (0..(U115.inputSize()-1)).each{
            int index ->
                _board.connect(U106, U106.OUTPUT + index, U115, U115.INPUT + index + offset)
        }
        
        offset = U115.inputSize()
        (0..(U115.inputSize()-1)).each{
            int index ->
                Wire wire = _board.connect(U500, U500.INSTRUCTION + index, U115, U115.INPUT + index + offset)
                U116.connect(wire.split(), U116.INPUT + (U116.inputSize() * 2) + index, Wire.OUT)
                INSTR[index] = wire.split()
        }
    
        (0..(U115_SEL.length-1)).each {
            int index ->
                Wire wire = new UniDirectionalWire()
                U115_SEL[index] = wire
                U115.connect(wire, U115.SELECTOR + index, Wire.OUT)
        }
    
        U115.connect(ALWAYS_HIGH, U115.CHIP_SELECT, Wire.OUT)

// ====== U116
        offset = U116.inputSize() * 3
        (0..(DATA.length-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                DATA[index] = wire
                U115.connect(wire, U115.INPUT + index, Wire.OUT)
                U116.connect(wire.split(), U116.INPUT + offset + index, Wire.OUT)
        }

        offset = U116.inputSize()
        (0..(SP.length-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                SP[index] = wire
                U116.connect(wire, U116.INPUT + index + offset, Wire.OUT)
        }

        (0..(U116.inputSize()-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                MEM[index] = wire
                U116.connect(wire, U116.OUTPUT + index, Wire.IN)
        }
        
        (0..(U116_SEL.length-1)).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                U116_SEL[index] = wire
                U116.connect(wire, U116.SELECTOR + index, Wire.OUT)
        }
    
        U116.connect(ALWAYS_HIGH, U116.CHIP_SELECT, Wire.OUT)


// ====== U500
        (0..(MEM_1.length-1)).each{
            int index ->
                Wire wire
                
                wire = new UniDirectionalWire()
                MEM_1[index] = wire
                U500.connect(wire, U500.MEM_1 + index, Wire.OUT)
    
                wire = new UniDirectionalWire()
                MEM_2[index] = wire
                U500.connect(wire, U500.MEM_2 + index, Wire.OUT)
    
                wire = new UniDirectionalWire()
                MEM_3[index] = wire
                U500.connect(wire, U500.MEM_3 + index, Wire.OUT)
    
                wire = new UniDirectionalWire()
                MEM_4[index] = wire
                U500.connect(wire, U500.MEM_4 + index, Wire.OUT)
        }

        
        
    }
    
    
}