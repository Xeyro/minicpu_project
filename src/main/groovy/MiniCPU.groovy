package main.groovy

import main.groovy.Board.Board
import main.groovy.Board.CircuitBoard
import main.groovy.Component.Adder
import main.groovy.Component.CombinationalLogicChip
import main.groovy.Component.DataSelector
import main.groovy.Component.FlagsChip
import main.groovy.Component.InstructionDecoder
import main.groovy.Component.LogicChip
import main.groovy.Component.RAM
import main.groovy.Component.Register
import main.groovy.Component.U114Demulitplexer
import main.groovy.GUIWrapper.WrappedBoards.WrappedBoardFactory
import main.groovy.GUIWrapper.WrapperInterface.WrappedBoard
import main.groovy.MetaData.MetaData
import main.groovy.Wire.UniDirectionalWire
import main.groovy.Wire.Wire

class MiniCPU {
    
    private final Wire ALWAYS_HIGH  = [getEnd: { end -> true }, setEnd: { end, state -> }] as Wire
    private final Wire ALWAYS_LOW   = [getEnd : { end -> false }, setEnd : { end, state -> }] as Wire
    private final Wire NOTHING = new UniDirectionalWire()
    
    private final Board _board
    
    private final Register              U10
    private final Register              U11
    private final Register              U12
    private final Register              U13
    private final Register              U14
    private final Register              U15
    private final Adder                 U100
    private final LogicChip             U101
    private final LogicChip             U102
    private final LogicChip             U103
    private final LogicChip             U104
    private final Adder                 U105
    private final Adder                 U106
    private final Adder                 U107
    private final Register              U110
    private final DataSelector          U111
    private final DataSelector          U112
    private final DataSelector          U113
    private final U114Demulitplexer     U114
    private final DataSelector          U115
    private final DataSelector          U116
    private final DataSelector          U117
    private final DataSelector          U118A
    private final DataSelector          U118B
    private final DataSelector          U120
    private final InstructionDecoder    U500
    private final FlagsChip             E1
    private final FlagsChip             E2

    private final CombinationalLogicChip U100_M
    private final DataSelector          E3
    private final DataSelector          U220
    private final DataSelector          U221
    private final RAM                   U200
    private final RAM                   U201
    private final RAM                   U202
    private final RAM                   U203
    private final RAM                   U204
    private final RAM                   U205
    private final RAM                   U206
    private final RAM                   U207
    private final RAM                   U208
    private final RAM                   U209
    private final RAM                   U210
    private final RAM                   U211
    private final RAM                   U212
    private final RAM                   U213
    private final RAM                   U215
    
    /** Inputs */
    public final Wire   CLOCK                   = new UniDirectionalWire()

    public final Wire   U15_CLOCK               = new UniDirectionalWire()
    
    /** Other */
    public final Wire[] FLAGS                   = new Wire[4]
    public final Wire[] SP                      = new Wire[16]
    public final Wire[] ALU                     = new Wire[8]
    public final Wire[] INSTR                   = new Wire[16]
    public final Wire[] DATA                    = new Wire[16]
    public final Wire[] MEM_ADDR                = new Wire[16]
    public final Wire[] MEM                     = new Wire[8]

    public final Wire[] REGISTERS_DATA_IN       = new Wire[8]
    
    WrappedBoard Board() { WrappedBoardFactory.wrap(_board) }
    
    MiniCPU() {
        _board = new CircuitBoard()
    
        /** Create Components ======== */
        /** ========================== */
        
        U10     = new Register(8)
        U10.setId("U10")
        U10.setName("Register (R1)")
        _board.addComponent(U10)
    
        U11     = new Register(8)
        U11.setId("U11")
        U11.setName("Register (R2)")
        _board.addComponent(U11)
    
        U12     = new Register(8)
        U12.setId("U12")
        U12.setName("Register (R3)")
        _board.addComponent(U12)
    
        U13     = new Register(8)
        U13.setId("U13")
        U13.setName("Register (R4)")
        _board.addComponent(U13)
    
        U14     = new Register(16)
        U14.setId("U14")
        U14.setName("Stack Pointer")
        _board.addComponent(U14)
    
        U15     = new Register(16)
        U15.setId("U15")
        U15.setName("Instruction Pointer")
        _board.addComponent(U15)
        
        U100    = new Adder(8)
        U100.setId("U100")
        U100.setName("ALU Adder")
        _board.addComponent(U100)
    
        U101    = new LogicChip.Builder()
            .operandSize(8)
            .functionAnd()
            .build()
        U101.setId("U101")
        U101.setName("ALU AND Gate")
        _board.addComponent(U101)
    
        U102    = new LogicChip.Builder()
            .operandSize(8)
            .functionOr()
            .build()
        U102.setId("U102")
        U102.setName("ALU OR Gate")
        _board.addComponent(U102)
    
        U103    = new LogicChip.Builder()
            .operandSize(8)
            .functionXor()
            .build()
        U103.setId("U103")
        U103.setName("ALU XOR Gate")
        _board.addComponent(U103)
    
        U104    = new LogicChip.Builder()
            .operandSize(8)
            .functionNot()
            .build()
        U104.setId("U104")
        U104.setName("ALU NOT Gate")
        _board.addComponent(U104)
    
        U105     = new Adder(16)
        U105.setId("U105")
        U105.setName("Inst. Length Adder")
        _board.addComponent(U105)
    
        U106     = new Adder(16)
        U106.setId("U106")
        U106.setName("Inst. Offset Adder")
        _board.addComponent(U106)
        
        U107    = new Adder(16)
        U107.setId("U107")
        U107.setName("Stack Pointer Adder")
        _board.addComponent(U107)
    
        U110    = new Register(4)
        U110.setId("U110")
        U110.setName("FLAGS Register")
        _board.addComponent(U110)
    
        U111    = new DataSelector.Builder()
            .inputSize(8)
            .ioCount(8)
            .selects(3)
            .buildMultiplexer()
        U111.setId("U111")
        U111.setName("ALU Output Selector")
        _board.addComponent(U111)
    
        U112    = new DataSelector.Builder()
            .inputSize(8)
            .ioCount(8)
            .selects(3)
            .buildMultiplexer()
        U112.setId("U112")
        U112.setName("ALU Reg. A Selector")
        _board.addComponent(U112)
    
        U113    = new DataSelector.Builder()
            .inputSize(8)
            .ioCount(8)
            .selects(3)
            .buildMultiplexer()
        U113.setId("U113")
        U113.setName("ALU Reg. B Selector")
        _board.addComponent(U113)
    
        U114    = new U114Demulitplexer()
        U114.setId("U114")
        U114.setName("2-4 Demultiplexer")
        _board.addComponent(U114)
    
        U115    = new DataSelector.Builder()
            .inputSize(16)
            .ioCount(4)
            .buildMultiplexer()
        U115.setId("U115")
        U115.setName("Inst. Point Selector")
        _board.addComponent(U115)
    
        U116    = new DataSelector.Builder()
            .inputSize(16)
            .ioCount(4)
            .buildMultiplexer()
        U116.setId("U116")
        U116.setName("Memory Addr. Selector")
        _board.addComponent(U116)
        
        U117    = new DataSelector.Builder()
            .inputSize(16)
            .ioCount(2)
            .buildMultiplexer()
        U117.setId("U117")
        U117.setName("2-1 Multiplexer")
        _board.addComponent(U117)
    
        U118A   = new DataSelector.Builder()
            .inputSize(8)
            .ioCount(8)
            .selects(3)
            .buildMultiplexer()
        U118A.setId("U118A")
        U118A.setName("ALU Input A Selector")
        _board.addComponent(U118A)
    
        U118B   = new DataSelector.Builder()
            .inputSize(8)
            .ioCount(8)
            .selects(3)
            .buildMultiplexer()
        U118B.setId("U118B")
        U118B.setName("ALU Input B Selector")
        _board.addComponent(U118B)
    
        U120    = new DataSelector.Builder()
            .inputSize(4)
            .ioCount(2)
            .buildMultiplexer()
        U120.setId("U120")
        U120.setName("2-1 Multiplexer")
        _board.addComponent(U120)
    
        U500    = new InstructionDecoder(32)
        U500.setId("U500")
        _board.addComponent(U500)
    
        E1      = new FlagsChip(8)
        E1.setId("E1")
        E1.setName("Flags")
        _board.addComponent(E1)
    
        E2      = new FlagsChip(8)
        E2.setId("E2")
        E2.setName("Flags")
        _board.addComponent(E2)
    
        U200 = new RAM(12)
        U200.setId("U200")
        U200.setName("RAM")
        _board.addComponent(U200)
        
        U201 = new RAM(12)
        U201.setId("U201")
        U201.setName("RAM")
        _board.addComponent(U201)
    
        U202 = new RAM(12)
        U202.setId("U202")
        U202.setName("RAM")
        _board.addComponent(U202)
    
        U203 = new RAM(12)
        U203.setId("U203")
        U203.setName("RAM")
        _board.addComponent(U203)
    
        U204 = new RAM(12)
        U204.setId("U204")
        U204.setName("RAM")
        _board.addComponent(U204)
    
        U205 = new RAM(12)
        U205.setId("U205")
        U205.setName("RAM")
        _board.addComponent(U205)
    
        U206 = new RAM(12)
        U206.setId("U206")
        U206.setName("RAM")
        _board.addComponent(U206)
    
        U207 = new RAM(12)
        U207.setId("U207")
        U207.setName("RAM")
        _board.addComponent(U207)
    
        U208 = new RAM(12)
        U208.setId("U208")
        U208.setName("RAM")
        _board.addComponent(U208)
    
        U209 = new RAM(12)
        U209.setId("U209")
        U209.setName("RAM")
        _board.addComponent(U209)
    
        U210 = new RAM(12)
        U210.setId("U210")
        U210.setName("RAM")
        _board.addComponent(U210)
    
        U211 = new RAM(12)
        U211.setId("U211")
        U211.setName("RAM")
        _board.addComponent(U211)
    
        U212 = new RAM(12)
        U212.setId("U212")
        U212.setName("RAM")
        _board.addComponent(U212)
    
        U213 = new RAM(12)
        U213.setId("U213")
        U213.setName("RAM")
        _board.addComponent(U213)
    
        U215 = new RAM(12)
        U215.setId("U215")
        U215.setName("RAM")
        _board.addComponent(U215)
        
        U220 = new DataSelector.Builder()
            .inputSize(8)
            .ioCount(4)
            .buildMultiplexer()
        U220.setId("U220")
        U220.setName("Memory Input Selector")
        _board.addComponent(U220)
        
        U221 = new DataSelector.Builder()
            .inputSize(8)
            .ioCount(16)
            .buildMultiplexer()
        U221.setId("U221")
        U221.setName("T-Gate")
        _board.addComponent(U221)
    
        E3 = new DataSelector.Builder()
            .inputSize(4)
            .ioCount(2)
            .buildMultiplexer()
        E3.setId("E3")
        E3.setName("Write Selector")
        _board.addComponent(E3)
        
        U100_M = new CombinationalLogicChip.Builder()
            .encodedDataSize(4)
            .buildDecoder()
        U100_M.setId("U100_M")
        U100_M.setName("Write Decoder")
        _board.addComponent(U100_M)

        /** Connect Control lines */
        
        _board.connect(U500, U500.CONTROL + MetaData.R1_CS, U10, U10.CHIP_SELECT)
        _board.connect(U500, U500.CONTROL + MetaData.R2_CS, U11, U11.CHIP_SELECT)
        _board.connect(U500, U500.CONTROL + MetaData.R3_CS, U12, U12.CHIP_SELECT)
        _board.connect(U500, U500.CONTROL + MetaData.R4_CS, U13, U13.CHIP_SELECT)
        _board.connect(U500, U500.CONTROL + MetaData.U14_CS, U14, U14.CHIP_SELECT)
        _board.connect(U500, U500.CONTROL + MetaData.U100_CIN, U100, U100.CARRY_IN)
        _board.connect(U500, U500.CONTROL + MetaData.U107_PLUS_MINUS, U107, U107.SUBTRACT)
        _board.connect(U500, U500.CONTROL + MetaData.U110_CS, U110, U110.CHIP_SELECT)
        (0..2).each {
            int index ->
                _board.connect(U500, U500.CONTROL + MetaData.U111_SEL + index, U111, U111.SELECTOR + index)
        }
        (0..2).each {
            int index ->
                _board.connect(U500, U500.CONTROL + MetaData.U112_SEL + index, U112, U112.SELECTOR + index)
        }
        (0..2).each {
            int index ->
                _board.connect(U500, U500.CONTROL + MetaData.U113_SEL + index, U113, U113.SELECTOR + index)
        }
        _board.connect(U500, U500.CONTROL + MetaData.U114_OEA, U114, U114.OUTPUT_ENABLE_A)
        (0..1).each {
            int index ->
                _board.connect(U500, U500.CONTROL + MetaData.U114_SELA + index, U114, U114.SELECT_A + index)
        }
        _board.connect(U500, U500.CONTROL + MetaData.U114_OEB, U114, U114.OUTPUT_ENABLE_B)
        (0..1).each {
            int index ->
                _board.connect(U500, U500.CONTROL + MetaData.U114_SELB + index, U114, U114.SELECT_B + index)
        }
        _board.connect(U500, U500.CONTROL + MetaData.U117_SEL, U117, U117.SELECTOR)
        (0..2).each {
            int index ->
                _board.connect(U500, U500.CONTROL + MetaData.U118A_SEL + index, U118A, U118A.SELECTOR + index)
        }
        (0..2).each {
            int index ->
                _board.connect(U500, U500.CONTROL + MetaData.U118B_SEL + index, U118B, U118B.SELECTOR + index)
        }
        _board.connect(U500, U500.CONTROL + MetaData.U120_SEL, U120, U120.SELECTOR)
        (0..1).each {
            int index ->
                _board.connect(U500, U500.CONTROL + MetaData.U115_SEL + index, U115, U115.SELECTOR + index)
        }
        (0..1).each {
            int index ->
                _board.connect(U500, U500.CONTROL + MetaData.U116_SEL + index, U116, U116.SELECTOR + index)
        }
        _board.connect(U500, U500.CONTROL + MetaData.U15_CS, U15, U15.CHIP_SELECT)
        (0..1).each {
            int index ->
                _board.connect(U500, U500.CONTROL + MetaData.U220_SEL + index, U220, U220.SELECTOR + index)
        }
        _board.connect(U500, U500.CONTROL + MetaData.READ_WRITE, E3, E3.SELECTOR)
        
        /** Connect Components ======= */
        /** ========================== */
        
        int offset = 0

        offset = U116.inputSize() * 2
        (0..(INSTR.length-1)).each{
            int index ->
                INSTR[index] = _board.connect(U500, U500.INSTRUCTION + index, U116, U116.INPUT + index + offset)
        }
    
        (0..(FLAGS.size()-1)).each{
            int index ->
                FLAGS[index] = _board.connect(U110, U110.OUTPUT + index, U113, U113.INPUT + index + (U113.inputSize() * 5))
                U112.connect(FLAGS[index].split(), U112.INPUT + index + (U112.inputSize() * 5), Wire.OUT)
        }
    
        (0..3).each {
            int index ->
                U112.connect(ALWAYS_LOW, U112.INPUT + index + (U112.inputSize() * 5) + 4, Wire.OUT)
                U113.connect(ALWAYS_LOW, U112.INPUT + index + (U113.inputSize() * 5) + 4, Wire.OUT)
        }
    
        (0..(U116.inputSize()-5)).each{
            int index ->
                MEM_ADDR[index] = _board.connect(U116, U116.OUTPUT + index, U200, U200.ADDRESS + index)
        }
        
        (0..(U118A.inputSize()-1)).each{
            int index ->
                MEM[index] = _board.connect(U221, U221.OUTPUT + index, U118A, U118A.INPUT + index + (U118A.inputSize() * 5))
        }
        
        offset = 0
// ====== U10 (Register 1)
        U10.connect(CLOCK.split(), U10.CLOCK, Wire.OUT)

// ====== U11 (Register 2)
        U11.connect(CLOCK.split(), U11.CLOCK, Wire.OUT)

// ====== U12 (Register 3)
        U12.connect(CLOCK.split(), U12.CLOCK, Wire.OUT)

// ====== U13 (Register 4)
        U13.connect(CLOCK.split(), U13.CLOCK, Wire.OUT)
    
        // Connect Registers in bulk
        (0..7).each{
            int index ->
                _board.connect(U114, U114.OUTPUT_R1 + index, U10, U10.INPUT + index)
                _board.connect(U114, U114.OUTPUT_R2 + index, U11, U11.INPUT + index)
                _board.connect(U114, U114.OUTPUT_R3 + index, U12, U12.INPUT + index)
                _board.connect(U114, U114.OUTPUT_R4 + index, U13, U13.INPUT + index)
        }

// ====== U14 Stack Pointer
        offset = 0
        (0..(U14.inputSize()-1)).each{
            int index ->
                _board.connect(U117, U117.OUTPUT + index, U14, U14.INPUT + index)
        }
    
        U14.connect(CLOCK, U14.CLOCK, Wire.OUT)
    

// ====== U15
        offset = 0
        (0..(U15.inputSize()-1)).each{
            int index ->
                _board.connect(U115, U115.OUTPUT + index, U15, U15.INPUT + index)
        }
        U15.connect(U15_CLOCK, U15.CLOCK, Wire.OUT)
        
        

// ====== U100 - U104
        // U100
        U100.connect(ALWAYS_HIGH, U100.CHIP_SELECT, Wire.OUT)
        U100.connect(ALWAYS_LOW, U100.SUBTRACT, Wire.OUT)
    
        U101.connect(ALWAYS_HIGH, U101.CHIP_SELECT, Wire.OUT)
        U102.connect(ALWAYS_HIGH, U102.CHIP_SELECT, Wire.OUT)
        U103.connect(ALWAYS_HIGH, U103.CHIP_SELECT, Wire.OUT)
        U104.connect(ALWAYS_HIGH, U104.CHIP_SELECT, Wire.OUT)
    
        Wire[] U112ToLogicChips = new Wire[U112.inputSize()]
        Wire[] U113ToLogicChips = new Wire[U113.inputSize()]
    
        (0..(U112.inputSize()-1)).each{
            int index ->
                Wire u112wire = _board.connect(U112, U112.OUTPUT + index, U100, U100.INPUT_A + index)
                Wire u113wire = _board.connect(U113, U113.OUTPUT + index, U100, U100.INPUT_B + index)
            
                U112ToLogicChips[index] = u112wire
                U113ToLogicChips[index] = u113wire
            
                U101.connect(u112wire.split(), U101.INPUT_A + index, Wire.OUT)
                U101.connect(u113wire.split(), U101.INPUT_B + index, Wire.OUT)
            
                U102.connect(u112wire.split(), U102.INPUT_A + index, Wire.OUT)
                U102.connect(u113wire.split(), U102.INPUT_B + index, Wire.OUT)
            
                U103.connect(u112wire.split(), U103.INPUT_A + index, Wire.OUT)
                U103.connect(u113wire.split(), U103.INPUT_B + index, Wire.OUT)
            
                // 104 is a NOT gate, only takes one input; other is always low
                U104.connect(u112wire.split(), U104.INPUT_A + index, Wire.OUT)
                U104.connect(ALWAYS_LOW, U104.INPUT_B + index, Wire.OUT)
// DATA
                DATA[index] = u112wire.split()
                DATA[U112.inputSize() + index] = u113wire.split()
        }

// ====== U105
        (0..(U105.operandSize()-1)).each{
            int index ->
                _board.connect(U500, U500.INSTRUCTION_LENGTH + index, U105, U105.INPUT_B + index)
                Wire wire = _board.connect(U15, U15.OUTPUT + index, U105, U105.INPUT_A + index)
                U116.connect(wire.split(), U116.INPUT + index, Wire.OUT)
        }
    
        U105.connect(ALWAYS_HIGH, U105.CHIP_SELECT, Wire.OUT)
        U105.connect(ALWAYS_LOW, U105.SUBTRACT, Wire.OUT)
        U105.connect(ALWAYS_LOW, U105.CARRY_IN, Wire.OUT)

// ====== U106
        offset = U115.inputSize() * 2
        (0..(U106.operandSize()-1)).each{
            int index ->
                _board.connect(U500, U500.OFFSET + index, U106, U106.INPUT_B + index)
                Wire wire = _board.connect(U105, U105.OUTPUT + index, U106, U106.INPUT_A + index)
                U115.connect(wire.split(), U115.INPUT + offset + index, Wire.OUT)
        }
    
        U106.connect(NOTHING, U106.CARRY_OUT, Wire.IN)
        U106.connect(ALWAYS_HIGH, U106.CHIP_SELECT, Wire.OUT)
        U106.connect(ALWAYS_LOW, U106.SUBTRACT, Wire.OUT)
        _board.connect(U105, U105.CARRY_OUT, U106, U106.CARRY_IN)

// ====== U107
        offset = 0
        (0..(U14.inputSize()-1)).each{
            int index ->
                Wire stackPointerWire = _board.connect(U14, U14.OUTPUT + index, U107, U107.INPUT_A + index)
                SP[index] = stackPointerWire.split()
        }
    
        U107.connect(ALWAYS_HIGH, U107.CHIP_SELECT, Wire.OUT)
        U107.connect(ALWAYS_LOW, U107.CARRY_IN, Wire.OUT)
    
        // U107, input_b = 2
        (0..(U107.operandSize()-1)).each{
            int index ->
                if(index == 1)
                    U107.connect(ALWAYS_HIGH, U107.INPUT_B + index , Wire.OUT)
                else
                    U107.connect(ALWAYS_LOW, U107.INPUT_B + index, Wire.OUT)
        }
    
        U107.connect(NOTHING, U107.CARRY_OUT, Wire.IN)

// ====== U110
        offset = 0
        (0..(U110.inputSize()-1)).each{
            int index ->
                _board.connect(U120, U120.OUTPUT + index, U110, U110.INPUT + index)
        }
    
        U110.connect(CLOCK, U110.CLOCK, Wire.OUT)

// ====== U111
        offset = 0
        (0..(U111.inputSize()-1)).each{
            int index ->
                _board.connect(U100, U100.OUTPUT + index, U111, U111.INPUT + offset + index)
        }
    
        offset += U111.inputSize()
        (0..(U111.inputSize()-1)).each{
            int index ->
                _board.connect(U101, U101.OUTPUT + index, U111, U111.INPUT + offset + index)
        }
    
        offset += U111.inputSize()
        (0..(U111.inputSize()-1)).each{
            int index ->
                _board.connect(U102, U102.OUTPUT + index, U111, U111.INPUT + offset + index)
        }
    
        offset += U111.inputSize()
        (0..(U111.inputSize()-1)).each{
            int index ->
                _board.connect(U103, U103.OUTPUT + index, U111, U111.INPUT + offset + index)
        }
    
        offset += U111.inputSize()
        (0..(U111.inputSize()-1)).each{
            int index ->
                _board.connect(U104, U104.OUTPUT + index, U111, U111.INPUT + offset + index)
        }
    
        offset += U111.inputSize()
        (0..23).each{
            int index ->
                U111.connect(ALWAYS_LOW, U111.INPUT + offset + index, Wire.OUT)
        }
    
        U111.connect(ALWAYS_HIGH, U111.CHIP_SELECT, Wire.OUT)
    

// ALU
        (0..(U111.inputSize()-1)).each{
            int index ->
                Wire aluWire = new UniDirectionalWire()
                ALU[index] = aluWire
                U111.connect(aluWire, U111.OUTPUT + index, Wire.IN)
        }



// ====== U112 (Register A Selector)
    
    
        // R1
        offset = 0
        (0..(U112.inputSize()-1)).each {
            int index ->
                Wire wire = _board.connect(U10, U10.OUTPUT + index, U112, U112.INPUT + offset + index)
                U113.connect(wire.split(), U113.INPUT + offset + index, Wire.OUT)
        }
    
        // R2
        offset += U112.inputSize()
        (0..(U112.inputSize()-1)).each{
            int index ->
                Wire wire = _board.connect(U11, U11.OUTPUT + index, U112, U112.INPUT + offset + index)
                U113.connect(wire.split(), U113.INPUT + offset + index, Wire.OUT)
        }
    
        // R3
        offset += U112.inputSize()
        (0..(U112.inputSize()-1)).each{
            int index ->
                Wire wire = _board.connect(U12, U12.OUTPUT + index, U112, U112.INPUT + offset + index)
                U113.connect(wire.split(), U113.INPUT + offset + index, Wire.OUT)
        }
    
        // R4
        offset += U112.inputSize()
        (0..(U112.inputSize()-1)).each{
            int index ->
                Wire wire = _board.connect(U13, U13.OUTPUT + index, U112, U112.INPUT + offset + index)
                U113.connect(wire.split(), U113.INPUT + offset + index, Wire.OUT)
        }
    
        // MEM_ADDR
        offset += U112.inputSize()
        (0..(U112.inputSize()-1)).each{
            int index ->
                U112.connect(MEM[index].split(), U112.INPUT + offset + index, Wire.OUT)
        }
    
        // INST
        offset += (U112.inputSize() * 2)
        (0..(U112.inputSize()-1)).each{
            int index ->
                U112.connect(INSTR[index].split(), U112.INPUT + offset + index, Wire.OUT)
        }
    
        offset += U112.inputSize()
        (0..7).each{
            int index ->
                U112.connect(ALWAYS_LOW, U112.INPUT + index + offset, Wire.OUT)
        }
    
        U112.connect(ALWAYS_HIGH, U112.CHIP_SELECT, Wire.OUT)
    
// ====== U113 (Register B Selector)
    
    
        offset = U113.inputSize() * 3
        // MEM_ADDR
        offset += U113.inputSize()
        (0..(U113.inputSize()-1)).each{
            int index ->
                U113.connect(MEM[index].split(), U113.INPUT + offset + index, Wire.OUT)
        }
    
        // INST
        offset += (U113.inputSize() * 2)
        (0..(U113.inputSize()-1)).each{
            int index ->
                U113.connect(INSTR[index].split(), U113.INPUT + offset + index, Wire.OUT)
        }
    
        offset += U113.inputSize()
        (0..7).each{
            int index ->
                U113.connect(ALWAYS_LOW, U113.INPUT + index + offset, Wire.OUT)
        }
    
        U113.connect(ALWAYS_HIGH, U113.CHIP_SELECT, Wire.OUT)
    
// ====== U114
        // U118A
        (0..7).each{
            int index ->
                _board.connect(U118A, U118A.OUTPUT + index, U114, U114.INPUT_A + index)
        }
        // U118B
        (0..7).each{
            int index ->
                _board.connect(U118B, U118B.OUTPUT + index, U114, U114.INPUT_B + index)
        }
    
// ====== U115
        offset = U115.inputSize() * 3
        (0..(U115.inputSize()-1)).each{
            int index ->
                _board.connect(U106, U106.OUTPUT + index, U115, U115.INPUT + index + offset)
        }
    
        offset = U115.inputSize()
        (0..(U115.inputSize()-1)).each{
            int index ->
                U115.connect(INSTR[index].split(), U115.INPUT + index + offset, Wire.OUT)
        }
    
        U115.connect(ALWAYS_HIGH, U115.CHIP_SELECT, Wire.OUT)

// ====== U116
        offset = U116.inputSize() * 3
        (0..(DATA.length-1)).each{
            int index ->
                U115.connect(DATA[index].split(), U115.INPUT + index, Wire.OUT)
                U116.connect(DATA[index].split(), U116.INPUT + offset + index, Wire.OUT)
        }
    
        offset = U116.inputSize()
        (0..(SP.length-1)).each{
            int index ->
                U116.connect(SP[index].split(), U116.INPUT + index + offset, Wire.OUT)
        }
    
        U116.connect(ALWAYS_HIGH, U116.CHIP_SELECT, Wire.OUT)

// ====== U117
        // DATA must be instantiated and populated first
        offset = 0
        (0..(U117.inputSize()-1)).each{
            int index ->
                U117.connect(DATA[index].split(), U117.INPUT + offset + index, Wire.OUT)
        }
    
        offset += U117.inputSize()
        (0..(U117.inputSize()-1)).each{
            int index ->
                _board.connect(U107, U107.OUTPUT + index, U117, U117.INPUT + offset + index)
        }
    
        U117.connect(ALWAYS_HIGH, U117.CHIP_SELECT, Wire.OUT)

// ====== U118A
        // DATA
        offset = 0
        (0..(U118A.inputSize()-1)).each{
            int index ->
                U118A.connect(DATA[index].split(), U118A.INPUT + offset + index, Wire.OUT)
        }
    
        // SP
        offset += U118A.inputSize()
        (0..(U118A.inputSize()-1)).each{
            int index ->
                U118A.connect(SP[index].split(), U118A.INPUT + offset + index, Wire.OUT)
        }
    
        // INST
        offset += U118A.inputSize()
        (0..(U118A.inputSize()-1)).each{
            int index ->
                U118A.connect(INSTR[index].split(), U118A.INPUT + offset + index, Wire.OUT)
        }
    
        // ALU
        offset += U118A.inputSize()
        (0..(U118A.inputSize()-1)).each{
            int index ->
                U118A.connect(ALU[index].split(), U118A.INPUT + offset + index, Wire.OUT)
        }
    
        // RES
        offset += U118A.inputSize()
        (0..7).each{
            int index ->
                U118A.connect(ALWAYS_LOW, U118A.INPUT + offset + index, Wire.OUT)
        }
    
        offset += U118A.inputSize()
    
        // RES x 2
        offset += U118A.inputSize()
        (0..15).each{
            int index ->
                U118A.connect(ALWAYS_LOW, U118A.INPUT + offset + index, Wire.OUT)
        }
    
        // SEL
        offset = 0

        U118A.connect(ALWAYS_HIGH, U118A.CHIP_SELECT, Wire.OUT)

// ====== U118B
        offset = 0
        // DATA
        (0..(U118B.inputSize()-1)).each{
            int index ->
                U118B.connect(DATA[index + U118B.inputSize()].split(), U118B.INPUT + offset + index, Wire.OUT)
        }
    
        // SP
        offset += U118B.inputSize()
        (0..(U118B.inputSize()-1)).each{
            int index ->
                U118B.connect(SP[index + U118B.inputSize()].split(), U118B.INPUT + offset + index, Wire.OUT)
        }
    
        // INST
        offset += U118B.inputSize()
        (0..(U118B.inputSize()-1)).each{
            int index ->
                U118B.connect(INSTR[index + U118B.inputSize()].split(), U118B.INPUT + offset + index, Wire.OUT)
        }
    
        // ALU
        offset += U118B.inputSize()
        (0..(U118B.inputSize()-1)).each{
            int index ->
                U118B.connect(ALU[index], U118B.INPUT + offset + index, Wire.OUT)
        }
    
        // RES
        offset += U118B.inputSize()
        (0..7).each{
            int index ->
                U118B.connect(ALWAYS_LOW, U118B.INPUT + offset + index, Wire.OUT)
        }
    
        // MEM_ADDR
        offset += U118B.inputSize()
        (0..(U118B.inputSize()-1)).each{
            int index ->
                U118B.connect(MEM[index].split(), U118B.INPUT + offset + index, Wire.OUT)
        }
    
        // RES x 2
        offset += U118B.inputSize()
        (0..15).each{
            int index ->
                U118B.connect(ALWAYS_LOW, U118B.INPUT + offset + index, Wire.OUT)
        }
    
        // SEL
        offset = 0
    
        U118B.connect(ALWAYS_HIGH, U118B.CHIP_SELECT, Wire.OUT)

// ====== U120
    
        offset = 0
        (0..(U120.inputSize()-1)).each{
            int index ->
                _board.connect(E1, E1.OUTPUT + index, U120, U120.INPUT + index)
        }
    
        offset += U120.inputSize()
        (0..(U120.inputSize()-1)).each{
            int index ->
                _board.connect(E2, E2.OUTPUT + index, U120, U120.INPUT + index + offset)
        }
    
        U120.connect(ALWAYS_HIGH, U120.CHIP_SELECT, Wire.OUT)
    
// ====== U500
    
        U500.connect(_board.clockWire().split(), U500.CHIP_SELECT, Wire.OUT)
        
        offset = 0
        (0..7).each{
            int index ->
                U500.connect(MEM[index].split(), U500.MEM_1 + index, Wire.OUT)
                U500.connect(MEM[index].split(), U500.MEM_2 + index, Wire.OUT)
                U500.connect(MEM[index].split(), U500.MEM_3 + index, Wire.OUT)
                U500.connect(MEM[index].split(), U500.MEM_4 + index, Wire.OUT)
        }
        
// ====== E1
    
        (0..(E1.inputSize()-1)).each{
            int index ->
                _board.connect(U100, U100.OUTPUT + index, E1, E1.INPUT + index)
        }
    
        _board.connect(U100, U100.CARRY_OUT, E1, E1.CARRY_IN)

// ====== E2
    
        (0..(E2.inputSize()-1)).each{
            int index ->
                Wire wire = ALU[index].split()
                E2.connect(wire, E2.INPUT + index, Wire.OUT)
        }
    
        E2.connect(ALWAYS_LOW, E2.CARRY_IN, Wire.OUT)

// ====== E3
    
        offset = E3.inputSize()
        (0..(E3.inputSize()-1)).each{
            int index ->
                MEM_ADDR[12 + index]  = _board.connect(U116, U116.OUTPUT + 12 + index, E3, E3.INPUT + offset + index)
        }
    
        E3.connect(ALWAYS_LOW, E3.INPUT + 0, Wire.OUT)
        E3.connect(ALWAYS_HIGH, E3.INPUT + 1, Wire.OUT)
        E3.connect(ALWAYS_HIGH, E3.INPUT + 2, Wire.OUT)
        E3.connect(ALWAYS_HIGH, E3.INPUT + 3, Wire.OUT)
    
        E3.connect(ALWAYS_HIGH, E3.CHIP_SELECT, Wire.OUT)
        
        
// ====== U100_M

        (0..(E3.inputSize()-1)).each{
            int index ->
                _board.connect(E3, E3.OUTPUT + index, U100_M, U100_M.INPUT + index)
        }

        U100_M.connect(ALWAYS_HIGH, U100_M.CHIP_SELECT, Wire.OUT)
        
        U100_M.connect(NOTHING, U100_M.OUTPUT + 14, Wire.IN)

// ====== U220
        
        offset = 0
        (0..(U220.inputSize()-1)).each{
            int index ->
                U220.connect(DATA[index].split(), U220.INPUT + offset + index, Wire.OUT)
        }
    
        offset += U220.inputSize()
        (0..(U220.inputSize()-1)).each{
            int index ->
                U220.connect(DATA[index + U220.inputSize()].split(), U220.INPUT + offset + index, Wire.OUT)
        }
    
        offset += U220.inputSize()
        (0..(U220.inputSize()-1)).each{
            int index ->
                U220.connect(ALU[index].split(), U220.INPUT + offset + index, Wire.OUT)
        }
    
        offset += U220.inputSize()
        (0..(U220.inputSize()-1)).each{
            int index ->
                U220.connect(INSTR[index].split(), U220.INPUT + offset + index, Wire.OUT)
        }
        
        U220.connect(ALWAYS_HIGH, U220.CHIP_SELECT, Wire.OUT)
    
// ====== RAM - 200
    
        (0..(U220.inputSize()-1)).each{
            int index ->
                REGISTERS_DATA_IN[index] = _board.connect(U220, U220.OUTPUT + index, U200, U200.DATA_IN + index)
        }
        
        offset = 0
        (0..(U200.addressSize()-1)).each{
            int index ->
                U200.connect(MEM_ADDR[index].split(), U200.ADDRESS + index, Wire.OUT)
        }
        
        _board.connect(U100_M, U100_M.OUTPUT + 0, U200, U200.CHIP_SELECT)
        
        (0..(U221.inputSize()-1)).each{
            int index ->
                _board.connect(U200, U200.DATA_OUT + index, U221, U221.INPUT + index)
            
        }

// ====== RAM - Rest
        
        List<RAM> remainingRamChips = [
            U201, U202, U203, U204,
            U205, U206, U207, U208,
            U209, U210, U211, U212,
            U213
        ]
        
        remainingRamChips.eachWithIndex{
            RAM chip, int chipNumber ->
                chipNumber += 1
    
                (0..(U220.inputSize()-1)).each{
                    int index ->
                        chip.connect(REGISTERS_DATA_IN[index].split(), chip.DATA_IN + index, Wire.OUT)
                }
    
                offset = 0
                (0..(chip.addressSize()-1)).each{
                    int index ->
                        chip.connect(MEM_ADDR[index].split(), chip.ADDRESS + index, Wire.OUT)
                }
    
                _board.connect(U100_M, U100_M.OUTPUT + chipNumber, chip, chip.CHIP_SELECT)
    
                (0..(U221.inputSize()-1)).each{
                    int index ->
                        _board.connect(chip, chip.DATA_OUT + index, U221, U221.INPUT + (chipNumber * U221.inputSize()) + index)
        
                }
        }

// ====== EPROM
    
        (0..(U220.inputSize()-1)).each{
            int index ->
                U215.connect(REGISTERS_DATA_IN[index].split(), U215.DATA_IN + index, Wire.OUT)
        }
    
        offset = 0
        (0..(U215.addressSize()-1)).each{
            int index ->
                U215.connect(MEM_ADDR[index].split(), U215.ADDRESS + index, Wire.OUT)
        }
    
        _board.connect(U100_M, U100_M.OUTPUT + 15, U215, U215.CHIP_SELECT)
    
        (0..(U221.inputSize()-1)).each{
            int index ->
                _board.connect(U215, U215.DATA_OUT + index, U221, U221.INPUT + index + (15 * U221.inputSize()))
        
        }
    


// ====== U221
        
        U221.connect(MEM_ADDR[12].split(), U221.SELECTOR + 0, Wire.OUT)
        U221.connect(MEM_ADDR[13].split(), U221.SELECTOR + 1, Wire.OUT)
        U221.connect(MEM_ADDR[14].split(), U221.SELECTOR + 2, Wire.OUT)
        U221.connect(MEM_ADDR[15].split(), U221.SELECTOR + 3, Wire.OUT)
        
        U221.connect(ALWAYS_HIGH, U221.CHIP_SELECT, Wire.OUT)

        (0..(U221.inputSize()-1)).each{
            int index ->
                U221.connect(ALWAYS_LOW, U221.INPUT + index + (U221.inputSize() * 14), Wire.OUT)
        }
        
    }
    
}