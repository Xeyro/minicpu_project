package main.groovy.Board

import main.groovy.Component.Component
import main.groovy.Wire.Wire

/**
 * Board
 * Represents a physical board to which components can be added.
 */
interface Board {
    
    /**
     * Add Component:
     * Add a component to the board
     * @param component - component to be added
     */
    void addComponent(Component component)
    
    /**
     * Add Clock:
     * Add a clock to the board
     * @param clock - clock to be added
     */
    void addClock(Component clock)
    
    /**
     * Clock Tick:
     * Causes all clocks attached to the board to execute a single
     * clock tick
     */
    void clockTick()
    
    /**
     * Clock Cycle:
     * Causes all Components on the board to tick through a full clock
     * cycle
     */
    void clockCycle()
    
    Wire clockWire()
    
    /**
     * Connect:
     * Connect two components with a wire
     * Components are assumed to already be attached to the board. Calling
     * this on components that are not attached to the board has undefined
     * results. (Probably bad ones)
     * @param from      - component to connect to the INPUT side of the wire
     * @param fromPin   - pin on the from component to connect the wire to
     * @param to        - component to connect to the OUTPUT side of the wire
     * @param toPin     - pin on the to component to connect the wire to
     */
    Wire connect(Component from, int fromPin, Component to, int toPin)
    
    /**
     * Get the list of Components currently attached to the board.
     * @return The list of Components on the board
     */
    List<Component> getComponents()
    
    /**
     * Get the list of wires currently attached to the board
     * @return The list of wires currently on the board
     */
    List<Wire> getWires()
}