package main.groovy.Board

import main.groovy.Component.Component
import main.groovy.Wire.UniDirectionalWire
import main.groovy.Wire.Wire

class CircuitBoard implements Board {
    
    protected List<Component>               _components
    protected List<Component>               _clocks
    protected Map<Component, List<Wire>>    _componentsToWires
    protected int                           _ticksPerCycle
    
    protected Wire                          _clockWire
    
    Wire clockWire() { _clockWire}
    
    CircuitBoard() {
        _components = new ArrayList<>()
        _clocks = new ArrayList<>()
        _componentsToWires = new HashMap<>()
        _clockWire = new UniDirectionalWire()
        _ticksPerCycle = 20
    }
    
    void addComponent(Component component) {
        _components.add(component)
        _componentsToWires.put(component, new ArrayList<Wire>())
    }
    
    void addClock(Component clock) {
        _clocks.add(clock)
        _componentsToWires.put(clock, new ArrayList<Wire>())
    }

    void clockTick() {
        // A clock tick is both a high and a low, so do twice
            // Tick clocks
            _clocks.each{ it.process() }
            _clocks.each{
                if(_componentsToWires.containsKey(it))
                    _componentsToWires.get(it).each{it.process()}
            }
            
            // Tick components
            _components.each{ it.process() }
            _components.each{
                if(_componentsToWires.containsKey(it))
                    _componentsToWires.get(it).each{it.process()}
            }
    }
    
    void ticksPerCycle(int ticks) { _ticksPerCycle = ticks }
    int ticksPerCycle() { return _ticksPerCycle }
    
    void clockCycle() {
        _clockWire.setEnd(Wire.IN, true)
        _clockWire.process()
        clockTick()
        _clockWire.setEnd(Wire.IN, false)
        _clockWire.process()
        (1..(_ticksPerCycle-1)).each { clockTick() }
    }
    
    Wire connect(Component from, int fromPin, Component to, int toPin) {
        Wire wire = new UniDirectionalWire()
        from.connect(wire, fromPin, Wire.IN)
        to.connect(wire, toPin, Wire.OUT)
        _componentsToWires.get(from).add(wire)
        return wire
    }
    
    List<Component> getComponents() { _components }
    
    List<Wire> getWires() {
        List<Wire> wires = new ArrayList<>()
        _componentsToWires.each {
            it.value.each {
                wires.add(it)
            }
        }
        return wires
    }
}
