package main.groovy.MetaData

class MetaData {
    
    static final int R1_CS              = 0
    static final int R2_CS              = 1 + R1_CS
    static final int R3_CS              = 1 + R2_CS
    static final int R4_CS              = 1 + R3_CS
    static final int U14_CS             = 1 + R4_CS
    static final int U100_CIN           = 1 + U14_CS
    static final int U107_PLUS_MINUS    = 1 + U100_CIN
    static final int U110_CS            = 1 + U107_PLUS_MINUS
    static final int U111_SEL           = 1 + U110_CS
    static final int U112_SEL           = 3 + U111_SEL
    static final int U113_SEL           = 3 + U112_SEL
    static final int U114_OEA           = 3 + U113_SEL
    static final int U114_SELA          = 1 + U114_OEA
    static final int U114_OEB           = 2 + U114_SELA
    static final int U114_SELB          = 1 + U114_OEB
    static final int U117_SEL           = 2 + U114_SELB
    static final int U118A_SEL          = 1 + U117_SEL
    static final int U118B_SEL          = 3 + U118A_SEL
    static final int U120_SEL           = 3 + U118B_SEL
    static final int U115_SEL           = 1 + U120_SEL
    static final int U116_SEL           = 2 + U115_SEL
    static final int U15_CS             = 2 + U116_SEL
    static final int U220_SEL           = 1 + U15_CS
    static final int READ_WRITE         = 2 + U220_SEL
    static final int CONTROL_PINS       = 1 + READ_WRITE
    
    
    /** Mov ########## */
    static final int MOV_80     = 128   /** Mov  R8, R8         3B -- 2B *packed */
    static final int MOV_81     = 129   /** Mov  R8, $HH        3B */
    static final int MOV_82     = 130   /** Mov  R8, [$MMMM]    4B */
    static final int MOV_83     = 131   /** Mov  [$MMMM], R8    4B */
    
    
    /** Math ########## */
    static final int ADDC_10    = 16    /** Addc R8, R8         3B -- 2B *packed */
    static final int ADDC_11    = 17    /** Addc R8, $HH        3B */
    static final int ADDC_12    = 18    /** Addc R8, [$MMMM]    4B */
    static final int ADDC_13    = 19    /** Addc [$MMMM], R8    4B */
    static final int SUBB_20    = 32    /** Subb R8, R8         3B -- 2B *packed */
    static final int SUBB_21    = 33    /** Subb R8, $HH        3B */
    static final int SUBB_22    = 34    /** Subb R8, [$MMMM]    4B */
    static final int SUBB_23    = 35    /** Subb [$MMMM], R8    4B */
    static final int CMP_30     = 48    /** Cmp  R8, R8         3B -- 2B *packed */
    static final int CMP_31     = 49    /** Cmp  R8, $HH        3B */
    static final int CMP_32     = 50    /** Cmp  R8, [$MMMM]    4B */
    static final int CMP_33     = 51    /** Cmp  [$MMMM], R8    4B */
    
    
    /** Logical ########## */
    static final int NOT_40     = 64    /** Not  R8             2B */
    static final int NOT_43     = 67    /** Not  [$MMMM]        3B */
    static final int AND_50     = 80    /** And  R8, R8         3B -- 2B *packed */
    static final int AND_51     = 81    /** And  R8, $HH        3B */
    static final int AND_52     = 81    /** And  R8, [$MMMM]    4B */
    static final int AND_53     = 83    /** And  [$MMMM], R8    4B */
    static final int OR_60      = 96    /** Or   R8, R8         3B -- 2B *packed */
    static final int OR_61      = 97    /** Or   R8, $HH        3B */
    static final int OR_62      = 98    /** Or   R8, [$MMMM]    4B */
    static final int OR_63      = 99    /** Or   [$MMMM], R8    4B */
    static final int XOR_70     = 112   /** Xor  R8, R8         3B -- 2B *packed */
    static final int XOR_71     = 113   /** Xor  R8, $HH        3B */
    static final int XOR_72     = 114   /** Xor  R8, [$MMMM]    4B */
    static final int XOR_73     = 115   /** Xor  [$MMMM], R8    4B */

    
    /** Control (Unconditional) ########## */
    static final int JMP_B8     = 184   /** Jmp  R16            2B */
    static final int JMP_B9     = 185   /** Jmp  [$MMMM]        3B */
    
    
    /** Control (Conditional) ########## */
    static final int JLO_D6     = 214   /** Jlo  $HHHH          3B */
    static final int JHS_D7     = 215   /** jhs  $HHHH          3B */
    static final int JEQ_D8     = 216   /** Jeq  $HHHH          3B */
    static final int JNE_D9     = 217   /** Jne  $HHHH          3B */
    static final int JMI_DA     = 218   /** Jmi  $HHHH          3B */
    static final int JPL_DB     = 219   /** Jpl  $HHHH          3B */
    
    
    /** Misc ########## */
    static final int NOP_E0     = 224   /** nop                 1B */

    static final List<boolean[]> DO_NOTHING = [new boolean[CONTROL_PINS]]
    
    static final List<boolean[]> STARTUP_INSTRUCTION =
        (List<boolean[]>) {
            List<boolean[]> values = new ArrayList<>()
            boolean [] cycle1 = new boolean[CONTROL_PINS]
                cycle1[U115_SEL] = true
                cycle1[U15_CS] = true
            values.add(cycle1)
            return values
        }()
    
    static class InstructionMetaData {
        final String name
        final int size
        final int operand1Size
        final int operand2Size
        final boolean operand1IsPacked = false
        final int cycles
        final List<boolean[]> controlAtCycle
        
        InstructionMetaData(String name, int size, int operand1Size, int operand2Size, List<boolean[]> controlAtCycle) {
            this.size = size
            this.operand1Size = operand1Size
            this.operand2Size = operand2Size
            this.name = name
            this.controlAtCycle = controlAtCycle
            cycles = 1
        }
    }

    static final InstructionMetaData[] InstructionInfo = new InstructionMetaData[256]
    
    static final InstructionMetaData ClearStackPointer = new InstructionMetaData("Spc0", 1, 0, 0, STARTUP_INSTRUCTION)
    
    static {
    
        (0..(InstructionInfo.length-1)).each{ InstructionInfo[it] = new InstructionMetaData("???", 1, 0, 0, DO_NOTHING) }
        
        InstructionInfo.with {
            /** Mov ########## */
            it[MOV_80] = new InstructionMetaData("Mov", 3, 1, 1, DO_NOTHING)
            it[MOV_81] = new InstructionMetaData("Mov", 3, 1, 1, DO_NOTHING)
            it[MOV_82] = new InstructionMetaData("Mov", 4, 1, 2, DO_NOTHING)
            it[MOV_83] = new InstructionMetaData("Mov", 4, 2, 1, DO_NOTHING)
        
            /** Math ########## */
            it[ADDC_10] = new InstructionMetaData("Addc",3, 1, 1, DO_NOTHING)
            it[ADDC_11] = new InstructionMetaData("Addc",3, 1, 1, DO_NOTHING)
            it[ADDC_12] = new InstructionMetaData("Addc",4, 1, 2, DO_NOTHING)
            it[ADDC_13] = new InstructionMetaData("Addc",4, 2, 1, DO_NOTHING)
    
            it[SUBB_20] = new InstructionMetaData("Subb",3, 1, 1, DO_NOTHING)
            it[SUBB_21] = new InstructionMetaData("Subb",3, 1, 1, DO_NOTHING)
            it[SUBB_22] = new InstructionMetaData("Subb",4, 1, 2, DO_NOTHING)
            it[SUBB_23] = new InstructionMetaData("Subb",4, 2, 1, DO_NOTHING)
        
            it[CMP_30] = new InstructionMetaData("Cmp",3, 1, 1, DO_NOTHING)
            it[CMP_31] = new InstructionMetaData("Cmp",3, 1, 1, DO_NOTHING)
            it[CMP_32] = new InstructionMetaData("Cmp",4, 1, 2, DO_NOTHING)
            it[CMP_33] = new InstructionMetaData("Cmp",4, 2, 1, DO_NOTHING)
        
            /** Logical ########## */
            it[NOT_40] = new InstructionMetaData("Not", 2, 1, 0, DO_NOTHING)
            it[NOT_43] = new InstructionMetaData("Not",3, 2, 0, DO_NOTHING)
        
            it[AND_50] = new InstructionMetaData("And",3, 1, 1, DO_NOTHING)
            it[AND_51] = new InstructionMetaData("And",3, 1, 1, DO_NOTHING)
            it[AND_52] = new InstructionMetaData("And",4, 1, 2, DO_NOTHING)
            it[AND_53] = new InstructionMetaData("And",4, 2, 1, DO_NOTHING)
        
            it[OR_60] = new InstructionMetaData("Or", 3, 1, 1, DO_NOTHING)
            it[OR_61] = new InstructionMetaData("Or",3, 1, 1, DO_NOTHING)
            it[OR_62] = new InstructionMetaData("Or",4, 1, 2, DO_NOTHING)
            it[OR_63] = new InstructionMetaData("Or",4, 2, 1, DO_NOTHING)
        
            it[XOR_70] = new InstructionMetaData("Xor", 3, 1, 1, DO_NOTHING)
            it[XOR_71] = new InstructionMetaData("Xor",3, 1, 1, DO_NOTHING)
            it[XOR_72] = new InstructionMetaData("Xor",4, 1, 2, DO_NOTHING)
            it[XOR_73] = new InstructionMetaData("Xor",4, 2, 1, DO_NOTHING)
        
            /** Control (Unconditional) ########## */
            it[JMP_B8] = new InstructionMetaData("Jmp", 2, 1, 0, DO_NOTHING)
            it[JMP_B9] = new InstructionMetaData("Jmp",3, 2, 0, DO_NOTHING)
        
            /** Control (Conditional) ########## */
            it[JLO_D6] = new InstructionMetaData("Jlo", 3, 2, 0, DO_NOTHING)
            it[JHS_D7] = new InstructionMetaData("Jhs", 3, 2, 0, DO_NOTHING)
            it[JEQ_D8] = new InstructionMetaData("Jeq", 3, 2, 0, DO_NOTHING)
            it[JNE_D9] = new InstructionMetaData("Jne", 3, 2, 0, DO_NOTHING)
            it[JMI_DA] = new InstructionMetaData("Jmi", 3, 2, 0, DO_NOTHING)
            it[JPL_DB] = new InstructionMetaData("Jpl", 3, 2, 0, DO_NOTHING)
        
            /** Misc ########## */
            it[NOP_E0] = new InstructionMetaData("Nop", 1, 0, 0, DO_NOTHING)
        }
    }
    
    static class ChipMetaData {
        
        final Map<String, String> renameInput
        final List<String> hideInput
        
        ChipMetaData(Map<String, String> rename, List<String> hide) {
            renameInput = rename
            hideInput = hide
        }
    }
    
    static final HashMap<String, ChipMetaData> chipMetaData = new HashMap<>()
    
    static {
        chipMetaData.with{
    
            put("U100", new ChipMetaData(
                [:],
                ["CHIP_SELECT", "SUBTRACT"]
            ))
    
            put("U101", new ChipMetaData(
                [:],
                ["CHIP_SELECT"]
            ))
    
            put("U102", new ChipMetaData(
                [:],
                ["CHIP_SELECT"]
            ))
    
            put("U103", new ChipMetaData(
                [:],
                ["CHIP_SELECT"]
            ))
    
            put("U104", new ChipMetaData(
                [:],
                [
                    "CHIP_SELECT",
                    "INPUT_B"
                ]
            ))
            
            put("U107", new ChipMetaData(
                [
                    "INPUT_A"   : "SP",
                    "INPUT_B"   : "2",
                    "SUBTRACT"  : "+/-"
                ],
                ["CARRY_OUT", "CHIP_SELECT", "CARRY_IN"]
            ))
            
            put("U111", new ChipMetaData(
                [
                    "INPUT_0" : "0_SUM",
                    "INPUT_1" : "1_AND",
                    "INPUT_2" : "2_OR",
                    "INPUT_3" : "3_XOR",
                    "INPUT_4" : "4_NOT"
                ],
                [
                    "INPUT_5",
                    "INPUT_6",
                    "INPUT_7",
                    "CHIP_SELECT"
                ]))
    
            put("U112", new ChipMetaData([
                "INPUT_0" : "0_Reg 1",
                "INPUT_1" : "1_Reg 2",
                "INPUT_2" : "2_Reg 3",
                "INPUT_3" : "3_Reg 4",
                "INPUT_4" : "4_MEM",
                "INPUT_5" : "5_FLAGS",
                "INPUT_6" : "6_INST",
            ],
                ["INPUT_7","CHIP_SELECT"]
            ))
    
            put("U113", new ChipMetaData([
                "INPUT_0" : "0_Reg 1",
                "INPUT_1" : "1_Reg 2",
                "INPUT_2" : "2_Reg 3",
                "INPUT_3" : "3_Reg 4",
                "INPUT_4" : "4_MEM",
                "INPUT_5" : "5_FLAGS",
                "INPUT_6" : "6_INST",
            ],
                ["INPUT_7", "CHIP_SELECT"]
            ))
    
            put("U117", new ChipMetaData(
                [
                    "INPUT_0" : "0_DATA",
                    "INPUT_1" : "1_SP_SUM",
                ],
                ["CHIP_SELECT"]
            ))
            
            put("U118A", new ChipMetaData([
                "INPUT_0" : "0_DATA",
                "INPUT_1" : "1_SP",
                "INPUT_2" : "2_INST",
                "INPUT_3" : "3_ALU",
                "INPUT_4" : "4_RES",
                "INPUT_5" : "5_MEM",
                "INPUT_6" : "6_RES",
                "INPUT_7" : "7_RES"
            ],
                ["CHIP_SELECT"]))
            
            put("U118B", new ChipMetaData([
                "INPUT_0" : "0_DATA",
                "INPUT_1" : "1_SP",
                "INPUT_2" : "2_INST",
                "INPUT_3" : "3_ALU",
                "INPUT_4" : "4_RES",
                "INPUT_5" : "5_MEM",
                "INPUT_6" : "6_RES",
                "INPUT_7" : "7_RES"
            ],
                ["CHIP_SELECT"]))
            
            put("U120", new ChipMetaData([
                "INPUT_0" : "0_Adder",
                "INPUT_1" : "1_ALU_OUT"
            ],
                ["CHIP_SELECT"]))
        }
    }
}
