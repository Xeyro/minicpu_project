package groovy.IntegrationTests

import groovy.IntegrationTests.Util.LED
import groovy.IntegrationTests.Util.Switch
import main.groovy.Board.Board
import main.groovy.Component.Clock
import main.groovy.Component.DataSelector
import org.junit.Test

import static groovy.IntegrationTests.Util.IntegrationTestUtil.simpleCircuitBoard

class DataSelectorCircuitTest {

    @Test
    void demultiplexer_circuit() {
        
        // Create board
        Board board = simpleCircuitBoard()
        
        DataSelector demultiplexer = new DataSelector.Builder()
            .selects(2)
            .ioCount(4)
            .buildDemultiplexer()
        
        board.addComponent(demultiplexer)
        
        // Add a clock
        Clock clock = new Clock()
        board.addClock(clock)
        board.connect(clock, clock.OUT, demultiplexer, demultiplexer.CHIP_SELECT)
        
        // Add an input switch
        Switch inputSwitch = new Switch()
        board.addComponent(inputSwitch)
        board.connect(inputSwitch, inputSwitch.OUT, demultiplexer, demultiplexer.INPUT)

        // Add switches to control the select lines
        Switch[] selectSwitches = new Switch[2]
        
        (0..1).eachWithIndex {
            int entry, int index ->
                final Switch _switch = new Switch()
                selectSwitches[index] = _switch
                board.addComponent(_switch)
                board.connect(_switch, _switch.OUT, demultiplexer, demultiplexer.SELECTOR + index)
        }
        
        // Add some leds to check the output
        LED[] leds = new LED[4]
        (0..3).eachWithIndex {
            int entry, int index ->
                final LED led = new LED()
                leds[index] = led
                board.addComponent(led)
                board.connect(demultiplexer, demultiplexer.OUTPUT + index, led, led.IN)
        }
    
        inputSwitch.turnOn()
        
        // Select = 0, Input = 1
        // Expected: Led0 = 1
        board.clockTick()
        board.clockTick()
        board.clockTick()
        
        assert leds[0].isOn()
        assert leds[1].isOn() == false
        assert leds[2].isOn() == false
        assert leds[3].isOn() == false
    
        // Select = 1, Input = 1
        // Expected: Led1 = 1
        selectSwitches[0].turnOn()
        selectSwitches[1].turnOff()
        board.clockTick()
        board.clockTick()
        board.clockTick()
    
        assert leds[0].isOn() == false
        assert leds[1].isOn()
        assert leds[2].isOn() == false
        assert leds[3].isOn() == false
    
        // Select = 2, Input = 1
        // Expected: Led2 = 1
        selectSwitches[0].turnOff()
        selectSwitches[1].turnOn()
        board.clockTick()
        board.clockTick()
        board.clockTick()
    
        assert leds[0].isOn() == false
        assert leds[1].isOn() == false
        assert leds[2].isOn()
        assert leds[3].isOn() == false
    
        // Select = 3, Input = 1
        // Expected: Led3 = 1
        selectSwitches[0].turnOn()
        selectSwitches[1].turnOn()
        board.clockTick()
        board.clockTick()
        board.clockTick()
    
        assert leds[0].isOn() == false
        assert leds[1].isOn() == false
        assert leds[2].isOn() == false
        assert leds[3].isOn()
    }
    
    @Test
    void demultiplexer_multiszie_input_circuit() {
    
        // Create board
        Board board = simpleCircuitBoard()
    
        DataSelector demultiplexer = new DataSelector.Builder()
            .selects(1)
            .ioCount(2)
            .inputSize(2)
            .buildDemultiplexer()
    
        board.addComponent(demultiplexer)
    
        // Add a clock
        Clock clock = new Clock()
        board.addClock(clock)
        board.connect(clock, clock.OUT, demultiplexer, demultiplexer.CHIP_SELECT)
    
        // Add an input switch
        Switch[] inputSwitches = new Switch[2]
        (0..1).each {
            int offset ->
                final Switch aSwitch = new Switch()
                board.addComponent(aSwitch)
                board.connect(aSwitch, aSwitch.OUT, demultiplexer, demultiplexer.INPUT + offset)
                inputSwitches[offset] = aSwitch
        }
        
        // Add switches to control the select lines
        Switch selectSwitch = new Switch()
        board.addComponent(selectSwitch)
        board.connect(selectSwitch, selectSwitch.OUT, demultiplexer, demultiplexer.SELECTOR)
    
        // Add some leds to check the output
        LED[] leds = new LED[4]
        (0..3).eachWithIndex {
            int entry, int index ->
                final LED led = new LED()
                leds[index] = led
                board.addComponent(led)
                board.connect(demultiplexer, demultiplexer.OUTPUT + index, led, led.IN)
        }
    
    
        // Select = 0, Input = 0
        // Expected: Leds0 = 0
        selectSwitch.turnOff()
        board.clockTick()
        board.clockTick()
        board.clockTick()
    
        assert leds[0].isOn() == false
        assert leds[1].isOn() == false
        
        assert leds[2].isOn() == false
        assert leds[3].isOn() == false
    
        // Select = 0, Input = 1
        // Expected: Leds0 = 1
        selectSwitch.turnOff()
        inputSwitches[0].turnOn()
        inputSwitches[1].turnOff()
        board.clockTick()
        board.clockTick()
        board.clockTick()
    
        assert leds[0].isOn()
        assert leds[1].isOn() == false
        
        assert leds[2].isOn() == false
        assert leds[3].isOn() == false
    
        // Select = 1, Input = 2
        // Expected: Leds1 = 2
        selectSwitch.turnOn()
        inputSwitches[0].turnOff()
        inputSwitches[1].turnOn()
        board.clockTick()
        board.clockTick()
        board.clockTick()
    
        assert leds[0].isOn() == false
        assert leds[1].isOn() == false
        
        assert leds[2].isOn() == false
        assert leds[3].isOn()
    
        // Select = 1, Input = 3
        // Expected: Leds1 = 3
        selectSwitch.turnOn()
        inputSwitches[0].turnOn()
        inputSwitches[1].turnOn()
        board.clockTick()
        board.clockTick()
        board.clockTick()
    
        assert leds[0].isOn() == false
        assert leds[1].isOn() == false
        
        assert leds[2].isOn()
        assert leds[3].isOn()
    }

    @Test
    void multiplexer_circuit() {
        // Create board
        Board board = simpleCircuitBoard()
    
        DataSelector multiplexer = new DataSelector.Builder()
            .selects(2)
            .ioCount(4)
            .buildMultiplexer()
    
        board.addComponent(multiplexer)
    
        // Add a clock
        Clock clock = new Clock()
        board.addClock(clock)
        board.connect(clock, clock.OUT, multiplexer, multiplexer.CHIP_SELECT)
    
        
        // Add input switches
        Switch[] inputSwitches = new Switch[4]

        (0..3).each{
            int inputNumber ->
                Switch aSwitch = new Switch()
                board.addComponent(aSwitch)
                inputSwitches[inputNumber] = aSwitch
                board.connect(aSwitch, aSwitch.OUT, multiplexer, multiplexer.INPUT + inputNumber)
        }
        
        // Add switches to control the select lines
        Switch[] selectSwitches = new Switch[2]
    
        (0..1).eachWithIndex {
            int entry, int index ->
                final Switch _switch = new Switch()
                selectSwitches[index] = _switch
                board.addComponent(_switch)
                board.connect(_switch, _switch.OUT, multiplexer, multiplexer.SELECTOR + index)
        }
        
    
        // Add some leds to check the output
        LED outputLed = new LED()
        board.addComponent(outputLed)
        board.connect(multiplexer, multiplexer.OUTPUT, outputLed, outputLed.IN)
    
    
        inputSwitches[0].turnOn()
        inputSwitches[2].turnOn()
    
        // 0
        selectSwitches[0].turnOff()
        selectSwitches[1].turnOff()
        board.clockTick()
        board.clockTick()
        board.clockTick()
        assert outputLed.isOn()
    
        // 1
        selectSwitches[0].turnOn()
        selectSwitches[1].turnOff()
        board.clockTick()
        board.clockTick()
        board.clockTick()
        assert outputLed.isOn() == false
    
        // 2
        selectSwitches[0].turnOff()
        selectSwitches[1].turnOn()
        board.clockTick()
        board.clockTick()
        board.clockTick()
        assert outputLed.isOn()
    
        // 3
        selectSwitches[0].turnOn()
        selectSwitches[1].turnOn()
        board.clockTick()
        board.clockTick()
        board.clockTick()
        assert outputLed.isOn() == false
    }
    
    
    @Test
    void multiplexer_multisize_input_circuit() {
        // Create board
        Board board = simpleCircuitBoard()
    
        DataSelector multiplexer = new DataSelector.Builder()
            .selects(1)
            .inputSize(2)
            .ioCount(2)
            .buildMultiplexer()
    
        board.addComponent(multiplexer)
    
        // Add a clock
        Clock clock = new Clock()
        board.addClock(clock)
        board.connect(clock, clock.OUT, multiplexer, multiplexer.CHIP_SELECT)
    
    
        // Add input switches
        Switch[] inputSwitches = new Switch[4]
    
        (0..3).each{
            int inputNumber ->
                Switch aSwitch = new Switch()
                board.addComponent(aSwitch)
                inputSwitches[inputNumber] = aSwitch
                board.connect(aSwitch, aSwitch.OUT, multiplexer, multiplexer.INPUT + inputNumber)
        }
    
        // Add switches to control the select lines
        Switch selectSwitch = new Switch()
        board.addComponent(selectSwitch)
        board.connect(selectSwitch, selectSwitch.OUT, multiplexer, multiplexer.SELECTOR)
    
    
        // Add some leds to check the output
        LED[] outputLeds = new LED[2]
        (0..1).each{
            int index ->
                final LED led = new LED()
                board.addComponent(led)
                board.connect(multiplexer, multiplexer.OUTPUT + index, led, led.IN)
                outputLeds[index] = led
        }
        
        // Sel= 0, Input = 0
        // leds = 0
        selectSwitch.turnOff()
        inputSwitches[0].turnOff()
        inputSwitches[1].turnOff()
        board.clockTick()
        board.clockTick()
        board.clockTick()
        assert outputLeds[0].isOn() == false
        assert outputLeds[1].isOn() == false
    
        
        // Sel= 0, Input = 1
        // leds = 1
        selectSwitch.turnOff()
        inputSwitches[0].turnOn()
        inputSwitches[1].turnOff()
        board.clockTick()
        board.clockTick()
        board.clockTick()
        assert outputLeds[0].isOn()
        assert outputLeds[1].isOn() == false
    
        
        // Sel= 1, Input = 2
        // leds = 2
        selectSwitch.turnOn()
        inputSwitches[2].turnOff()
        inputSwitches[3].turnOn()
        board.clockTick()
        board.clockTick()
        board.clockTick()
        assert outputLeds[0].isOn() == false
        assert outputLeds[1].isOn()
    
    
        // Sel= 1, Input = 3
        // leds = 3
        selectSwitch.turnOn()
        inputSwitches[2].turnOn()
        inputSwitches[3].turnOn()
        board.clockTick()
        board.clockTick()
        board.clockTick()
        assert outputLeds[0].isOn()
        assert outputLeds[1].isOn()
    }
}
