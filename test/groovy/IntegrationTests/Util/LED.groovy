package groovy.IntegrationTests.Util

import main.groovy.Component.BaseComponent
import main.groovy.Component.Component
import main.groovy.Wire.Wire

/**
 * LED
 * Simulated LED used mainly for testing
 */
class LED extends BaseComponent implements Component {

    private int _blinkCount
    private boolean _isOn
    
    public final int IN     = 0
    public final int OUT    = 1

    LED() {
        _pins = new Wire[2]
        _end = new int[2]
    }
    
    int blinkCount() {
        return _blinkCount
    }
    
    boolean isOn() {
        return _isOn
    }
    
    @Override
    void process() {
        final boolean nextState = _pins[IN].getEnd(_end[IN])
        if(_isOn || nextState == false)
            _blinkCount++
        _isOn = nextState
    }
}
