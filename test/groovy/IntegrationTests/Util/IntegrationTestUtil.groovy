package groovy.IntegrationTests.Util

import main.groovy.Board.Board
import main.groovy.Board.CircuitBoard
import main.groovy.Component.Clock
import main.groovy.Wire.Wire

class IntegrationTestUtil {
    
    static Wire alwaysHigh() {
        [
            getEnd : { end -> true },
            setEnd : { end, state -> }
        ] as Wire
    }
    
    static Wire alwaysLow() {
        [
            getEnd : { end -> false },
            setEnd : { end, state -> }
        ] as Wire
    }
    
    static Board simpleCircuitBoard() {
        Board board = new CircuitBoard()
        board.addClock(new Clock())
        return board
    }
}
