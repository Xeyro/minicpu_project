package groovy.IntegrationTests.Util

import main.groovy.Component.BaseComponent
import main.groovy.Component.Component
import main.groovy.Wire.Wire

class Switch extends BaseComponent implements Component {

    public final int OUT = 0
    
    private boolean _isOn
    
    Switch() {
        _pins = new Wire[1]
        _end = new int[1]
    }

    void turnOn() { _isOn = true }
    void turnOff() { _isOn = false }
    
    @Override
    void process() {
        setPin(0, _isOn)
    }
}
