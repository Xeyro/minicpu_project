package groovy.Component

import main.groovy.Component.BaseComponent
import main.groovy.Component.Register
import main.groovy.Wire.UniDirectionalWire
import main.groovy.Wire.Wire
import org.junit.Test

class RegisterTest {

    @Test
    void reads_input_into_internal_value_when_selected() {
        
        Register register = new Register(8)
        
        Wire chipSelect = new UniDirectionalWire()
        register.connect(chipSelect, register.CHIP_SELECT, Wire.OUT)
        chipSelect.setEnd(Wire.IN, true)
        
        Wire[] inputs = new Wire[8]
        (0..7).each {
            int index ->
                Wire wire = new UniDirectionalWire()
                inputs[index] = wire
                register.connect(wire, register.INPUT + index, Wire.OUT)
        }
        
        Wire[] outputs = new Wire[8]
        (0..7).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                outputs[index] = wire
                register.connect(wire, register.OUTPUT + index, Wire.IN)
        }
    
        int newInternalValue = 42
        
        send(inputs, newInternalValue)
        inputs.each { it.process() }
        chipSelect.process()
        register.process()

        assert register.internalValue() == newInternalValue
    }
    
    
    @Test
    void writes_value_as_output() {
    
        Register register = new Register(8)
    
        Wire chipSelect = new UniDirectionalWire()
        register.connect(chipSelect, register.CHIP_SELECT, Wire.OUT)
        chipSelect.setEnd(Wire.IN, false)
        
        Wire[] inputs = new Wire[8]
        (0..7).each {
            int index ->
                Wire wire = new UniDirectionalWire()
                inputs[index] = wire
                register.connect(wire, register.INPUT + index, Wire.OUT)
        }
    
        Wire[] outputs = new Wire[8]
        (0..7).each{
            int index ->
                Wire wire = new UniDirectionalWire()
                outputs[index] = wire
                register.connect(wire, register.OUTPUT + index, Wire.IN)
        }
        
        final int internalValue = register.internalValue()
        
        chipSelect.process()
        register.process()
        outputs.each { it.process() }
        
        boolean[] output = new boolean[8]
        outputs.eachWithIndex {
            Wire wire, int index ->
                output[index] = wire.getEnd(wire.OUT)
        }
        
        assert BaseComponent.binaryToDecimal(output) == internalValue
    }
    
    static void send(Wire[] wires, int value) {
        final boolean[] binary = BaseComponent.decimalToBinary(value)
        wires.eachWithIndex{
            Wire wire, int index ->
                wire.setEnd(Wire.IN, binary[index])
        }
    }
}
