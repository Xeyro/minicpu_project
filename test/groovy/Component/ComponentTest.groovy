package groovy.Component

import main.groovy.Component.BaseComponent
import main.groovy.Component.Component
import main.groovy.Wire.Wire
import org.junit.Test

class ComponentTest extends BaseComponent  {

    static Wire alwaysHigh() {
        [
            getEnd : { end -> true },
            setEnd : { end, state -> }
        ] as Wire
    }
    
    static Wire alwaysLow() {
        [
            getEnd : { end -> false },
            setEnd : { end, state -> }
        ] as Wire
    }
    
    static BaseComponent defaultConfiguration() {
        BaseComponent baseComponent = new BaseComponent()
        baseComponent._pins = new Wire[3]
        baseComponent._end = new int[3]
     
        baseComponent.connect(alwaysHigh(), 0, 0)
        baseComponent.connect(alwaysLow(), 1, 0)
        
        return baseComponent
    }
    
    @Test
    void shorthand_methods_read_pins() {
        BaseComponent component = defaultConfiguration()
        
        assert component.isHigh(0)
        assert component.isLow(1)
        assert component.isConnected(0)
        assert component.isConnected(2) == false
    }

    @Test
    void reads_pins_into_array() {
        BaseComponent baseComponent = new BaseComponent()
        baseComponent._pins = new Wire[4]
        baseComponent._end = new int[4]
        
        def unused = -1 // Wire.end is not used in this test
        
        boolean[] input = [1, 0, 1, 0]
        input.eachWithIndex{
            boolean bit, int pinId ->
                baseComponent.connect(
                    bit ? alwaysHigh() : alwaysLow(),
                    pinId,
                    unused
                )
        }
        
        assert baseComponent.read(0, input.size()) == input
    }
    
    @Test
    void converts_decimal_to_binary() {
        
        Component component = new BaseComponent()
        
        boolean[] binaryValue
    
        // 0
        binaryValue = new boolean[32]
        assert component.decimalToBinary(0) == binaryValue
    
        // 1
        binaryValue = new boolean[32]
        binaryValue[0] = true
        assert component.decimalToBinary(1) == binaryValue
    
        // 2
        binaryValue = new boolean[32]
        binaryValue[1] = true
        assert component.decimalToBinary(2) == binaryValue
    
        // 3
        binaryValue = new boolean[32]
        binaryValue[0] = true
        binaryValue[1] = true
        assert component.decimalToBinary(3) == binaryValue
    }
}
