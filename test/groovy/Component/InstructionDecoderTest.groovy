package groovy.Component

import main.groovy.Component.BaseComponent
import main.groovy.Component.InstructionDecoder
import main.groovy.Wire.UniDirectionalWire
import main.groovy.Wire.Wire
import org.junit.Test

import static main.groovy.MetaData.MetaData.*

class InstructionDecoderTest {

    @Test
    void decodes_instruction() {
        /** Effectively tests the instruction read cycle */
        
        InstructionInfo.eachWithIndex{
            InstructionMetaData metaData, int index ->
                if(InstructionInfo[index] != null)
                {
                    final int opcode = index
    
    
                    InstructionDecoder instructionDecoder = new InstructionDecoder(32)
                    Wire[] inputs = new Wire[32]
                    (0..31).each {
                        Wire wire = new UniDirectionalWire()
                        instructionDecoder.connect(wire, instructionDecoder.MEM_1 + it, Wire.OUT)
                        inputs[it] = wire
                    }
    
                    send(inputs, opcode)
    
                    // MEM 1
                    inputs.each { it.process() }
                    instructionDecoder.process()
                    // MEM 2
                    instructionDecoder.process()
                    // MEM 3
                    instructionDecoder.process()
                    // MEM 4
                    instructionDecoder.process()
    
                    assert instructionDecoder.opCode() == opcode
                }
        }
    }
    
    @Test
    void decodes_operands() {
        InstructionDecoder instructionDecoder = new InstructionDecoder(32)
        Wire[] inputs = new Wire[32]
        (0..31).each {
            Wire wire = new UniDirectionalWire()
            instructionDecoder.connect(wire, instructionDecoder.MEM_1 + it, Wire.OUT)
            inputs[it] = wire
        }
    
        int opcode = MOV_80
        int operand1 = 10
        int operand2 = 43
        
        send(
            inputs,
            MOV_80 +
            (operand1 * (int)Math.pow(2, 8)) +
            (operand2 * (int)Math.pow(2, 16))
        )
    
        // MEM 1
        inputs.each { it.process() }
        instructionDecoder.process()
        // MEM 2
        instructionDecoder.process()
        // MEM 3
        instructionDecoder.process()
        // MEM 4
        instructionDecoder.process()
    
        assert instructionDecoder.opCode() == opcode
        assert instructionDecoder.operand1() == operand1
        assert instructionDecoder.operand2() == operand2
    }
    
    static void send(Wire[] wires, int value) {
        final boolean[] binary = BaseComponent.decimalToBinary(value)
        wires.eachWithIndex{
            Wire wire, int index ->
                wire.setEnd(Wire.IN, binary[index])
        }
    }
}
