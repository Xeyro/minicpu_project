package groovy.Component

import groovy.IntegrationTests.Util.LED
import main.groovy.Component.LogicChip
import main.groovy.Wire.UniDirectionalWire
import main.groovy.Wire.Wire
import org.junit.Test

class LogicChipTest {
    
    static Wire alwaysHigh() {
        [
            getEnd : { end -> true },
            setEnd : { end, state -> }
        ] as Wire
    }
    
    static Wire alwaysLow() {
        [
            getEnd : { end -> false },
            setEnd : { end, state -> }
        ] as Wire
    }
    
    void runLogicTest(LogicChip chip, boolean[] a, boolean[] b, boolean[] expected) {
        
        // Set input a on the chip
        a.eachWithIndex {
            boolean entry, int offset ->
                chip.connect(
                    entry ? alwaysHigh() : alwaysLow(),
                    chip.INPUT_A + offset,
                    Wire.OUT)
        }
    
        // Set input b on the chip
        b.eachWithIndex {
            boolean entry, int offset ->
                chip.connect(
                    entry ? alwaysHigh() : alwaysLow(),
                    chip.INPUT_B + offset,
                    Wire.OUT)
        }

        LED[] leds = new LED[a.size()]
        Wire[] wires = new Wire[a.size()]
        
        // Set up some LEDs to check the output
        expected.eachWithIndex{
            boolean entry, int offset ->
                Wire wire = new UniDirectionalWire()
                chip.connect(wire, chip.OUTPUT + offset, Wire.IN)
                LED led = new LED()
                led.connect(wire, led.IN, Wire.OUT)
                
                wires[offset] = wire
                leds[offset] = led
        }
    
        // Process everything
        chip.process()
        wires.each{ it.process() }
        leds.each{ it.process() }

        // Check output
        leds.eachWithIndex {
            LED entry, int offset ->
                assert entry.isOn() == expected[offset]
        }
    }

    @Test
    void can_perform_logical_and() {
        
        LogicChip chip = new LogicChip.Builder()
            .operandSize(4)
            .functionAnd()
            .build()
    
        runLogicTest(
            chip,
            binary(0, 0, 1, 1),
            binary(0, 1, 0, 1),
            binary(0, 0, 0, 1)
        )
    }
    
    @Test
    void can_perform_logical_or() {
        LogicChip chip = new LogicChip.Builder()
            .operandSize(4)
            .functionOr()
            .build()
    
        runLogicTest(
            chip,
            binary(0, 0, 1, 1),
            binary(0, 1, 0, 1),
            binary(0, 1, 1, 1)
        )
    }
    
    
    @Test
    void can_perform_logical_xor() {
        LogicChip chip = new LogicChip.Builder()
            .operandSize(4)
            .functionXor()
            .build()
    
        runLogicTest(
            chip,
            binary(0, 0, 1, 1),
            binary(0, 1, 0, 1),
            binary(0, 1, 1, 0)
        )
    }
    
    @Test
    void can_perform_logical_not() {
        LogicChip chip = new LogicChip.Builder()
            .operandSize(2)
            .functionNot()
            .build()
    
        runLogicTest(
            chip,
            binary(0, 1),
            binary(0, 0), // This is a NOT gate, so this input does nothing
            binary(1, 0)
        )
    }
    
    
    static boolean[] binary(int... values ) {
        return values.collect{it -> it == 1 }
    }
}