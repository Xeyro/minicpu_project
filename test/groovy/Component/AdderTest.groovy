package groovy.Component

import main.groovy.Component.Adder
import org.testng.annotations.Test


class AdderTest {
    
    @Test
    // Does Addition
    // Tests that the Adder.add method does binary addition correctly
    //
    void does_addition() {
        // 1 + 1 = 2
        testAddition(
            binary(1),
            binary(1),
            false,
            binary(1, 0)
        )
    
        // 10 + 5 = 15
        testAddition(
            binary(1, 0, 1, 0),
            binary(0, 1, 0, 1),
            false,
            binary(0, 1, 1, 1, 1)
        )
    
        // 3 + 3 = 6
        testAddition(
            binary(1, 1),
            binary(1, 1),
            false,
            binary(1, 1, 0)
        )
    
        // 2 + 2 + Carry = 5
        testAddition(
            binary(1, 0),
            binary(1, 0),
            true,
            binary(1, 0, 1)
        )
    }
    
    // Test Addition
    // Runs each test for the does_addition test method. This is because JUnit sucks -_-
    // Both operands and the expected array are assumed to be passed in in human readable form:
    // the binary number 1010 would be passed in as {1, 0, 1, 0}
    //
    static void testAddition(boolean[] operand1, boolean[] operand2, boolean carryIn, boolean[] expected) {
        // Operands are passed in in human readable form, they are processed backwards so we have to reverse them
        operand1 = operand1.toList().reverse()
        operand2 = operand2.toList().reverse()
        expected = expected.toList().reverse()
        
        boolean[] result = Adder.add(operand1, operand2, carryIn)

        assert expected == result
    }
    
    // Binary
    // Converts a list of integers in binary representation to an array of booleans
    // Example: binary(1, 0, 1, 0) = { true, false, true, false }
    // This method exists to make tests more readable
    //
    static boolean[] binary(int... values ) {
        return values.collect{it -> it == 1 }
    }
    
}
