package groovy.Component

import main.groovy.Component.DataSelector
import org.junit.Test

class MultiplexerTest {

    @Test
    void default_builder_creates_valid_multiplexer() {
        
        DataSelector multiplexer = new DataSelector.Builder()
            .buildMultiplexer()
        
        assert multiplexer.PIN_COUNT == 4
    }
    
    @Test
    void cannot_create_multiplexer_with_zero_inputs_or_selects() {
        
        // Telling the builder to use 0 ioCount should be ignored
        DataSelector multiplexer = new DataSelector.Builder()
            .ioCount(0)
            .buildMultiplexer()
        
        assert multiplexer.PIN_COUNT >= 4
    
        // Telling the builder to use 0 selects should be ignored
        multiplexer = new DataSelector.Builder()
            .selects(0)
            .buildMultiplexer()
    
        assert multiplexer.PIN_COUNT >= 4
    
        // Telling the builder to use 2 ioCount should increase pin count
        multiplexer = new DataSelector.Builder()
            .ioCount(2)
            .buildMultiplexer()
    
        assert multiplexer.PIN_COUNT > 4
    
        // Telling the builder to use 2 selects should increase pin count
        multiplexer = new DataSelector.Builder()
            .selects(2)
            .buildMultiplexer()
    
        assert multiplexer.PIN_COUNT > 4
    }
}
