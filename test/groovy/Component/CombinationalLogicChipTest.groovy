package groovy.Component

import groovy.IntegrationTests.Util.LED
import groovy.IntegrationTests.Util.Switch
import main.groovy.Board.Board
import main.groovy.Board.CircuitBoard
import main.groovy.Component.Clock
import main.groovy.Component.CombinationalLogicChip
import org.testng.annotations.Test

class CombinationalLogicChipTest {

    static CombinationalLogicChip Encoder(int outputSize) {
        return new CombinationalLogicChip.Builder()
            .encodedDataSize(outputSize)
            .buildEncoder()
    }
    

    @Test
    void encoder_encodes_data() {
    
        CombinationalLogicChip encoder
    
        encoder = Encoder(2)
        testChip(
            encoder,
            binary(0, 0, 0, 1),
            binary(0, 0)
        )
    
        encoder = Encoder(2)
        testChip(
            encoder,
            binary(0, 0, 1, 0),
            binary(0, 1)
        )
    
        encoder = Encoder(2)
        testChip(
            encoder,
            binary(0, 1, 0, 0),
            binary(1, 0)
        )
    
        encoder = Encoder(2)
        testChip(
            encoder,
            binary(1, 0, 0, 0),
            binary(1, 1)
        )
    }
    
    void testChip(CombinationalLogicChip chip, boolean[] inputs, boolean[] expected) {
        
        inputs = inputs.toList().reverse().toArray()
        expected = expected.toList().reverse().toArray()
        
        Board board = new CircuitBoard()
        board.addComponent(chip)
        
        Clock clock  = new Clock()
        board.addClock(clock)

        board.connect(clock, clock.OUT, chip, chip.CHIP_SELECT)
        
        inputs.eachWithIndex {
            boolean isOn, int offset ->
                Switch aSwitch = new Switch()
                board.addComponent(aSwitch)
                
                if(isOn)
                    aSwitch.turnOn()
                
                board.connect(aSwitch, aSwitch.OUT, chip, chip.INPUT + offset)
        }
        
        LED[] leds = new LED[expected.size()]
        
        expected.eachWithIndex {
            boolean entry, int index ->
                LED led = new LED()
                board.addComponent(led)
    
                leds[index] = led
                board.connect(chip, chip.OUTPUT + index, led, led.IN)
        }
        
        board.clockTick()
        board.clockTick()
        board.clockTick()

        expected.eachWithIndex {
            boolean bit, int index ->
                assert leds[index].isOn() == bit
        }
    }
    
    static boolean[] binary(int... values ) {
        return values.collect{it -> it == 1 }
    }
}
