package groovy.Wire

import groovy.IntegrationTests.Util.LED
import groovy.IntegrationTests.Util.Switch
import main.groovy.Wire.UniDirectionalWire
import main.groovy.Wire.Wire
import org.testng.annotations.Test

class WireTest {
    
    @Test
    void wire_propagates_signal() {
        Wire wire = new UniDirectionalWire()
     
        Switch aSwitch = new Switch()
        aSwitch.connect(wire, aSwitch.OUT, wire.IN)
        
        LED led = new LED()
        led.connect(wire, led.IN, wire.OUT)
        
        aSwitch.turnOn()
        aSwitch.process()
        wire.process()
        led.process()
        
        assert led.isOn()
    }
    
    @Test
    void each_end_of_split_wire_propagates_signal() {
        Wire wire = new UniDirectionalWire()
    
        LED[] leds = new LED[4]
    
        Switch aSwitch = new Switch()
        aSwitch.connect(wire, aSwitch.OUT, wire.IN)
    
        LED led = new LED()
        led.connect(wire, led.IN, wire.OUT)
        leds[0] = led
        
        (1..3).each {
            int index ->
                LED _led = new LED()
                Wire _wire = wire.split()
                _led.connect(_wire, _led.IN, _wire.OUT)
                
                leds[index] = _led
        }

        aSwitch.turnOn()
        aSwitch.process()
        wire.process()
        leds.each { it.process() }
        
        leds.each {
            assert it.isOn()
        }
    }
}
