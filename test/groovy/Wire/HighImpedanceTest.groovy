package groovy.Wire

import main.groovy.Wire.BiDirectionalWire
import main.groovy.Wire.Wire
import org.junit.Test

class HighImpedanceTest {
    
    @Test
    void wire_is_high_impedance_by_default() {

        Wire wire = new BiDirectionalWire()
        assert wire.isHighImpedance()
    }
    
    
    @Test
    void wire_is_not_high_impedance_after_getting_signal() {

        Wire wire
        
        wire = new BiDirectionalWire()
        wire.setEnd(Wire.IN, true)
        wire.process()
        assert wire.isHighImpedance() == false
    
        wire = new BiDirectionalWire()
        wire.setEnd(Wire.IN, false)
        wire.process()
        assert wire.isHighImpedance() == false
    
        wire = new BiDirectionalWire()
        wire.setEnd(Wire.OUT, true)
        wire.process()
        assert wire.isHighImpedance() == false
    
        wire = new BiDirectionalWire()
        wire.setEnd(Wire.OUT, false)
        wire.process()
        assert wire.isHighImpedance() == false
    }
    
    @Test
    void wire_is_high_impedance_1_tick_after_getting_signal() {

        Wire wire
        
        wire = new BiDirectionalWire()
        wire.setEnd(Wire.IN, true)
        wire.process()
        assert wire.isHighImpedance() == false
        wire.process()
        assert wire.isHighImpedance()
    }
}