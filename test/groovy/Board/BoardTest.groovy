package groovy.Board

import groovy.IntegrationTests.Util.LED
import main.groovy.Board.Board
import main.groovy.Board.CircuitBoard
import main.groovy.Component.Adder
import main.groovy.Component.Clock
import main.groovy.Component.Component
import org.junit.Test

class BoardTest {
    
    @Test
    void can_add_component_to_board() {
        Board board = new CircuitBoard()
        Component component = new Adder(4)
        board.addComponent(component)
    }

    @Test
    void can_add_clock_to_board() {
        Board board = new CircuitBoard()
        Component clock = new Clock()
        board.addClock(clock)
    }

    @Test
    void simulates_clock_ticks() {
        // Given:
        Board board = new CircuitBoard()
        Component clock = new Clock()
        LED led = new LED()
        
        board.addClock(clock)
        board.addComponent(led)
        
        board.connect(clock, clock.OUT, led, led.IN)
        
        // When:
        board.clockTick()
        
        // Then:
        assert led.blinkCount() == 1
    }
    
    @Test
    void connects_components() {
        // Given:
        Board board = new CircuitBoard()
        Component clock = new Clock()
        LED led1 = new LED()
        LED led2 = new LED()
        LED led3 = new LED()
    
        board.addClock(clock)
        board.addComponent(led1)
        board.addComponent(led2)
        board.addComponent(led3)
    
        board.connect(clock, clock.OUT, led1, led1.IN)
        board.connect(clock, clock.OUT, led2, led2.IN)
        board.connect(clock, clock.OUT, led3, led3.IN)
    
        // When:
        board.clockTick()
    
        // Then:
        assert led1.blinkCount() == 1
        assert led2.blinkCount() == 1
        assert led3.blinkCount() == 1
    }
}
