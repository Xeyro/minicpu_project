package groovy.GUIWrapper

import main.groovy.Board.Board
import main.groovy.Board.CircuitBoard
import main.groovy.Component.Adder
import main.groovy.Component.Clock
import main.groovy.GUIWrapper.WrappedBoards.WrappedBoardFactory
import main.groovy.GUIWrapper.WrapperInterface.WrappedBoard
import org.testng.annotations.Test

class WrappedBoardTest {
    
    @Test
    void can_create_wrapped_board() {
        
        Board board = new CircuitBoard()
        
        WrappedBoard wrappedBoard = WrappedBoardFactory.wrap(board)
        
        assert wrappedBoard.getComponents().size() == 0
        assert wrappedBoard.getWires().size() == 0
    }
    
    @Test
    void wrapped_board_wraps_components_correctly() {
        
        Board board = new CircuitBoard()
        
        Adder adder = new Adder(4)
        Clock clock = new Clock()
        
        board.addComponent(adder)
        board.addComponent(clock)
        
        board.connect(clock, clock.OUT, adder, adder.CHIP_SELECT)
        
        WrappedBoard wrappedBoard = WrappedBoardFactory.wrap(board)
    
        assert wrappedBoard.getComponents().size() == 2
        assert wrappedBoard.getWires().size() == 1
    }
    
    @Test
    void wrapped_wires_have_the_ids_of_components_they_are_connected_to() {
    
        Board board = new CircuitBoard()
    
        Adder adder = new Adder(4)
        Clock clock = new Clock()
    
        board.addComponent(adder)
        adder.setId("A10")
        board.addComponent(clock)
        clock.setId("A11")
    
        board.connect(clock, clock.OUT, adder, adder.CHIP_SELECT)
    
        WrappedBoard wrappedBoard = WrappedBoardFactory.wrap(board)
    
        assert wrappedBoard.getWires()[0].getFromId() == "A11"
        assert wrappedBoard.getWires()[0].getToIds().contains("A10")
    }
}
